<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgramstudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programstudies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->string('departementId')->nullable();
            $table->string('departement')->nullable();
            $table->string('niveauId')->nullable();
            $table->string('niveau')->nullable();
            $table->string('filiereId')->nullable();
            $table->string('filiere')->nullable();
            $table->string('userEmail')->nullable();
            $table->string('studentId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programstudies');
    }
}
