<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentmodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentmodes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('typePaiementId');
            $table->string('typePaiement');
            $table->decimal('montant');
            $table->string('institutionId');
            $table->string('institution');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paymentmodes');
    }
}
