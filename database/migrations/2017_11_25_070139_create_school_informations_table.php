<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_informations', function(Blueprint $table) {
            $table->increments('id');
            $table->date('startyear')->nullable();
            $table->date('endyear')->nullable();
            $table->string('nameschool')->nullable();
            $table->string('qualification')->nullable();
            $table->string('userEmail')->nullable();
            $table->string('studentId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_informations');
    }
}
