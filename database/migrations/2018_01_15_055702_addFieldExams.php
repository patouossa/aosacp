<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldExams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('examinations', function (Blueprint $table) {

            $table->string('mathematic')->nullable();
            $table->string('english')->nullable();
            $table->string('englishLitt')->nullable();
            $table->string('french')->nullable();
            $table->string('economic')->nullable();
            $table->string('geography')->nullable();
            $table->string('biology')->nullable();
            $table->string('humanBiology')->nullable();
            $table->string('chemistry')->nullable();
            $table->string('physic')->nullable();
            $table->string('futherMaths')->nullable();
            $table->string('history')->nullable();
            $table->string('computerTech')->nullable();
            $table->string('englishLang')->nullable();
            $table->string('frenchLang')->nullable();
            $table->string('frenchLitt')->nullable();
            $table->integer('nbSitting')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examinations', function (Blueprint $table) {
            //
        });
    }
}
