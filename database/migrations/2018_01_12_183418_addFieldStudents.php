<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {

            $table->string('fatherName')->nullable();
            $table->string('motherName')->nullable();
            $table->string('parentOcuppation')->nullable();
            $table->string('parentTelephone')->nullable();
            $table->string('parentEmail')->nullable();
            $table->string('lieuNaissance')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            //
        });
    }
}
