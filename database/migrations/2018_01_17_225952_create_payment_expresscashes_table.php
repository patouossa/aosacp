<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentExpresscashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_expresscashes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('slipNumber');
            $table->date('dateDepot');
            $table->string('studentId');
            $table->string('student');
            $table->string('studentPhone');
            $table->decimal('amount');
            $table->string('reference');
            $table->string('transactionNumber');
            $table->string('scanDepositSlip');
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->string('adminId')->nullable();
            $table->string('adminEmail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_expresscashes');
    }
}
