<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addelementinstitution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('institutions', function (Blueprint $table) {

            $table->boolean('paieMoMo')->nullable();
            $table->boolean('paieBank')->nullable();
            $table->boolean('paieExpressCash')->nullable();
            $table->string('numCompte')->nullable();
            $table->string('numExpressCash')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('institutions', function (Blueprint $table) {
            //
        });
    }
}
