<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function(Blueprint $table) {
            $table->increments('id')->nullable();
            $table->string('nom')->nullable();
            $table->date('dateNaiss')->nullable();
            $table->string('email')->nullable();
            $table->string('pobox')->nullable();
            $table->string('phone')->nullable();
            $table->string('validite')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('ville')->nullable();
            $table->string('religion')->nullable();
            $table->string('statusmatrimonial')->nullable();
            $table->string('sexe')->nullable();
            $table->string('countryparent')->nullable();
            $table->string('villeparent')->nullable();
            $table->string('adresseparent')->nullable();
            $table->string('occupationmere')->nullable();
            $table->string('occupationpere')->nullable();
            $table->string('telephonemere')->nullable();
            $table->string('telephonepere')->nullable();
            $table->string('emailmere')->nullable();
            $table->string('emailpere')->nullable();
            $table->string('userEmail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
