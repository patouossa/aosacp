<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMobileTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_transfers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('operatorName')->nullable();
            $table->string('mobileAccountName')->nullable();
            $table->string('mobile')->nullable();
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mobile_transfers');
    }
}
