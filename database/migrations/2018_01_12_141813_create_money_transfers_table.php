<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoneyTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_transfers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('compnayName')->nullable();
            $table->string('beneficaryName')->nullable();
            $table->string('mobile')->nullable();
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('money_transfers');
    }
}
