<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examinations', function(Blueprint $table) {
            $table->increments('id');
            $table->date('examinationyear')->nullable();
            $table->string('candidatenumber')->nullable();
            $table->string('examinationcenter')->nullable();
            $table->string('resultobtained')->nullable();
            $table->string('qualification')->nullable();
            $table->string('userEmail')->nullable();
            $table->string('studentId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examinations');
    }
}
