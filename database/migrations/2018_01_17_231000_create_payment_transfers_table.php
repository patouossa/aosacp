<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transfers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->string('adminId')->nullable();
            $table->string('adminEmail')->nullable();
            $table->date('dateDepot');
            $table->string('studentId');
            $table->string('student');
            $table->string('studentPhone');
            $table->decimal('amount');
            $table->string('reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_transfers');
    }
}
