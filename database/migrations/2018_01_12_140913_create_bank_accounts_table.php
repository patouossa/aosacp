<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('bankName')->nullable();
            $table->string('accountName')->nullable();
            $table->string('bankCode')->nullable();
            $table->string('brankCode')->nullable();
            $table->string('accountNumber')->nullable();
            $table->string('rip')->nullable();
            $table->string('institutionId')->nullable();
            $table->string('institution')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_accounts');
    }
}
