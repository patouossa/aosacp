<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCertificate extends Migration
{

/**
* Run the migrations.
*
* @return void
*/
    public function up()
    {
        Schema::table('certificates', function (Blueprint $table) {

            $table->string('handWriting')->nullable();
            $table->string('certifiedQualification')->nullable();
            $table->string('birth')->nullable();
            $table->string('transcriptgcea')->nullable();
            $table->string('applicationForm')->nullable();
            $table->string('cni')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certificates', function (Blueprint $table) {
            //
        });
    }
}
