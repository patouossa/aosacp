<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilieresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filieres', function(Blueprint $table) {
            $table->increments('id');
            $table->string('libelle')->nullable();
            $table->string('code')->nullable();
            $table->string('departementId')->nullable();
            $table->string('departemnent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('filieres');
    }
}
