-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- G�n�r� le: Mer 22 Novembre 2017 � 12:33
-- Version du serveur: 5.5.58-0ubuntu0.14.04.1
-- Version de PHP: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de donn�es: `aosa_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `departement_ins`
--

CREATE TABLE IF NOT EXISTS `departement_ins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institutId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institution` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `filieres`
--

CREATE TABLE IF NOT EXISTS `filieres` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departementId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departemnent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assets` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_type_id_foreign` (`type_id`),
  KEY `history_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 4, 'save', 'bg-aqua', 'trans("history.backend.roles.updated") Institution', NULL, '2017-11-16 12:46:45', '2017-11-16 12:46:45'),
(2, 2, 1, 5, 'save', 'bg-aqua', 'trans("history.backend.roles.updated") Student', NULL, '2017-11-17 11:25:11', '2017-11-17 11:25:11'),
(3, 1, 1, 10, 'plus', 'bg-green', 'trans("history.backend.users.created") ISTDI', NULL, '2017-11-21 14:16:15', '2017-11-21 14:16:15'),
(4, 1, 1, 11, 'plus', 'bg-green', 'trans("history.backend.users.created") ESG', NULL, '2017-11-21 14:18:20', '2017-11-21 14:18:20'),
(5, 1, 11, 12, 'plus', 'bg-green', 'trans("history.backend.users.created") UIT', NULL, '2017-11-21 17:49:52', '2017-11-21 17:49:52'),
(6, 1, 12, 13, 'plus', 'bg-green', 'trans("history.backend.users.created") TFS', NULL, '2017-11-22 05:01:51', '2017-11-22 05:01:51');

-- --------------------------------------------------------

--
-- Structure de la table `history_types`
--

CREATE TABLE IF NOT EXISTS `history_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2017-11-13 10:14:52', '2017-11-13 10:14:52'),
(2, 'Role', '2017-11-13 10:14:52', '2017-11-13 10:14:52');

-- --------------------------------------------------------

--
-- Structure de la table `institutions`
--

CREATE TABLE IF NOT EXISTS `institutions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `villeId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quartier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agreement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amountpreinscription` decimal(8,2) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Contenu de la table `institutions`
--

INSERT INTO `institutions` (`id`, `nom`, `description`, `regionId`, `region`, `villeId`, `ville`, `bp`, `quartier`, `telephone`, `agreement`, `type`, `created_at`, `updated_at`, `userId`, `userEmail`, `amountpreinscription`, `password`) VALUES
(1, 'ISTDI', 'Un �cole renomm� dans l''enseignement', '5', NULL, '11', 'DOUALA', '0909', 'Logbessou', '678877669', '00998888', 'PRIVATE', '2017-11-21 14:13:51', '2017-11-21 14:17:03', NULL, 'istdi@gmail.com', 10000.00, ''),
(6, 'ESG', 'Une des plus grandes �coles du cameroun', '3', NULL, '9', 'BERTOUA', '009', 'Bepanda', '678877667', 'zaezae', 'PRIVATE', '2017-11-21 14:18:20', '2017-11-21 14:18:20', NULL, 'esg@gmail.Com', 0.00, 'esg12345'),
(7, 'ISTA', 'Une des plus grandes �coles du cameroun', '2', 'Center, Cameroon', '15', 'ESEKA', '009', 'Bepanda', '678877667', 'zaezae', 'PRIVATE', '2017-11-21 14:18:20', '2017-11-21 14:48:19', '[11]', 'esg@gmail.Com', 0.00, 'esg12345'),
(8, 'UIT', 'IUT', '5', 'Littoral, Cameroon', '11', 'DOUALA', '009', 'Logbessou', '899988', 'zaezae', 'PARA-PUBLIC', '2017-11-21 17:49:52', '2017-11-21 17:49:52', '[12]', 'iut@gmail.com', 1233.00, 'iut1234'),
(9, 'TFS', 'TFS', '2', 'Center, Cameroon', '15', 'ESEKA', '009', 'Logbessou', '899988', 'zaezae', 'PRIVATE', '2017-11-22 05:01:51', '2017-11-22 05:01:51', '[13]', 'tfs@gmail.com', 12333.00, 'tfs12345');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_01_25_141613_create_posts_table', 1),
(7, '2017_08_03_091624_add_field_user', 2),
(8, '2017_11_16_101905_create_villes_table', 3),
(9, '2017_11_16_102039_create_regions_table', 3),
(10, '2017_11_16_103539_create_departement_ins_table', 3),
(11, '2017_11_16_104445_create_filieres_table', 3),
(12, '2017_11_16_121151_create_institutions_table', 3),
(13, '2017_11_16_160752_addFieldInstitution', 4),
(14, '2017_11_16_162650_addFieldDepartement', 5),
(15, '2017_11_16_162746_addFieldFiliere', 5),
(16, '2017_11_18_085232_addamountpreinsription', 6),
(17, '2017_11_21_143838_add_field_admin_school', 7);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('samuel.gildas12@gmail.com', 'a6fb5bfa1a58178f8fdb4f6eba2515ab5bec921fc6ce7832cff80c465c24ad01', '2017-11-13 12:36:14');

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2017-11-13 10:14:52', '2017-11-13 10:14:52'),
(2, 'manage-users', 'Manage Users', 2, '2017-11-13 10:14:52', '2017-11-13 10:14:52'),
(3, 'manage-roles', 'Manage Roles', 3, '2017-11-13 10:14:52', '2017-11-13 10:14:52'),
(4, 'manage-univ', 'INSTITUT SUPERIEUR', 4, '2017-11-13 10:14:52', '2017-11-13 10:14:52'),
(5, 'manage-student', 'ELEVE/ETUDIANT', 5, '2017-11-13 10:14:52', '2017-11-13 10:14:52');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2),
(2, 2, 2),
(5, 1, 4),
(6, 4, 4),
(7, 1, 5),
(8, 5, 5);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelleEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `libelleFR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `regions`
--

INSERT INTO `regions` (`id`, `libelleEN`, `libelleFR`, `created_at`, `updated_at`) VALUES
(1, 'Adamaoua, Cameroon', 'Adamaoua, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Center, Cameroon', 'Centre, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'East, Cameroon', 'Est, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Far North, Cameroon', 'Extreme Nord, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Littoral, Cameroon', 'Littoral, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'North, Cameroon', 'Nord, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'North West, Cameroon', 'Nord-Ouest, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'South, Cameroon', 'Sud, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'South-West, Cameroon', 'Sud-Ouest, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'West, Cameroon', 'Ouest, Cameroun', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-11-13 10:14:51', '2017-11-13 10:14:51'),
(2, 'Executive', 0, 2, '2017-11-13 10:14:51', '2017-11-13 10:14:51'),
(3, 'User', 0, 3, '2017-11-13 10:14:51', '2017-11-13 10:14:51'),
(4, 'Institution', 0, 4, '2017-11-13 10:14:51', '2017-11-16 12:46:45'),
(5, 'Student', 0, 5, '2017-11-13 10:14:51', '2017-11-17 11:25:11');

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Contenu de la table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(8, 8, 4),
(10, 10, 4),
(11, 11, 4),
(12, 12, 4),
(13, 13, 4);

-- --------------------------------------------------------

--
-- Structure de la table `social_logins`
--

CREATE TABLE IF NOT EXISTS `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `type`, `phone`) VALUES
(1, 'Admin Istrator', 'admin@admin.com', '$2y$10$/QHz9y0MfG684yLOw6eAt.n5ewDuBSdImy1JSy5XpDb0EYHNKk7eG', 1, '59c42dbaa52ee731e3400df042bf9ff5', 1, 'Qwv0yiMLrwd996UmNp7YWMamAnwuRAbI1mw7lc9jo116DjTUGUVSNhv55UH9', '2017-11-13 10:14:51', '2017-11-21 15:29:40', NULL, NULL, NULL),
(2, 'Backend User', 'executive@executive.com', '$2y$10$RCjrAB/1/u1KNM811KSi/uflpoTJ8vgl39l6xdOK/GU6tixxqjOHG', 1, '623718db7513f128e5c42de672a10c70', 1, NULL, '2017-11-13 10:14:51', '2017-11-13 10:14:51', NULL, NULL, NULL),
(3, 'Default User', 'user@user.com', '$2y$10$FrStyccTduC/MgFKQtg6Pemcea301TAqO0HbewT5NTh.ttMjaAek2', 1, '245030e034656be2ab93b27586642e00', 1, NULL, '2017-11-13 10:14:51', '2017-11-13 10:14:51', NULL, NULL, NULL),
(8, 'Samuel', 'samuel.gildas12@gmail.com', '$2y$10$9FetDDmaWnZ1.SwfP.SzAe7bGI/GL/gMmrqhwfQxF0Tx9vDAep0tm', 1, 'afb46c03d079ec7d05fbeccb5ca2e115', 1, 'UqV9WnG6vHbHlE4Z1kIR5m74q8VbYHY7huFHYBYLVrJojAddo8dK7BjHQkOS', '2017-11-13 12:27:55', '2017-11-18 08:45:22', NULL, 'manage-student', NULL),
(10, 'ISTDI', 'istdi@gmail.com', '$2y$10$vwFkY9yHofqabb3heEagmuT.mOH7et35Z1h6MNz9.bQnf1mTBYoEK', 1, '6cfbdfb3bef6ad3b9ba2fb28d6a8ea31', 1, NULL, '2017-11-21 14:16:14', '2017-11-21 14:16:14', NULL, NULL, NULL),
(11, 'ISTA', 'esg@gmail.Com', '$2y$10$mNmUV0omDdzKW2yVMc5uXehXBEcMq.OcPwkwaAURzCrktdIBWEJve', 1, '3f1e726766f90408e388b95f66c7107a', 1, NULL, '2017-11-21 14:18:20', '2017-11-21 14:47:15', NULL, NULL, NULL),
(12, 'UIT', 'iut@gmail.com', '$2y$10$SXRG2yKqoCPkLzJE3HI3puEKUT9VhKsS7DR/unpIxTEVUmKcZY0oi', 1, '32cd49825dde247d1316c6aab3b1c6d2', 1, 'SS5mGVvAKwj72oEsBQaxCKwqlJkuvXLqm4bNpe5H6CkYEKYkmoamJ2DtpvoV', '2017-11-21 17:49:52', '2017-11-22 05:02:17', NULL, NULL, NULL),
(13, 'TFS', 'tfs@gmail.com', '$2y$10$v6Xs8Npb.DOH7sGmPE9EwuzCazH4pNf5VSENDFyEMuA2to8P7VQTe', 1, '4ad956d899e968f64f94e7ad106195cd', 1, NULL, '2017-11-22 05:01:51', '2017-11-22 05:01:51', NULL, 'manage-univ', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

CREATE TABLE IF NOT EXISTS `villes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `libelleRegion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Contenu de la table `villes`
--

INSERT INTO `villes` (`id`, `libelle`, `regionId`, `libelleRegion`, `created_at`, `updated_at`) VALUES
(1, 'AKONOLINGA', '2', 'Center, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(2, 'AMBAM', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(3, 'BAFANG', '10', 'West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(4, 'BAFOUSSAM', '10', 'West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(5, 'BAMENDA', '7', 'North West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(6, 'BANDJOUN', '10', 'West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(7, 'BARDANKE', '6', 'North, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(8, 'BENGBIS', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(9, 'BERTOUA', '3', 'East, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(10, 'BUEA', '9', 'South West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(11, 'DOUALA', '5', 'Littoral, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(12, 'DSCHANG', '10', 'West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(13, 'EBOLOWA', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(14, 'EDEA', '5', 'Littoral, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(15, 'ESEKA', '2', 'Center, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(16, 'GAROUA', '6', 'North, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(17, 'KRIBI', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(18, 'KUMBA ', '9', 'South West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(19, 'LIMBE', '9', 'South West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(20, 'LOLODORF', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(21, 'MBOUDA', '10', 'West, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(22, 'MEYOMESSALA', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(23, 'NGAOUNDERE', '1', 'Adamaoua, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(24, 'NKONGSAMBA', '5', 'Littoral, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(25, 'SANGMELIMA', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(26, 'YAOUNDE', '2', 'Center, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(27, 'ZOETELE', '8', 'South, Cameroon', '2017-09-19 14:20:56', '2017-09-19 14:20:56'),
(43, 'LOLA', '2', 'Center, Cameroon', '2017-10-16 10:44:01', '2017-10-16 10:44:01');

--
-- Contraintes pour les tables export�es
--

--
-- Contraintes pour la table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
