@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-bank.management') . ' | ' . trans('labels.backend.access.payment-bank.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-bank.management') }}
    <small>{{ trans('labels.backend.access.payment-bank.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">payment-bank</h3>
        <div class="box-tools pull-right">

            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Slip Number </th><th> Date </th><th> Student </th><th> Student Phone </th><th> Amount </th><th> Reference </th><th> Transaction Number </th>@if(access()->user()->type == null)<th>Insitution</th>@endif </th><th> Scan deposit Slip </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($paymentbank as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->slipNumber }}</td><td>{{ $item->dateDepot }}</td><td>{{ $item->student }}</td><td>{{ $item->studentPhone }}</td><td>{{ $item->amount }}</td><td>{{ $item->reference }}</td><td>{{ $item->transactionNumber }}</td>@if(access()->user()->type == null)<td>{{ $item->institution }}</td>@endif<td>
                            <a target="_blank" href="{{ $item->scanDepositSlip }}">Scan Deposit Slip</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $paymentbank->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection