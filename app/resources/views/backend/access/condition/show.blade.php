@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.condition.management') . ' | ' . trans('labels.backend.access.condition.show'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.condition.management') }}
        <small>{{ trans('labels.backend.access.condition.show') }}</small>
    </h1>
@endsection

@section('content')


    <div class="box box-success">

        <div class="box-body">

            @if($bankaccount)
                <a href="{{ url('/admin/access/payment-bank/create?id='.$bankaccount->id) }}"
                   class="btn btn-primary btn-xs" title="Bank Account"><span
                            class="glyphicon glyphicon-usd" aria-hidden="true"/> Bank Account</a>
            @endif

                @if($moneytransfer)
                    <a href="{{ url('/admin/access/money-transfer/create?id='.$moneytransfer->id) }}"
                       class="btn btn-primary btn-xs" title="Money Transfer"><span
                                class="glyphicon glyphicon-usd" aria-hidden="true"/>Money Transfer </a>
                @endif

                @if($expresscash)
                    <a href="{{ url('/admin/access/express-cash-wallet/create?id='.$expresscash->id) }}"
                       class="btn btn-primary btn-xs" title="Express Cash Account"><span
                                class="glyphicon glyphicon-usd" aria-hidden="true"/> Express Cash Account</a>
                @endif

                @if($mobiletransfer)
                    <a href="{{ url('/admin/access/mobile-transfer/create?id='.$mobiletransfer->id) }}"
                       class="btn btn-primary btn-xs" title="Mobile Transfer"><span
                                class="glyphicon glyphicon-usd" aria-hidden="true"/> Mobile Transfer</a>
                @endif


        </div> <!-- /.box-body -->

    </div>


@endsection
