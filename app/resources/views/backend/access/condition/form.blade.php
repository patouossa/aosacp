<div class="form-group {{ $errors->has('institutionId') ? 'has-error' : ''}}">
    {!! Form::label('institutionId', 'Institutionid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('institutionId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('institutionId', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('institution') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Institution', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('institution', null, ['class' => 'form-control']) !!}
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('adminId') ? 'has-error' : ''}}">
    {!! Form::label('adminId', 'Adminid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('adminId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('adminId', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('amountInstitution') ? 'has-error' : ''}}">
    {!! Form::label('amountInstitution', 'Amountinstitution', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('amountInstitution', null, ['class' => 'form-control']) !!}
        {!! $errors->first('amountInstitution', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('amountAdmin') ? 'has-error' : ''}}">
    {!! Form::label('amountAdmin', 'Amountadmin', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('amountAdmin', null, ['class' => 'form-control']) !!}
        {!! $errors->first('amountAdmin', '<p class="help-block">:message</p>') !!}
    </div>
</div>

