@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.condition.management') . ' | ' . trans('labels.backend.access.condition.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.condition.management') }}
        <small>{{ trans('labels.backend.access.condition.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Condition</h3>
            <div class="box-tools pull-right">

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Institution</th>
                        <th> Application Fees</th>
                        <th> Web Admin Fees</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($condition as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->institution }}

                            </td>

                            <td>{{ $item->amountInstitution }}
                            @if($item->status == 1)
                                <!-- Single button -->
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Paid <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li> @if($item->bankAccountId)
                                                    <a href="{{ url('/admin/access/payment-bank/create?id='.$item->bankAccountId.'&cond='.$item->id) }}"
                                                    > Bank Account</a>
                                                @endif</li>
                                            <li>@if($item->moneyTransferId)
                                                    <a href="{{ url('/admin/access/money-transfer/create?id='.$item->moneyTransferId.'&cond='.$item->id) }}"
                                                    >Money Transfer </a>
                                                @endif</li>
                                            <li>@if($item->expresscashId)
                                                    <a href="{{ url('/admin/access/express-cash-wallet/create?id='.$item->expresscashId.'&cond='.$item->id) }}"
                                                    > Express Cash Account</a>
                                                @endif</li>

                                            <li>@if($item->mobileTransferId)
                                                    <a href="{{ url('/admin/access/mobile-transfer/create?id='.$item->mobileTransferId.'&cond='.$item->id) }}"
                                                    > Mobile Transfer</a>
                                                @endif</li>
                                        </ul>
                                    </div>
                                @endif
                            </td>


                            <td>{{ $item->amountAdmin }}

                            @if($item->status == 0)
                                <!-- Single button -->
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-xs dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Paid <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ url('/admin/access/payment-bank/create?id='.$item->bankAccountAdminId.'&cond='.$item->id) }}"
                                                > Bank Account</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/admin/access/money-transfer/create?id='.$item->moneyTransferAdminId.'&cond='.$item->id) }}"
                                                >Money Transfer </a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/admin/access/express-cash-wallet/create?id='.$item->expresscashAdminId.'&cond='.$item->id) }}"
                                                > Express Cash Account</a>
                                            </li>

                                            <li>
                                                <a href="{{ url('/admin/access/mobile-transfer/create?id='.$item->mobileTransferAdminId.'&cond='.$item->id) }}"
                                                > Mobile Transfer</a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </td>
                            <td>

                                @if($item->status == 2)
                                    <a class="btn btn-primary btn-xs" href="{{ url('/admin/access/student') }}"
                                    > Submit Application</a>

                                @endif
                                {!! Form::open([
                                   'method'=>'DELETE',
                                   'url' => ['/admin/access/condition', $item->id],
                                   'style' => 'display:inline'
                               ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Condition" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Condition',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $condition->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection