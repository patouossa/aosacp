@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.school-information.management') . ' | ' . trans('labels.backend.access.school-information.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.school-information.management') }}
    <small>{{ trans('labels.backend.access.school-information.edit') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::model($schoolinformation, [
            'method' => 'PATCH',
            'url' => ['/admin/access/school-information', $schoolinformation->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.school-information.edit') }} - {{ $schoolinformation->id }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        @include ('backend/access.school-information.form')

        <div class="box-footer">
            {{ link_to_route('admin.access.school-information.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}
            {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary pull-right']) }}
        </div>   <!-- /.box-footer -->

        {!! Form::close() !!}
    </div> <!-- /.box-body -->
        <div class="box-body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>ID</th><th> Startyear </th><th> Endyear </th><th> Nameschool </th><th> Qualification </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schoolinformations as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->startyear }}</td><td>{{ $item->endyear }}</td><td>{{ $item->nameschool }}</td><td>{{ $item->qualification }}</td>
                        <td>
                            <a href="{{ url('/admin/access/school-information/' . $item->id) }}" class="btn btn-success btn-xs" title="View SchoolInformation"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/school-information/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SchoolInformation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/school-information', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete SchoolInformation" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete SchoolInformation',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $schoolinformations->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     

    
</div> 


@endsection