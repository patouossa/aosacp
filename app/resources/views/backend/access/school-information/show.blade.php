@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.school-information.management') . ' | ' . trans('labels.backend.access.school-information.show'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.school-information.management') }}
    <small>{{ trans('labels.backend.access.school-information.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.school-information.show') }} - {{ $schoolinformation->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/school-information/' . $schoolinformation->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SchoolInformation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/school-information', $schoolinformation->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'SchoolInformation',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $schoolinformation->id }}</td>
                    </tr>
                    <tr><th> Startyear </th><td> {{ $schoolinformation->startyear }} </td></tr><tr><th> Endyear </th><td> {{ $schoolinformation->endyear }} </td></tr><tr><th> Nameschool </th><td> {{ $schoolinformation->nameschool }} </td></tr><tr><th> Qualification </th><td> {{ $schoolinformation->qualification }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.school-information.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
