@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.school-information.management') . ' | ' . trans('labels.backend.access.school-information.create'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.school-information.management') }}
    <small>{{ trans('labels.backend.access.school-information.create') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/school-information', 'class' => 'form-horizontal', 'files' => true]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ url('/admin/access/student/' . $student->id . '/edit') }}" class="btn btn-primary btn-xs" title="{{trans('buttons.general.crud.go_to_back_to_registration')}}">{{trans('buttons.general.crud.go_to_back_to_registration')}}</a>
        </h3>
        <div class="box-tools pull-right">
            @if($count >0)
            <a href="{{ url('/admin/access/examination/create') }}" class="btn btn-primary " title="{{trans('buttons.general.crud.go_to_examination')}}"><span class="glyphicon glyphicon-next" aria-hidden="true"/>{{trans('buttons.general.crud.go_to_examination')}}</a>
            @endif
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">        
        
        @include ('backend/access.school-information.form')

    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{--{{ link_to_route('admin.access.school-information.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}--}}
        {{ Form::submit(trans('buttons.general.crud.create_school'), ['class' => 'btn btn-primary pull-right']) }}
    </div>   <!-- /.box-footer -->
    {!! Form::close() !!}
    <div class="box-body">
    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
            <tr>
                <th>ID</th><th> Startyear </th><th> Endyear </th><th> Nameschool </th><th> Qualification </th><th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($schoolinformations as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->startyear }}</td><td>{{ $item->endyear }}</td><td>{{ $item->nameschool }}</td><td>{{ $item->qualification }}</td>
                    <td>
                        <a href="{{ url('/admin/access/school-information/' . $item->id) }}" class="btn btn-success btn-xs" title="View SchoolInformation"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/admin/access/school-information/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SchoolInformation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $schoolinformations->render() !!} </div>
    </div>
    </div> <!-- /.box-body -->
    
</div> 


@endsection