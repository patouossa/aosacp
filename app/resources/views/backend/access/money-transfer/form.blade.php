<div class="form-group {{ $errors->has('compnayName') ? 'has-error' : ''}}">
    {!! Form::label('compnayName', 'Company name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('compnayName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('compnayName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('beneficaryName') ? 'has-error' : ''}}">
    {!! Form::label('beneficaryName', 'Beneficary name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('beneficaryName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('beneficaryName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
