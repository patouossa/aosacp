@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-expresscash.management') . ' | ' . trans('labels.backend.access.payment-expresscash.show'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-expresscash.management') }}
    <small>{{ trans('labels.backend.access.payment-expresscash.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.payment-expresscash.show') }} - {{ $paymentexpresscash->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/payment-expresscash/' . $paymentexpresscash->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit PaymentExpresscash"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/payment-expresscash', $paymentexpresscash->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'PaymentExpresscash',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $paymentexpresscash->id }}</td>
                    </tr>
                    <tr><th> SlipNumber </th><td> {{ $paymentexpresscash->slipNumber }} </td></tr><tr><th> DateDepot </th><td> {{ $paymentexpresscash->dateDepot }} </td></tr><tr><th> StudentId </th><td> {{ $paymentexpresscash->studentId }} </td></tr><tr><th> Student </th><td> {{ $paymentexpresscash->student }} </td></tr><tr><th> StudentPhone </th><td> {{ $paymentexpresscash->studentPhone }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.payment-expresscash.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
