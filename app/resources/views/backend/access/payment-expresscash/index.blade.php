@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-expresscash.management') . ' | ' . trans('labels.backend.access.payment-expresscash.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-expresscash.management') }}
    <small>{{ trans('labels.backend.access.payment-expresscash.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">payment-expresscash</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/payment-expresscash/create') }}" class="btn btn-primary " title="Add New PaymentExpresscash"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>ID</th><th> Slip Number </th><th> Date </th><th> Student </th><th> Student Phone </th><th> Amount </th><th> Reference </th><th> Transaction Number </th>@if(access()->user()->type == null)<th>Insitution</th>@endif </th><th> Scan deposit Slip </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($paymentexpresscash as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->slipNumber }}</td><td>{{ $item->dateDepot }}</td><td>{{ $item->student }}</td><td>{{ $item->studentPhone }}</td><td>{{ $item->amount }}</td><td>{{ $item->reference }}</td><td>{{ $item->transactionNumber }}</td>@if(access()->user()->type == null)<td>{{ $item->institution }}</td>@endif<td>
                            <a target="_blank" href="{{ $item->scanDepositSlip }}"></a></td>
                        <td>
                            <a href="{{ url('/admin/access/payment-expresscash/' . $item->id) }}" class="btn btn-success btn-xs" title="View PaymentExpresscash"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/payment-expresscash/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit PaymentExpresscash"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/payment-expresscash', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete PaymentExpresscash" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete PaymentExpresscash',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $paymentexpresscash->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection