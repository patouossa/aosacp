@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.institution.management') . ' | ' . trans('labels.backend.access.institution.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.institution.management') }}
    <small>{{ trans('labels.backend.access.institution.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.institution.show') }} - {{ $institution->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/institution/' . $institution->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Institution"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/institution', $institution->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Institution',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $institution->id }}</td>
                    </tr>
                    <tr><th> Nom </th><td> {{ $institution->nom }} </td></tr><tr><th> Description </th><td> {{ $institution->description }} </td></tr><tr><th> Region </th><td> {{ $institution->region }} </td></tr><tr><th> Ville </th><td> {{ $institution->ville }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.institution.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
