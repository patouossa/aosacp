@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.institution.management') . ' | ' . trans('labels.backend.access.institution.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.institution.management') }}
    <small>{{ trans('labels.backend.access.institution.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Institution</h3>
        <div class="box-tools pull-right">
            @permissions(['manage-users', 'manage-roles'])
            <a href="{{ url('/admin/access/institution/create') }}" class="btn btn-primary " title="Add New Institution"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            @endauth
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Nom </th><th> Description </th><th> Region </th><th> Ville </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($institution as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nom }}</td><td>{{ $item->description }}</td><td>{{ $item->region }}</td><td>{{ $item->ville }}</td>
                        <td>
                            <a href="{{ url('/admin/access/institution/' . $item->id) }}" class="btn btn-success btn-xs" title="View Institution"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/institution/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Configurations"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>

                            @permissions(['manage-users', 'manage-roles'])
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/institution', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Institution" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Institution',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                            @endauth
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $institution->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection