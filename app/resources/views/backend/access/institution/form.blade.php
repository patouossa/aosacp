@permissions(['manage-users', 'manage-roles'])

<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!$edit)
    <div class="form-group">
        {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('email', 'userEmail', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->

    <div class="form-group">
        {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
        <div class="col-md-6">
            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
        </div><!--col-md-6-->
    </div><!--form-group-->
@endif
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('region') ? 'has-error' : ''}}">
    {!! Form::label('region', trans('labels.frontend.region'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control region" name="regionId" required>
            <option value="" selected disabled>{{ trans('labels.frontend.select_region') }}</option>
            @foreach($region as $item)

                @if ( Config::get('app.locale') == 'en')
                    <option value="{{$item->id}}" {{ ($institution != '' && $institution->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleEN}}</option>
                @else
                    <option value="{{$item->id}}" {{ ($institution != '' && $institution->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleFR}}</option>
                @endif

            @endforeach
        </select>
        {!! $errors->first('region', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('ville', trans('labels.frontend.ville'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control ville" id="ville" name="villeId" required>
            <option value="" selected disabled>{{ trans('labels.frontend.select_ville') }}</option>
            @foreach($ville as $item)
                <option value="{{$item->id}}" {{ ($institution != '' && $institution->villeId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
            @endforeach
        </select>
        {!! $errors->first('ville', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('bp') ? 'has-error' : ''}}">
    {!! Form::label('bp', 'Bp', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bp', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bp', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('quartier') ? 'has-error' : ''}}">
    {!! Form::label('quartier', 'Quartier', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('quartier', null, ['class' => 'form-control']) !!}
        {!! $errors->first('quartier', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
    {!! Form::label('telephone', 'Telephone', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
        {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('agreement') ? 'has-error' : ''}}">
    {!! Form::label('agreement', 'Autorisation', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('agreement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('agreement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('jour') ? 'has-error' : ''}}">
    {!! Form::label('jour', trans('labels.frontend.status'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {{--{!! Form::select('jour', ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'], null, ['class' => 'form-control']) !!}--}}
        <select class="form-control" name="type">
            <option value="" selected disabled>{{trans('labels.frontend.select_status')}}</option>
            <option value="{{trans('labels.frontend.public')}}" {{ ($institution != '' && $institution->type==  trans('labels.frontend.public')) ? 'selected="selected"' : ''}}>{{trans('labels.frontend.public')}}</option>
            <option value="{{trans('labels.frontend.para-public')}}" {{ ($institution != '' && $institution->type==  trans('labels.frontend..para-public')) ? 'selected="selected"' : ''}}>{{trans('labels.frontend.para-public')}}</option>
            <option value="{{trans('labels.frontend.private')}}" {{ ($institution != '' && $institution->type==  trans('labels.frontend.private')) ? 'selected="selected"' : ''}}>{{trans('labels.frontend.private')}}</option>


        </select>
        {!! $errors->first('jour', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endauth

@if(access()->user()->type == 'manage-univ')
<div class="form-group {{ $errors->has('beginDate') ? 'has-error' : ''}}">
    {!! Form::label('beginDate', 'Begin date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('beginDate', null, ['class' => 'form-control']) !!}
        {!! $errors->first('beginDate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('endDate') ? 'has-error' : ''}}">
    {!! Form::label('endDate', 'End date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('endDate', null, ['class' => 'form-control']) !!}
        {!! $errors->first('endDate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('nbChoise', 'Number choise of program study', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('nbChoise', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nbChoise', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}} amount">
    {!! Form::label('amountpreinscription', 'Application Fees from Local', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('amountpreinscriptionLocal', null, ['class' => 'form-control']) !!}
        {!! $errors->first('amountpreinscription', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}} amount">
    {!! Form::label('amountpreinscription', 'Application Fees from Remote', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('amountpreinscriptionRemote', null, ['class' => 'form-control']) !!}
        {!! $errors->first('amountpreinscription', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

    jQuery(document).ready(function ($) {
        $('.region').change(function () {
            $.get("{{ url('admin/access/region/ville/all')}}",
                    {region: $(this).val()},
                    function (data) {
                        var model = $('#ville');
                        model.empty();
                        $.each(data, function (index, element) {

                            model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                        });
                    });
        });
    });

   /* $(document).ready(function () {
        $('input[type="radio"]').click(function () {
            if ($(this).attr('class') == 'yes') {
                $('.paietype').show();
                $('.amount').show();

            }

            else if ($(this).attr('class') == 'no') {
                $('.paietype').hide();
                $('.amount').hide();
                $('.cash').hide();
                $('.bank').hide();
            }
        });

    });

    $(function () {

        $(".expresscach").on("click", function () {
            $(".cash").toggle(this.checked);
        });

        $(".bankpaiement").on("click", function () {
            $(".bank").toggle(this.checked);
        });
    });*/
</script>