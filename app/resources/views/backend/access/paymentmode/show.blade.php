@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.paymentmode.management') . ' | ' . trans('labels.backend.access.paymentmode.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.paymentmode.management') }}
    <small>{{ trans('labels.backend.access.paymentmode.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.paymentmode.show') }} - {{ $paymentmode->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/paymentmode/' . $paymentmode->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Paymentmode"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/paymentmode', $paymentmode->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Paymentmode',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $paymentmode->id }}</td>
                    </tr>
                    <tr><th> TypePaiementId </th><td> {{ $paymentmode->typePaiementId }} </td></tr><tr><th> TypePaiement </th><td> {{ $paymentmode->typePaiement }} </td></tr><tr><th> Montant </th><td> {{ $paymentmode->montant }} </td></tr><tr><th> InstitutionId </th><td> {{ $paymentmode->institutionId }} </td></tr><tr><th> Institution </th><td> {{ $paymentmode->institution }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.paymentmode.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
