<div class="form-group {{ $errors->has('typePaiementId') ? 'has-error' : ''}}">
    {!! Form::label('typePaiementId', 'Typepaiementid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control typePaiementId" id="typePaiementId" name="typePaiementId" required>
            <option value="" selected disabled>Type de paiement</option>
            @foreach($billingConfig as $item)
                <option value="{{$item->id}}" {{ ($paymentmode != '' && $paymentmode->typePaiementId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->type}}</option>
            @endforeach
        </select>

    </div>
</div>
<div class="form-group {{ $errors->has('montant') ? 'has-error' : ''}}">
    {!! Form::label('montant', 'Montant', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('montant', null, ['class' => 'form-control']) !!}
        {!! $errors->first('montant', '<p class="help-block">:message</p>') !!}
    </div>
</div>
