@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.programstudie.management') }}
    <small>{{ trans('labels.backend.access.programstudie.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.programstudie.show') }} - {{ $programstudie->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/programstudie/' . $programstudie->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Programstudie"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/programstudie', $programstudie->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Programstudie',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $programstudie->id }}</td>
                    </tr>
                    <tr><th> InstitutionId </th><td> {{ $programstudie->institutionId }} </td></tr><tr><th> Institution </th><td> {{ $programstudie->institution }} </td></tr><tr><th> DepartementId </th><td> {{ $programstudie->departementId }} </td></tr><tr><th> Departement </th><td> {{ $programstudie->departement }} </td></tr><tr><th> NiveauId </th><td> {{ $programstudie->niveauId }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.programstudie.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
