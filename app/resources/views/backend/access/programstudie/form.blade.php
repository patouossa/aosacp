<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', trans('labels.frontend.institution'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control institution" id="institution" name="institutionId" required>

            @if($result == false)
                <option value="" selected disabled>Selectionner l'institution</option>
                @foreach($institution as $item)
                    <option value="{{$item->id}}" {{ ($programstudie != '' && $programstudie->institutionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
                @endforeach
            @else
                <option value="{{$instit->id}}" selected readonly>{{$instit->nom}}</option>
            @endif

        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>

            <div class="form-group {{ $errors->has('faculte') ? 'has-error' : ''}}">
                {!! Form::label('institution', 'Faculté', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <select class="form-control faculte" id="faculte" name="faculteId" required>
                        <option value="" selected disabled>Selectionner la faculté</option>
                        @foreach($facultes as $item)
                            <option value="{{$item->id}}" {{ ($programstudie != '' && $programstudie->faculteId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
                {!! Form::label('institution', trans('labels.frontend.departement'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <select class="form-control departement" id="departement" name="departementId" required>
                        <option value="" selected disabled>Selectionner le département</option>
                        @foreach($departementins as $item)
                            <option value="{{$item->id}}" {{ ($programstudie != '' && $programstudie->departementId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
                {!! Form::label('niveau', 'Diplôme', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <select class="form-control niveau" id="niveau" name="niveauId" required>
                        <option value="" selected disabled>Selectionner le Diplôme</option>
                        @foreach($niveaux as $item)
                            <option value="{{$item->id}}" {{ ($programstudie != '' && $programstudie->niveauId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->niveau}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('niveau', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('filiere') ? 'has-error' : ''}}">
                {!! Form::label('filiere', 'Filière', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <select class="form-control filiere" id="filiere" name="filiereId" required>
                        <option value="" selected disabled>Select la Filière</option>
                        @foreach($filiere as $item)
                            <option value="{{$item->id}}" {{ ($programstudie != '' && $programstudie->filiereId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('niveau', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('filiere') ? 'has-error' : ''}}">
                <div class="col-md-4">

                </div>
                <div class="col-md-6">
                    <label><span class="label label-warning" id="place"></span></label>
                </div>
            </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

  /*  var acc = document.getElementsByClassName("accordion");
    var i;

    acc[0].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });*/


        $('.institution').change(function () {
            $.get("{{ url('admin/access/institution/faculte/all')}}",
                {institution: $(this).val()},
                function (data) {
                    var model = $('#faculte');
                    model.empty();
                    model.append("<option value=''>Selectionner la faculté</option>");
                    $.each(data, function (index, element) {
                        model.append("<option value='" + element.id + "'>" + element.nom + "</option>");
                    });
                });
        });



        $('.faculte').change(function () {

            $.get("{{ url('admin/access/faculte/departement/all')}}",
                {faculte: $(this).val()},
                function (data) {
                    var model = $('#departement');
                    model.empty();
                    model.append("<option value=''>Selectionner le departement</option>");
                    $.each(data, function (index, element) {
                        model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                    });
                });
        });

        $('.departement').change(function () {
            $.get("{{ url('admin/access/departement/niveau/all')}}",
                {departement: $(this).val()},
                function (data) {
                    var model = $('#niveau');
                    model.empty();
                    model.append("<option value=''>Selectionner le Diplôme</option>");
                    $.each(data, function (index, element) {

                        model.append("<option value='" + element.id + "'>" + element.niveau + "</option>");
                    });
                });
        });

        $('.niveau').change(function () {
            $.get("{{ url('admin/access/niveau/filiere/all')}}",
                {niveau: $(this).val()},
                function (data) {
                    var model = $('#filiere');
                    model.empty();
                    model.append("<option value=''>Selectionner la filière</option>");
                    $.each(data, function (index, element) {

                        model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                    });
                });
        });

        $('.filiere').change(function () {
            $.get("{{ url('admin/access/filiere/place/all')}}",
                {filiere: $(this).val()},
                function (data) {
                    var model = $('#place');
                    model.empty();
                    console.log(data)

                    model.append('<label class="label label-warning" id="place">' + data.nbPlace + ' place(s) disponibles</label>');


                });
        });

</script>
