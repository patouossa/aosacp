@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.examination.management') . ' | ' . trans('labels.backend.access.examination.create')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.examination.management') }}
    <small>{{ trans('labels.backend.access.examination.create') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/examination', 'class' => 'form-horizontal', 'files' => true]) !!} 
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><a href="{{ url('/admin/access/school-information/create') }}" class="btn btn-primary btn-xs" title="{{trans('buttons.general.crud.go_to_back_to_school_info')}}">{{trans('buttons.general.crud.go_to_back_to_school_info')}}</a>
        </h3>

        <div class="box-tools pull-right">
            @if($count > 0)
            <a href="{{ url('/admin/access/student') }}" class="btn btn-primary " title="{{trans('buttons.general.crud.save_phase1')}}"><span class="glyphicon glyphicon-next" aria-hidden="true"/>{{trans('buttons.general.crud.save_phase1')}}</a>
            @endif
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">        
        
        @include ('backend/access.examination.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{--{{ link_to_route('admin.access.examination.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}--}}
        {{ Form::submit(trans('buttons.general.crud.create_exam'), ['class' => 'btn btn-primary pull-right']) }}
    </div>   <!-- /.box-footer -->
    {!! Form::close() !!}

    <div class="box-body">

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>ID</th><th> Examination year </th><th> Candidate number </th><th> Examination center </th><th> Result obtained </th><th> Qualification </th><th>Sitting</th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($examinations as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->examinationyear }}</td><td>{{ $item->candidatenumber }}</td><td>{{ $item->examinationcenter }}</td><td>{{ $item->resultobtained }}</td><td>{{ $item->qualification }}</td><td>{{ $item->nbSitting }}</td>
                        <td>
                            <a href="{{ url('/admin/access/examination/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Examination"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $examinations->render() !!} </div>
        </div>

    </div> <!-- /.box-body -->
</div> 


@endsection