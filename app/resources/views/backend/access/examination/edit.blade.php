@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.examination.management') . ' | ' . trans('labels.backend.access.examination.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.examination.management') }}
    <small>{{ trans('labels.backend.access.examination.edit') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::model($examination, [
            'method' => 'PATCH',
            'url' => ['/admin/access/examination', $examination->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.examination.edit') }} - {{ $examination->id }}</h3>
        <div class="box-tools pull-right">
            @if($count > 0)
                <a href="{{ url('/admin/access/student') }}" class="btn btn-primary " title="Examinations"><span class="glyphicon glyphicon-next" aria-hidden="true"/>{{trans('buttons.general.crud.save_phase1')}}</a>
            @endif
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        @include ('backend/access.examination.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{ link_to_route('admin.access.examination.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}
        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary pull-right']) }}                
    </div>   <!-- /.box-footer -->

    {!! Form::close() !!}

    <div class="box-body">

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>ID</th><th> Examinationyear </th><th> Candidatenumber </th><th> Examinationcenter </th><th> Resultobtained </th><th> Qualification </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($examinations as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->examinationyear }}</td><td>{{ $item->candidatenumber }}</td><td>{{ $item->examinationcenter }}</td><td>{{ $item->resultobtained }}</td><td>{{ $item->qualification }}</td>
                        <td>
                            <a href="{{ url('/admin/access/examination/' . $item->id) }}" class="btn btn-success btn-xs" title="View Examination"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/examination/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Examination"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/examination', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Examination" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Examination',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $examinations->render() !!} </div>
        </div>

    </div> <!-- /.box-body -->
</div> 

@endsection