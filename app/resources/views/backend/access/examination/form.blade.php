<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Qualification', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control qualifcation" id="schoolInformationId" name="schoolInformationId" required>
            <option value="" selected disabled>Select a Qualification</option>
            @foreach($schoolInformations as $item)
                <option value="{{$item->qualification}}" {{ ($examination != '' && $examination->schoolInformationId==  $item->qualification) ? 'selected="selected"' : '' }} >{{$item->qualification}}</option>
            @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('nbSitting') ? 'has-error' : ''}}">
    {!! Form::label('nbSitting', 'Number of sitting', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6 select">
        <select class="form-control number-offices" id="number-offices" name="nbSitting" required>
            <option value="" selected disabled>Select number of sitting</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        {!! $errors->first('nbSitting', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div id="copy-area" class="copy-area">
    <div class="register-form-extra">
        <label class="accordion">Sitting</label>
        <div class="panel1">

            <div class="form-group {{ $errors->has('examinationyear') ? 'has-error' : ''}}">
                {!! Form::label('examinationyear', 'Examinationyear', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::date('examinationyear[]', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('examinationyear', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('candidatenumber') ? 'has-error' : ''}}">
                {!! Form::label('candidatenumber', 'Candidate number', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('candidatenumber[]', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('candidatenumber', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('examinationcenter') ? 'has-error' : ''}}">
                {!! Form::label('examinationcenter', 'Examination center', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('examinationcenter[]', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('examinationcenter', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('resultobtained') ? 'has-error' : ''}}">
                {!! Form::label('resultobtained', 'Result obtained', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('resultobtained[]', null, ['class' => 'form-control obtained']) !!}
                    {!! $errors->first('resultobtained', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="o-level">

                <div class="form-group {{ $errors->has('mathematic') ? 'has-error' : ''}}">
                    {!! Form::label('mathematic', 'Mathematics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="mathematic[]">
                            <option value="A" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('english') ? 'has-error' : ''}}">
                    {!! Form::label('english', 'English', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="english[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('englishLitt') ? 'has-error' : ''}}">
                    {!! Form::label('englishLitt', 'English Littarature', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="englishLitt[]">
                            <option value="" selected >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('french') ? 'has-error' : ''}}">
                    {!! Form::label('french', 'French', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="french[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('economic') ? 'has-error' : ''}}">
                    {!! Form::label('french', 'Economics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="economic[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('geography') ? 'has-error' : ''}}">
                    {!! Form::label('geography', 'Geography', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="geography[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('biology') ? 'has-error' : ''}}">
                    {!! Form::label('biology', 'Biology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="biology[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('humanBiology') ? 'has-error' : ''}}">
                    {!! Form::label('humanBiology', 'Human Biology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="humanBiology[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('chemistry') ? 'has-error' : ''}}">
                    {!! Form::label('chemistry', 'Chemistry', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="chemistry[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('physic') ? 'has-error' : ''}}">
                    {!! Form::label('physic', 'Physics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="physic[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('futherMaths') ? 'has-error' : ''}}">
                    {!! Form::label('futherMaths', 'Futher Mathmatics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="futherMaths[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('history') ? 'has-error' : ''}}">
                    {!! Form::label('history', 'History', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="history[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('history') ? 'has-error' : ''}}">
                    {!! Form::label('computerTech', 'Computer Technology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="computerTech[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

            </div>


            <div class="a-level">

                <div class="form-group {{ $errors->has('mathematic') ? 'has-error' : ''}}">
                    {!! Form::label('mathematic', 'Mathematics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="mathematic[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('english') ? 'has-error' : ''}}">
                    {!! Form::label('english', 'English', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="english[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('englishLitt') ? 'has-error' : ''}}">
                    {!! Form::label('englishLitt', 'English Littarature', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="englishLitt[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('french') ? 'has-error' : ''}}">
                    {!! Form::label('french', 'French', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="french[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('economic') ? 'has-error' : ''}}">
                    {!! Form::label('french', 'Economics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="economic[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('geography') ? 'has-error' : ''}}">
                    {!! Form::label('geography', 'Geography', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="geography[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('biology') ? 'has-error' : ''}}">
                    {!! Form::label('biology', 'Biology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="biology[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('humanBiology') ? 'has-error' : ''}}">
                    {!! Form::label('humanBiology', 'Human Biology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="humanBiology[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('chemistry') ? 'has-error' : ''}}">
                    {!! Form::label('chemistry', 'Chemistry', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="chemistry[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('physic') ? 'has-error' : ''}}">
                    {!! Form::label('physic', 'Physics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="physic[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('futherMaths') ? 'has-error' : ''}}">
                    {!! Form::label('futherMaths', 'Futher Mathmatics', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="futherMaths[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('history') ? 'has-error' : ''}}">
                    {!! Form::label('history', 'History', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">

                        <select class="form-control" name="history[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('history') ? 'has-error' : ''}}">
                    {!! Form::label('computerTech', 'Computer Technology', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="computerTech[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('englishLang') ? 'has-error' : ''}}">
                    {!! Form::label('englishLang', 'English Language', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="englishLang[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('frenchLang') ? 'has-error' : ''}}">
                    {!! Form::label('frenchLang', 'French Language', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="frenchLang[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('frenchLitt') ? 'has-error' : ''}}">
                    {!! Form::label('frenchLitt', 'French Littature', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" name="frenchLitt[]">
                            <option value="" selected  >Grade</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>
                            <option value="E">F</option>
                        </select>
                    </div>
                </div>


            </div>

        </div>

    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>


    $('.copy-area').hide();
    $('.number-offices').change(function () {
        var officesAmount = parseInt($(this).val(), 10);
        var e = $('.copy-area .register-form-extra').eq(0);
        $('.copy-area .register-form-extra').remove();
        for (var i = 0; i < officesAmount; i++) {
            e.clone().appendTo('.copy-area');

        }
        $('.copy-area').show();


        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

    });

    $('.o-level').hide();
    $('.a-level').hide();
    $('.qualifcation').change(function () {
        var qualification = $(this).val();

        if (qualification.localeCompare('GCE “A” Level') == 0) {
            $('.a-level').show();
            $('.o-level').hide();
        } else if (qualification.localeCompare('GCE “O” Level') == 0) {
            $('.o-level').show();
            $('.a-level').hide();
        } else {
            $('.o-level').hide();
            $('.a-level').hide();
            $('.obtained').hide();
        }
    });


</script>


