<section class="header">
    <div class="container">
        <div class="row">
            <h1 class="text-center">AOSA Details Preinscription</h1>
        </div>
    </div>
</section>
<section class="information">
    <div class="container">
        <div class="row">
            <div class="col-md-7  col-md-offset-3">
                <div class="center-block">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Personal Informations</h3>
                        </div>
                        <div class="panel-body">
                            <label for="name">Family Surname</label>
                            <p>{{$student->nom}}</p>
                            <label for="gender">Email</label>
                            <p>{{$student->email}}</p>
                            <label for="age">Phone (H)</label>
                            <p>{{$student->phone}}</p>
                            <label for="country">Date Naissance</label>
                            <p>{{$student->dateNaiss}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="divder"></div>
</section>
<section class="information">
    <div class="container">
        <div class="row center">
            <div class="col-md-6 col-sm-6">
                <div class="center-block">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">school Informations</h3>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th> Startyear </th><th> Endyear </th><th> Nameschool </th><th> Qualification </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($schoolinformations as $item)
                                    <tr>
                                        <td>{{ $item->startyear }}</td><td>{{ $item->endyear }}</td><td>{{ $item->nameschool }}</td><td>{{ $item->qualification }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $schoolinformations->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="center-block">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Examinations</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                   <th> Examinationyear </th><th> Candidatenumber </th><th> Examinationcenter </th><th> Resultobtained </th><th> Qualification </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($examinations as $item)
                                    <tr>
                                        <td>{{ $item->examinationyear }}</td><td>{{ $item->candidatenumber }}</td><td>{{ $item->examinationcenter }}</td><td>{{ $item->resultobtained }}</td><td>{{ $item->qualification }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $examinations->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section>
    <div class="divder"></div>
</section>
<section>
    <div class="divder"></div>
</section>
<section class="information">
    <div class="container">
        <div class="row center">
            <div class="col-md-6 col-sm-6">
                <div class="center-block">
                    <div class="panel panel-success">

                    <div class="panel-heading">
                        <h3 class="panel-title">Study Program</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th> Institution </th><th> Faculté </th><th> Departement </th><th>Niveau</th><th> Filière </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programStudy as $item)
                                <tr>
                                    <td>{{ $item->institution }}</td><td>{{ $item->faculte }}</td><td>{{ $item->departement }}</td><td>{{ $item->niveau }}</td><td>{{ $item->filiere }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $programStudy->render() !!} </div>
                    </div>
                </div>
            </div>

            @if($certificate != '')
            <div class="col-md-6 col-sm-6">
                <div class="center-block">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Certificates and Passport</h3>
                        </div>
                        <div class="panel-body">
                            <label for="name">Passport</label>

                            <p><a href="{{$certificate->passport}}" target="_blank"><img src="{{$certificate->passport}}" alt="" height="50px" width="50px"></a></p>
                            <label for="Gender">Certificates</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="{{$certificate->certificate1}}" target="_blank"><img src="{{$certificate->certificate1}}" alt="" height="50px" width="50px"></a>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{$certificate->certificate2}}" target="_blank"><img src="{{$certificate->certificate2}}" alt="" height="50px" width="50px"></a>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{$certificate->certificate3}}" target="_blank"><img src="{{$certificate->certificate3}}" alt="" height="50px" width="50px"></a>
                                </div>

                                <div class="col-md-3">
                                    <a href="{{$certificate->certificate4}}" target="_blank"><img src="{{$certificate->certificate4}}" alt="" height="50px" width="50px"></a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
                @endif
        </div>
        <section>
            @if($student->status == 0)

                <div class="row">

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">{{ trans('labels.frontend.finish_preinscription') }}</button>

                </div>
            @endif

                @if($student->status == 1)
                <div class="box-footer">
                    @if(access()->user()->type == 'manage-univ')

                        {!! Form::open([
                                'method'=>'patch',
                                'url' => ['/admin/access/student/reject', $student->id],
                                'style' => 'display:inline'
                            ]) !!}
                            <button type="submit" class="btn btn-danger" >{{ trans('labels.frontend.reject') }}</button>
                        {!! Form::close() !!}

                        {!! Form::open([
                                    'method'=>'patch',
                                    'url' => ['/admin/access/student/accept', $student->id],
                                    'style' => 'display:inline'
                                ]) !!}
                        <button type="submit" class="btn btn-success pull-right" >{{ trans('labels.frontend.accept') }}</button>
                        {!! Form::close() !!}


                    @endif
                </div>   <!-- /.box-footer -->
                @endif

        </section>
    </div>

    <section>
        <div class="divder"></div>
    </section>


{{--

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{{ trans('labels.frontend.pay_to_submit') }}</h4>
                </div>
                {!! Form::open([
                           'method' => 'patch',
                           'url' => ['admin/access/checkout'],
                           'style' => 'display:inline'
                       ]) !!}
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">{{ trans('labels.frontend.amount_institution') }}
                            : </label>
                        <input type="number" class="form-control"  name="montantInstitution" value="{{$institution->amountpreinscription}}" readonly>
                        <input type="hidden" class="form-control" name="studentId" value="{{$student->id}}"
                               id="recipient-name">
                        <input type="hidden" class="form-control" name="institutionId" value="{{$institution->id}}"
                               id="recipient-name">
                    </div>

                    <div class="form-group">
                        <label for="amount" class="control-label">{{ trans('labels.frontend.amount_platform') }}
                            : </label>
                        <input type="number" class="form-control" name="montantSysteme" value="{{$billingConfig->montant}}" id="amount" readonly>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ trans('labels.frontend.close') }}</button>
                    <button type="submit"
                            class="btn btn-primary">{{ trans('labels.frontend.pay') }}</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
        </div>--}}
        </div>
</section>