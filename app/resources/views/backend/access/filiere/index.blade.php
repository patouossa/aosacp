@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.filiere.management') . ' | ' . trans('labels.backend.access.filiere.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.filiere.management') }}
    <small>{{ trans('labels.backend.access.filiere.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Study Program</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/filiere/create') }}" class="btn btn-primary " title="Add New Filiere"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Name </th><th> Code </th><th> Anticipated Qualification </th><th> Departemnent </th><th> Places </th><th> Study program duration </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($filiere as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelle }}</td><td>{{ $item->code }}</td><td>{{ $item->niveau }}</td><td>{{ $item->departemnent }}</td><td>{{ $item->nbPlace }}</td><td>{{ $item->duration }}</td>
                        <td>
                            <a href="{{ url('/admin/access/filiere/' . $item->id) }}" class="btn btn-success btn-xs" title="View Filiere"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/filiere/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Filiere"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/filiere', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Filiere" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Filiere',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $filiere->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection