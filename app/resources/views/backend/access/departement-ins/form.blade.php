<div class="form-group {{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Faculte', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control" id="faculte" name="faculteId" required>
            <option value="" selected disabled>Selectionner la faculté</option>
            @foreach($facultes as $item)
                <option value="{{$item->id}}" {{ ($departementin != '' && $departementin->faculteId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
            @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>


