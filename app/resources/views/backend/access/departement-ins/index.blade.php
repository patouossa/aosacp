@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.departement-ins.management') . ' | ' . trans('labels.backend.access.departement-ins.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.departement-ins.management') }}
    <small>{{ trans('labels.backend.access.departement-ins.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Departementins</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/departement-ins/create') }}" class="btn btn-primary " title="Add New DepartementIn"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Libelle </th><th> Faculte </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($departementins as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelle }}</td><td>{{ $item->faculte }}</td>
                        <td>
                            <a href="{{ url('/admin/access/departement-ins/' . $item->id) }}" class="btn btn-success btn-xs" title="View DepartementIn"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/departement-ins/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit DepartementIn"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/departement-ins', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete DepartementIn" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete DepartementIn',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $departementins->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection