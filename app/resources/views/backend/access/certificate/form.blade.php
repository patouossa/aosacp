@foreach($uploadrequirement as $item)

    @if($item->requirementId == 1)
        <div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
            {!! Form::label('passport', 'Hand written application', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('handWriting', null, ['class' => 'form-control']) !!}
                {!! $errors->first('passport', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif

    @if($item->requirementId == 2)
        <div class="form-group {{ $errors->has('certificate') ? 'has-error' : ''}}">
            {!! Form::label('certificate', 'A certified true copy of the qualification eligible for admission', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('certifiedQualification', null, ['class' => 'form-control']) !!}
                {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif

    @if($item->requirementId == 3)
        <div class="form-group {{ $errors->has('certificate') ? 'has-error' : ''}}">
            {!! Form::label('certificate', 'A certified true copy of birth certificate', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('birth', null, ['class' => 'form-control']) !!}
                {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif

    @if($item->requirementId == 4)
        <div class="form-group {{ $errors->has('certificate') ? 'has-error' : ''}}">
            {!! Form::label('certificate', 'A transcript for candidates expecting their GCE Advanced Level results', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('transcriptgcea', null, ['class' => 'form-control']) !!}
                {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    @if($item->requirementId == 5)
        <div class="form-group {{ $errors->has('certificate') ? 'has-error' : ''}}">
            {!! Form::label('certificate', 'The institutions application form ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('applicationForm', null, ['class' => 'form-control']) !!}
                {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    @if($item->requirementId == 6)
        <div class="form-group {{ $errors->has('certificate') ? 'has-error' : ''}}">
            {!! Form::label('certificate', 'A certified true copy of your National Identify card or passport ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('cni', null, ['class' => 'form-control']) !!}
                {!! $errors->first('certificate', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    <input type="hidden" name="institutionId" value="{{$institution->id}}">
@endforeach