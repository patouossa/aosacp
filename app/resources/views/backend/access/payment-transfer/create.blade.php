@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-transfer.management') . ' | ' . trans('labels.backend.access.payment-transfer.create'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-transfer.management') }}
    <small>{{ trans('labels.backend.access.payment-transfer.create') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/payment-transfer', 'class' => 'form-horizontal', 'files' => true]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.payment-transfer.create') }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">


        <section class="header">
            <div class="container">
                <div class="row">
                    <h1 class="text-center">Information's to use for applications fee</h1>
                </div>
            </div>
        </section>
        <section class="information">
            <div class="container">
                <div class="row">
                    <div class="col-md-7  col-md-offset-3">
                        <div class="center-block">

                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{$moneyTransfer->institution}}</h3>
                                </div>
                                <div class="panel-body">
                                    <label for="name">Company Name</label>
                                    <p>{{$moneyTransfer->compnayName}}</p>
                                    <label for="gender">Beneficary Name</label>
                                    <p>{{$moneyTransfer->beneficaryName}}</p>
                                    <label for="age">Mobile</label>
                                    <p>{{$moneyTransfer->mobile}}</p>
                                    <label for="age">Amount</label>
                                    <p>{{$moneyTransfer->amount}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="divder"></div>
        </section>
        
        @include ('backend/access.payment-transfer.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{ link_to_route('admin.access.payment-transfer.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}
        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary pull-right']) }}                
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection