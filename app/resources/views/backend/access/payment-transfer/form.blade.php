<div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
    {!! Form::label('dateDepot', 'Datedepot', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
        {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<input type="hidden" name="institutionId" value="{{$moneyTransfer->institutionId}}">
<input type="hidden" name="adminEmail" value="{{$moneyTransfer->adminEmail}}">
<input type="hidden" name="conditionId" value="{{$conditionId}}">
<input type="hidden" name="amount" value="{{$moneyTransfer->amount}}">
