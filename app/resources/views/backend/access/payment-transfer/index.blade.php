@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-transfer.management') . ' | ' . trans('labels.backend.access.payment-transfer.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-transfer.management') }}
    <small>{{ trans('labels.backend.access.payment-transfer.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">payment-transfer</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/payment-transfer/create') }}" class="btn btn-primary " title="Add New PaymentTransfer"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Student Name </th><th> Student Phone </th>@if(access()->user()->type == null)<th> Institution </th>@endif<th> Amount </th><th> Reference </th><th> Date </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($paymenttransfer as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->student }}</td><td>{{ $item->studentPhone }}</td>@if(access()->user()->type == null)<td>{{ $item->institution }}</td>@endif<td>{{ $item->amount }}</td><td>{{ $item->reference }}</td><td>{{ $item->dateDepot }}</td>
                        <td>
                            <a href="{{ url('/admin/access/payment-transfer/' . $item->id) }}" class="btn btn-success btn-xs" title="View PaymentTransfer"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/payment-transfer/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit PaymentTransfer"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/payment-transfer', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete PaymentTransfer" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete PaymentTransfer',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $paymenttransfer->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection