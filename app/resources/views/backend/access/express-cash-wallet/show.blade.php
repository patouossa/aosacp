@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.express-cash-wallet.management') . ' | ' . trans('labels.backend.access.express-cash-wallet.show'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.express-cash-wallet.management') }}
    <small>{{ trans('labels.backend.access.express-cash-wallet.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.express-cash-wallet.show') }} - {{ $expresscashwallet->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/express-cash-wallet/' . $expresscashwallet->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit ExpressCashWallet"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/express-cash-wallet', $expresscashwallet->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'ExpressCashWallet',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $expresscashwallet->id }}</td>
                    </tr>
                    <tr><th> InstitutionName </th><td> {{ $expresscashwallet->institutionName }} </td></tr><tr><th> AccountName </th><td> {{ $expresscashwallet->accountName }} </td></tr><tr><th> AccountNumber </th><td> {{ $expresscashwallet->accountNumber }} </td></tr><tr><th> InstitutionId </th><td> {{ $expresscashwallet->institutionId }} </td></tr><tr><th> Institution </th><td> {{ $expresscashwallet->institution }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.express-cash-wallet.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
