<div class="form-group {{ $errors->has('institutionName') ? 'has-error' : ''}}">
    {!! Form::label('institutionName', 'Institution name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="text" name="institutionName" class="form-control" readonly value="CamCCUL">
        {!! $errors->first('institutionName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('accountName') ? 'has-error' : ''}}">
    {!! Form::label('accountName', 'Account name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('accountName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('accountName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('accountNumber') ? 'has-error' : ''}}">
    {!! Form::label('accountNumber', 'Account number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('accountNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('accountNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('reference', null, ['class' => 'form-control','placeholder' => 'Please enter this reference number to identify these deposit']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>