@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.student.management') . ' | ' . trans('labels.backend.access.student.create')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.student.management') }}
    <small>{{ trans('labels.backend.access.student.create') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/student', 'class' => 'form-horizontal', 'files' => true]) !!} 
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.student.create') }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">        
        
        @include ('backend/access.student.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{--{{ link_to_route('admin.access.student.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}--}}
        {{ Form::submit(trans('buttons.general.crud.next'), ['class' => 'btn btn-primary pull-right']) }}
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection