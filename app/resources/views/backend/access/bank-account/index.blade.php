@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.bank-account.management') . ' | ' . trans('labels.backend.access.bank-account.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.bank-account.management') }}
        <small>{{ trans('labels.backend.access.bank-account.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">bank-account</h3>
            <div class="box-tools pull-right">

                <a href="{{ url('/admin/access/bank-account/create') }}" class="btn btn-primary "
                   title="Add New bank-account"><span class="glyphicon glyphicon-plus"
                                                      aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Bank Name</th>
                        <th> Account Name</th>
                        <th> Bank Code</th>
                        <th> Account Number</th>
                        <th> RIP</th>
                        <th> Reference</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bankaccount as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->bankName }}</td>
                            <td>{{ $item->accountName }}</td>
                            <td>{{ $item->brankCode }}</td>
                            <td>{{ $item->accountNumber }}</td>
                            <td>{{ $item->rip }}</td>
                            <td>{{ $item->reference }}</td>

                            @if( empty(access()->user()->type))
                                @if($item->institutionId == null)
                                    <td>
                                        <a href="{{ url('/admin/access/bank-account/' . $item->id) }}"
                                           class="btn btn-success btn-xs" title="View BankAccount"><span
                                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        <a href="{{ url('/admin/access/bank-account/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit BankAccount"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/access/bank-account', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete BankAccount" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete BankAccount',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>

                                @endif

                            @else
                                <td>
                                    <a href="{{ url('/admin/access/bank-account/' . $item->id) }}"
                                       class="btn btn-success btn-xs" title="View BankAccount"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('/admin/access/bank-account/' . $item->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit BankAccount"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/access/bank-account', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete BankAccount" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete BankAccount',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            @endif


                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $bankaccount->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection