@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.mobile-transfer.management') . ' | ' . trans('labels.backend.access.mobile-transfer.show'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.mobile-transfer.management') }}
    <small>{{ trans('labels.backend.access.mobile-transfer.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.mobile-transfer.show') }} - {{ $mobiletransfer->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/mobile-transfer/' . $mobiletransfer->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit MobileTransfer"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/mobile-transfer', $mobiletransfer->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'MobileTransfer',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $mobiletransfer->id }}</td>
                    </tr>
                    <tr><th> OperatorName </th><td> {{ $mobiletransfer->operatorName }} </td></tr><tr><th> MobileAccountName </th><td> {{ $mobiletransfer->mobileAccountName }} </td></tr><tr><th> Mobile </th><td> {{ $mobiletransfer->mobile }} </td></tr><tr><th> InstitutionId </th><td> {{ $mobiletransfer->institutionId }} </td></tr><tr><th> Institution </th><td> {{ $mobiletransfer->institution }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.mobile-transfer.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
