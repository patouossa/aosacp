@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.mobile-transfer.management') . ' | ' . trans('labels.backend.access.mobile-transfer.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.mobile-transfer.management') }}
        <small>{{ trans('labels.backend.access.mobile-transfer.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Mobiletransfer</h3>
            <div class="box-tools pull-right">

                <a href="{{ url('/admin/access/mobile-transfer/create') }}" class="btn btn-primary "
                   title="Add New MobileTransfer"><span class="glyphicon glyphicon-plus"
                                                        aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Operator Name</th>
                        <th> Mobile Account Name</th>
                        <th> Mobile</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mobiletransfer as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->operatorName }}</td>
                            <td>{{ $item->mobileAccountName }}</td>
                            <td>{{ $item->mobile }}</td>

                            @if( empty(access()->user()->type))
                                @if($item->institutionId == null)
                                    <td>
                                        <a href="{{ url('/admin/access/mobile-transfer/' . $item->id) }}"
                                           class="btn btn-success btn-xs" title="View MobileTransfer"><span
                                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        <a href="{{ url('/admin/access/mobile-transfer/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit MobileTransfer"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/access/mobile-transfer', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete MobileTransfer" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete MobileTransfer',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                @endif

                            @else

                                <td>
                                    <a href="{{ url('/admin/access/mobile-transfer/' . $item->id) }}"
                                       class="btn btn-success btn-xs" title="View MobileTransfer"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('/admin/access/mobile-transfer/' . $item->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit MobileTransfer"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/access/mobile-transfer', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete MobileTransfer" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete MobileTransfer',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $mobiletransfer->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection