<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nom', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('localisation') ? 'has-error' : ''}}">
    {!! Form::label('localisation', 'Localisation', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('localisation', null, ['class' => 'form-control']) !!}
        {!! $errors->first('localisation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

