@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.religion.management') . ' | ' . trans('labels.backend.access.religion.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.religion.management') }}
    <small>{{ trans('labels.backend.access.religion.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Religion</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/religion/create') }}" class="btn btn-primary " title="Add New Religion"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> LibelleEN </th><th> LibelleFR </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($religion as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelleEN }}</td><td>{{ $item->libelleFR }}</td>
                        <td>
                            <a href="{{ url('/admin/access/religion/' . $item->id) }}" class="btn btn-success btn-xs" title="View Religion"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/religion/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Religion"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/religion', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Religion" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Religion',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $religion->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection