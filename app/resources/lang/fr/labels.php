<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'Tout',
        'yes'     => 'Oui',
        'no'      => 'Non',
        'custom'  => 'Personnalisé',
        'actions' => 'Actions',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Enregistrer',
            'update' => 'Mettre à jour',
        ],
        'hide'              => 'Cacher',
        'inactive'          => 'Inactive',
        'none'              => 'Aucun',
        'show'              => 'Voir',
        'toggle_navigation' => 'Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Créer un rôle',
                'edit'       => 'Editer un rôle',
                'management' => 'Gestion des rôles',

                'table' => [
                    'number_of_users' => "Nombre d'utilisateurs",
                    'permissions'     => 'Permissions',
                    'role'            => 'Rôle',
                    'sort'            => 'Ordre',
                    'total'           => 'rôle total|rôles total',
                ],
            ],

            'users' => [
                'active'              => 'Utilisateurs actifs',
                'all_permissions'     => 'Toutes les permissions',
                'change_password'     => 'Modifier le mot de passe',
                'change_password_for' => 'Modifier le mot de passe pour :user',
                'create'              => 'Créer un utilisateur',
                'deactivated'         => 'Utilisateurs désactivés',
                'deleted'             => 'Utilisateurs supprimés',
                'edit'                => 'Éditer un utilisateur',
                'management'          => 'Gestion des utilisateurs',
                'no_permissions'      => 'Aucune permission',
                'no_roles'            => 'Aucun rôle à affecter.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'      => 'Confirmé',
                    'created'        => 'Création',
                    'email'          => 'Email',
                    'id'             => 'ID',
                    'last_updated'   => 'Mise à jour',
                    'name'           => 'Nom',
                    'no_deactivated' => "Pas d'utilisateurs désactivés",
                    'no_deleted'     => "Pas d'utilisateurs supprimés",
                    'roles'          => 'Rôles',
                    'total'          => 'utilisateur total|utilisateurs total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Aperçu',
                        'history'  => 'Historique',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmé',
                            'created_at'   => 'Créé le',
                            'deleted_at'   => 'Supprimé le',
                            'email'        => 'E-mail',
                            'last_updated' => 'Mise à jour',
                            'name'         => 'Nom',
                            'status'       => 'Statut',
                        ],
                    ],
                ],

                'view' => 'Voir utilisateur',
            ],

            'institution' => [
                'create' => 'créér une institution',
                'show' => 'Institutions',
                'management' => 'Gestion des Institutions',
                'edit' => 'Modifier une institution',
            ],

            'departement-ins' => [
                'create' => 'créér un departement',
                'show' => 'Departements',
                'management' => 'Gestion de Departements',
                'edit' => 'modifier un departement',
            ],

            'region' => [
                'create' => 'créér une region',
                'show' => 'Regions',
                'management' => 'Gestion de Regions',
                'edit' => 'Modifier une region',
            ],

            'ville' => [
                'create' => 'créér une ville',
                'show' => 'Villes',
                'management' => 'Gestion de Ville',
                'edit' => 'modifier une ville',
            ],

            'filiere' => [
                'create' => 'Créér une filiere',
                'show' => 'Filieres',
                'management' => 'Gestion de Filieres',
                'edit' => 'Modifier une  filiere',
            ],

            'niveau' => [
                'create' => 'Créér un Diplôme',
                'show' => 'Diplômes',
                'management' => 'Gestion de Diplômes',
                'edit' => 'Modifier un  Diplôme',
            ],


            'student' => [
                'create' => 'Create a student',
                'show' => 'Students',
                'management' => 'Students Management',
                'edit' => 'update a student',
            ],

            'school-information' => [
                'create' => 'Create a School Information',
                'show' => 'School Information',
                'management' => 'School Information Management',
                'edit' => 'update a School Information',
            ],

            'examination' => [
                'create' => 'Create a Examination',
                'show' => 'Examinations',
                'management' => 'Examinations Management',
                'edit' => 'update a Examination',
            ],

            'programstudie' => [
                'create' => 'Create a Program Study',
                'show' => 'Programs Studies',
                'management' => 'Programs Studies Management',
                'edit' => 'update a Program Study',
            ],

            'certificate' => [
                'create' => 'Create a Certificate',
                'show' => 'Certificates',
                'management' => 'Programs Certificates',
                'edit' => 'update a Certificate',
            ],

            'billing-config' => [
                'create' => 'Create a Billing Config',
                'show' => 'Billing Config',
                'management' => 'Billing Config Management',
                'edit' => 'update a Billing Config',
            ],

            'billing' => [
                'create' => 'Create a Billing',
                'show' => 'Billing',
                'management' => 'Billing Management',
                'edit' => 'Update a Billing',
            ],

            'faculte' => [
                'create' => 'Create a Faculty',
                'show' => 'Faculty',
                'management' => 'Faculty Management',
                'edit' => 'Update a Faculty',
            ],

            'Paymentmode' => [
                'create' => 'Create a Payment mode',
                'show' => 'Payment mode',
                'management' => 'Payment mode Management',
                'edit' => 'Update a Payment mode',
            ],

            'qualification' => [
                'create' => 'Create a Qualification',
                'show' => 'Qualifications',
                'management' => 'Qualification Management',
                'edit' => 'Update a Qualification',
            ],

            'requirement' => [
                'create' => 'Create a Requirement',
                'show' => 'Requirement',
                'management' => 'Requirement Management',
                'edit' => 'Update a Requirement',
            ],

            'payment-bank' => [
                'create' => 'Create a Bank Payment',
                'show' => 'Bank Payment',
                'management' => 'Bank Payment Management',
                'edit' => 'Update a Bank Payment',
            ],

            'payment-expresscash' => [
                'create' => 'Create a Express Cash Payment',
                'show' => 'Express Cash',
                'management' => 'Express Cash Management',
                'edit' => 'Update a Express Cash',
            ],

            'payment-mobile' => [
                'create' => 'Create a Mobile Payment',
                'show' => 'Mobile Payment',
                'management' => 'Mobile Payment Management',
                'edit' => 'Update a Mobile Payment',
            ],

            'payment-transfer' => [
                'create' => 'Create a Money Transfer Payment',
                'show' => 'Money Transfer Payment',
                'management' => 'Money Transfer Payment Management',
                'edit' => 'Update a Money Transfer Payment',
            ],

            'condition' => [
                'create' => 'Create a Terms & Conditions ',
                'show' => 'Terms & Conditions',
                'management' => 'Terms & Conditions Management',
                'edit' => 'Update a Terms & Conditions',
            ],

        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Connexion',
            'login_button'       => 'Entrer',
            'login_with'         => 'Se connecter avec :social_media',
            'register_box_title' => "S'enregistrer",
            'register_button'    => 'Créer le compte',
            'remember_me'        => 'Se souvenir de moi',
        ],

        'passwords' => [
            'forgot_password'                 => 'Avez-vous oublié votre mot de passe&nbsp;?',
            'reset_password_box_title'        => 'Réinitialisation du mot de passe',
            'reset_password_button'           => 'Réinitialiser le mot de passe',
            'send_password_reset_link_button' => 'Envoyer le lien de réinitialisation',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Pays Alpha Codes',
                'alpha2'  => 'Pays Alpha 2 Codes',
                'alpha3'  => 'Pays Alpha 3 Codes',
                'numeric' => 'Pays Numéros Codes',
            ],

            'macro_examples' => 'Exemples de macros',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Fuseau horaire',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Modifier le mot de passe',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Date de création',
                'edit_information'   => 'Éditer les informations',
                'email'              => 'Email',
                'last_updated'       => 'Date de mise à jour',
                'name'               => 'Nom',
                'update_information' => 'Mettre à jour les informations',
            ],
        ],
        'pay_to_submit' => 'Payer pour soumettre votre dossier',
        'amount_platform' => 'Frais d\'utilisation de la platforme',
        'amount_institution' => 'Frais de depot de dossier',
        'pay' => 'Payer',
        'finish_preinscription' => 'Finaliser la preiscription',
        'accept' => 'Accepter',
        'reject' => 'Rejeter',

        'nom' => 'Name',
        'name_with_bird' => 'Name of applicant as on their Birth Certificate',
        'date_naiss' => 'Date of Birth',
        'disability' => 'Disability',
        'select_disability' => 'Select Disability',
        'select_ville' => 'Select the Town',
        'all_ville' => 'All the Town',
        'select_region' => 'Select the Region',
        'all_region' => 'All Region',
        'none' => 'None',
        'country_birth' => 'Country of Residence',
        'select_country' => 'Select Country',
        'nationality' => 'Nationality',
        'select_nationality' => 'Select a Nationality',
        'religion' => 'Religion',
        'select_religion' => 'Select Religion',
        'annuler' => 'Cancel',
        'marital_status' => 'Marital Status',
        'select_marital_status' => 'Select Marital Status',
        'seemore' => 'See more',
        'region' => 'Region',
        'ville' => 'Town',
        'specialite' => 'Specialty',
        'lieu' => 'Location',
        'email' => 'Email',
        'phone' => 'Phone',
        'sexe' => 'Sex',
        'male' => 'Male',
        'female' => 'Female',
        'fatherName' => 'Father\'s Names',
        'mothername' => 'Mother\'s Names',
        'country_parent' => 'Parent\'s Country',
        'ville_parent' => 'Parent\'s Town/City',
        'adresseparent' => 'Parent\'s Address (Quarter & Location)',
        'parent_occupation' => 'Parent\'s Occupation',
        'parent_phone' => 'Parent\'s Telephone',
        'parent_email' => 'Parent\'s Email',
        'sans_experience' => 'No experience',
        'patience' => 'Please wait while the map is loading...',
        'avis' => 'Tell Us Your Experience',
        'commentaire' => 'Comments',
        'avis_connect' => 'share your Experience',
        'your_comment' => 'Your Comments:',
        'send_your_comment' => 'Send',
        'back' => 'BacK',
        'titre' => 'Title',
        'close' => 'Close',
        'frais_consultation' => 'Consultation Fee :',
        'nbre_medecin' => 'Number of Doctors : ',
        'typecentre' => 'Type of centre : ',
        'nbre_infirmier' => ' Number of Nurses : ',
        'nbre_lit' => 'Number of Beds : ',
        'description' => 'Description',
        'approuve' => 'Approved',
        'localisation' => 'Location',
        'arrondissement' => 'District',
        'departement' => 'Department',
        'select_arrondissement' => 'Select the district',
        'all_arrondissement' => 'All the district',
        'select_departement' => 'Select the department',
        'all_departement' => 'All the department',
        'bp' => 'P.O. Box',
        'photo' => 'Picture',
        'libelle' => 'Label',
        'jour' => 'Days',
        'select_jour' => 'select the day',
        'plagehoraire' => 'Time Range',
        'lundi' => 'Monday',
        'mardi' => 'Tuesday',
        'mercredi' => 'Wednesday',
        'jeudi' => 'Thursday',
        'vendredi' => 'Friday',
        'samedi' => 'Saturday',
        'dimanche' => 'Sunday',
        'de' => 'To',
        'a' => 'From',

        'confirm_delete' => 'return confirm("Confirm delete?")',
        'delete' => 'Delete',
        'add' => 'Add',
        'view' => 'View',
        'edit' => 'Update',
        'cancel_pub' => 'Cancel Publication',
        'confirm_cancel_pub' => 'return confirm("Cancel publication?")',
        'publication' => 'Publication',
        'confirm_publication' => 'Confirm Publication',
        'donate' => 'Faire un don',
        'montant_usd' => 'Amount in USD',
        'thank_donate' => 'Thanks to make a donate',

        'status' => 'Status',
        'see_googlemaps' => 'See on Google Maps',
        'select_status' => 'Select a Status',
        'public' => 'PUBLIC',
        'private' => 'PRIVATE',
        'para-public' => 'PARA-PUBLIC',
        'longitude' => 'Longitude',
        'latitude' => 'Latitude',
        'cordx' => 'Cordx',
        'select_institution' => 'Selectionner l\'institution',
        'institution' => 'Institution',
    ],
];
