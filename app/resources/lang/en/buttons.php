<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Activate',
                'change_password'    => 'Change Password',
                'deactivate'         => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'login_as'           => 'Login As :user',
                'resend_email'       => 'Resend Confirmation E-mail',
                'restore_user'       => 'Restore User',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password'  => 'Reset Password',
        ],
    ],

    'general' => [
        'cancel' => 'Cancel',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit'   => 'Edit',
            'update' => 'Update',
            'view'   => 'View',
            'add'    => 'Ajouter',
            'next'    => 'Next',
            'to_preinscription'    => 'Do a preinscription',
            'save_phase1'    => 'Create your Application',
            'save_phase2'    => 'Save Phase 2',
            'save' => 'Save',
            'create_exam' => 'Create an examination',
            'create_school' => 'Enter another school',
            'go_to_school_info' => 'Go to School Information',
            'go_to_examination' => 'Go to Examination',
            'go_to_back_to_registration' => 'Go back to Registration',
            'go_to_back_to_school_info' => 'Go back to School Information',
            'create_program' => 'Create a Study of program',
        ],

        'save' => 'Save',
        'view' => 'View',
    ],
];
