<?php

namespace App\Http\Controllers\Backend\Access\Certificate;

use App\Http\Controllers\Backend\Access\ProframStudy\ProgramstudieController;
use App\Http\Controllers\Backend\Access\Student\StudentController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BankAccount\BankAccount;
use App\Models\Access\Billing\Billing;
use App\Models\Access\BillingConfig\BillingConfig;
use App\Models\Access\Certificate\Certificate;
use App\Models\Access\Condition\Condition;
use App\Models\Access\Examination\Examination;
use App\Models\Access\ExpressCashWallet\ExpressCashWallet;
use App\Models\Access\Institution\Institution;
use App\Models\Access\MobileTransfer\MobileTransfer;
use App\Models\Access\MoneyTransfer\MoneyTransfer;
use App\Models\Access\ProgramStudy\Programstudie;
use App\Models\Access\SchoolInformation\SchoolInformation;
use App\Models\Access\Student\Student;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Session;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $certificate = Certificate::paginate(25);

        $user = \Auth::user();
        $student1 = Student::where('email', $user->email)->first();

        return view('backend/access.certificate.index', compact('certificate'),['student1'=>$student1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();
        $student1 = Student::where('email', $user->email)->first();

        return view('backend/access.certificate.create',['student1'=>$student1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();

        $requestData['studentId'] = $student->id;

        if ($request->hasFile('handWriting')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('handWriting')->getClientOriginalExtension();
            $fileName = $request->file('handWriting')->getClientOriginalName() ;

            $request->file('handWriting')->move($uploadPath, $fileName);

            $requestData['handWriting'] = '/uploads/' . $fileName;
            $requestData['handWritingName'] = $request->file('handWriting')->getClientOriginalName();
            $requestData['handWritingSize'] = $request->file('handWriting')->getSize();
        } else {
            $requestData['handWriting'] = '';
        }


        if ($request->hasFile('certifiedQualification')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('certifiedQualification')->getClientOriginalExtension();
            $fileName = $request->file('certifiedQualification')->getClientOriginalName() ;

            $request->file('certifiedQualification')->move($uploadPath, $fileName);

            $requestData['certifiedQualification'] = '/uploads/' . $fileName;

            $requestData['certifiedQualification'] = '/uploads/' . $fileName;
            $requestData['certifiedQualificationName'] = $request->file('certifiedQualification')->getClientOriginalName();
            $requestData['certifiedQualificationSize'] = $request->file('certifiedQualification')->getSize();
        } else {
            $requestData['certifiedQualification'] = '';
        }

        if ($request->hasFile('birth')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('birth')->getClientOriginalExtension();
            $fileName = $request->file('birth')->getClientOriginalName() ;

            $request->file('birth')->move($uploadPath, $fileName);

            $requestData['birth'] = '/uploads/' . $fileName;

            $requestData['birthName'] = $request->file('birth')->getClientOriginalName();
            $requestData['birthSize'] = $request->file('birth')->getSize();
        } else {
            $requestData['birth'] = '';
        }

        if ($request->hasFile('transcriptgcea')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('transcriptgcea')->getClientOriginalExtension();
            $fileName = $request->file('transcriptgcea')->getClientOriginalName() ;

            $request->file('transcriptgcea')->move($uploadPath, $fileName);
            $requestData['transcriptgcea'] = '/uploads/' . $fileName;

            $requestData['transcriptgceaName'] = $request->file('transcriptgcea')->getClientOriginalName();
            $requestData['transcriptgceaSize'] = $request->file('transcriptgcea')->getSize();

        } else {
            $requestData['transcriptgcea'] = '';
        }

        if ($request->hasFile('applicationForm')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('applicationForm')->getClientOriginalExtension();
            $fileName = $request->file('applicationForm')->getClientOriginalName() ;

            $request->file('applicationForm')->move($uploadPath, $fileName);
            $requestData['applicationForm'] = '/uploads/' . $fileName;

            $requestData['applicationFormName'] = $request->file('applicationForm')->getClientOriginalName();
            $requestData['applicationFormSize'] = $request->file('applicationForm')->getSize();

        } else {
            $requestData['applicationForm'] = '';
        }
        if ($request->hasFile('cni')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('cni')->getClientOriginalExtension();
            $fileName = $request->file('cni')->getClientOriginalName() ;

            $request->file('cni')->move($uploadPath, $fileName);
            $requestData['cni'] = '/uploads/' . $fileName;

            $requestData['cniName'] = $request->file('cni')->getClientOriginalName();
            $requestData['cniSize'] = $request->file('cni')->getSize();

        } else {
            $requestData['cni'] = '';
        }

        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();
        $student1 = $student;


        $requestData['studentId'] = $student->id;
        $requestData['userEmail'] = $user->email;
        $studentData['status'] = '2';

        $student->update($studentData);
        Certificate::create($requestData);
        $institution = Institution::findOrFail($requestData['institutionId']);
        $userAdmin = User ::where('type',null)->first();

        $billing = Billing::where('institutionId',$institution->id)->first();

        $bankaccount = BankAccount::where('institutionId',$institution->id)->first();
        $expresscash = ExpressCashWallet::where('institutionId',$institution->id)->first();
        $mobiletransfer = MobileTransfer::where('institutionId',$institution->id)->first();
        $moneytransfer = MoneyTransfer::where('institutionId',$institution->id)->first();

        if ($bankaccount != null){
            $conditionData['bankAccountId']=$bankaccount->id;
        }

        if ($expresscash != null){
            $conditionData['expresscashId']=$expresscash->id;
        }

        if ($mobiletransfer != null){
            $conditionData['mobileTransferId']=$mobiletransfer->id;
        }

        if ($moneytransfer != null){
            $conditionData['moneyTransferId']=$moneytransfer->id;
        }

        //for Admin

        $bankaccountAdmin = BankAccount::where('adminEmail',$userAdmin->email)->first();
        $expresscashAdmin = ExpressCashWallet::where('adminEmail',$userAdmin->email)->first();
        $mobiletransferAdmin = MobileTransfer::where('adminEmail',$userAdmin->email)->first();
        $moneytransferAdmin = MoneyTransfer::where('adminEmail',$userAdmin->email)->first();

        if ($bankaccountAdmin != null){
            $conditionData['bankAccountAdminId']=$bankaccountAdmin->id;
        }

        if ($expresscashAdmin != null){
            $conditionData['expresscashAdminId']=$expresscashAdmin->id;
        }

        if ($mobiletransferAdmin != null){
            $conditionData['mobileTransferAdminId']=$mobiletransferAdmin->id;
        }

        if ($moneytransferAdmin != null){
            $conditionData['moneyTransferAdminId']=$moneytransferAdmin->id;
        }

        $conditionData['institutionId'] = $requestData['institutionId'];
        $conditionData['institution'] = $institution->nom;
        $conditionData['amountInstitution'] = $institution->amountpreinscriptionLocal;
        $conditionData['adminId'] = $userAdmin->email;
        $conditionData['amountAdmin'] = $billing->montantSysteme;
        Condition::create($conditionData);

        $programstudyController = new ProgramstudieController();

        //return $programstudyController->create();
        $condition = Condition::paginate(25);

        return redirect('admin/access/institution-summary');
        //return view('backend/access.condition.index', compact('condition'),['student1'=>$student1]);
    }

    public function gotoDetails($studentId,$institutionId)
    {

        $schoolinformations = SchoolInformation::where('studentId', $studentId)->paginate(25);
        $certificate = Certificate::where('studentId', $studentId)->where('institutionId', $institutionId)->first();

        if ($certificate == null) {
            $certificate = '';
        }

        $user = \Auth::user();

        $institution = Institution::where('userEmail', $user->email)->first();

        if($institution == null){
            $programStudy = Programstudie::where('studentId', $studentId)->where('institutionId', $institutionId)->paginate(8);
        }else{
            $programStudy = Programstudie::where('studentId', $studentId)->where('institutionId', $institution->id)->paginate(8);
        }

        $examination = Examination::where('studentId', $studentId)->paginate(8);
        $student = Student::findOrFail($studentId);
        $billingConfig = BillingConfig::first();
        return view('backend/access.details.details', ['schoolinformations' => $schoolinformations, 'certificate' => $certificate, 'programStudy' => $programStudy, 'examinations' => $examination, 'student' => $student, 'institution' => $institution, 'billingConfig' => $billingConfig,]);
    }

    public function gotoSucess($studentId)
    {


        $certificate = Certificate::where('studentId', $studentId)->first();
        $programStudy = Programstudie::where('studentId', $studentId)->first();
        $student = Student::findOrFail($studentId);
        $institution = Institution::findOrFail($programStudy->institutionId);

        return view('backend/access.details.success', ['certificate' => $certificate, 'programStudy' => $programStudy, 'student' => $student, 'institution' => $institution,]);
    }

    public function billing(Request $request)
    {

        $requestData = $request->all();

        $student = Student::findOrFail($requestData['studentId']);
        $studentData['status'] = 1;

        $student->update($studentData);


        Billing::create($requestData);


        return $this->gotoSucess($student->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('backend/access.certificate.show', compact('certificate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('backend/access.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();


        if ($request->hasFile('handWriting')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('handWriting')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) ;

            $request->file('handWriting')->move($uploadPath, $fileName);
            $requestData['handWriting'] = $fileName;
        }


        if ($request->hasFile('certifiedQualification')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('certifiedQualification')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) ;

            $request->file('certifiedQualification')->move($uploadPath, $fileName);
            $requestData['certifiedQualification'] = $fileName;
        }

        $certificate = Certificate::findOrFail($id);
        $certificate->update($requestData);

        Session::flash('flash_message', 'Certificate updated!');

        return redirect('admin/access/certificate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Certificate::destroy($id);

        Session::flash('flash_message', 'Certificate deleted!');

        return redirect('admin/access/certificate');
    }
}
