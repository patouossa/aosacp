<?php

namespace App\Http\Controllers\Backend\Access\PaymentOptions;

use App\Http\Controllers\Backend\Access\Certificate\CertificateController;
use App\Http\Controllers\Backend\Access\Student\StudentController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Certificate\Certificate;
use App\Models\Access\DepartementIns\DepartementIn;
use App\Models\Access\EtudiantInstitution\EtudiantInstitution;
use App\Models\Access\Faculte\Faculte;
use App\Models\Access\Filiere\Filiere;
use App\Models\Access\Institution\Institution;
use App\Models\Access\Niveau\Niveau;
use App\Models\Access\ProgramStudy\Programstudie;
use App\Models\Access\Student\Student;
use App\Models\Access\UploadRequirement\UploadRequirement;
use Illuminate\Http\Request;
use Session;

class AddMoneyTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //$programstudie = Programstudie::paginate(25);
        return $this->create($request);
        //return view('backend/access.money-transfer-student.index', compact('programstudie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $user = \Auth::user();

        $requestData = $request->all();

        $student = Student::where('email', $user->email)->first();
        $student1 = $student;
        $institution = Institution::get();


        $result = false;

        return view('backend/access.money-transfer-student.create', ['institution' => $institution, 'student1' => $student1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $departementId = $requestData['departementId'];

        $departement = DepartementIn::where('id', $departementId)->first();

        $requestData['departement'] = $departement->libelle;

        $niveauId = $requestData['niveauId'];

        $niveau = Niveau::where('id', $niveauId)->first();

        $requestData['niveau'] = $niveau->niveau;

        $institutionId = $requestData['institutionId'];

        $institution = Institution::where('id', $institutionId)->first();

        $requestData['institution'] = $institution->nom;

        $filiereId = $requestData['filiereId'];

        $filiere = Filiere::where('id', $filiereId)->first();

        $requestData['filiere'] = $filiere->libelle;

        $faculteId = $requestData['faculteId'];

        $faculte = Faculte::where('id', $faculteId)->first();

        $requestData['faculte'] = $faculte->nom;

        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;

        $studentInstitutionData['institutionEmail'] = $institution->userEmail;
        $studentInstitutionData['institutionId'] = $institution->id;
        $studentInstitutionData['studentEmail'] = $user->email;
        $studentInstitutionData['studentId'] = $student->id;
        EtudiantInstitution::create($studentInstitutionData);

        Programstudie::create($requestData);

        return $this->create($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $programstudie = Programstudie::findOrFail($id);

        return view('backend/access.money-transfer-student.show', compact('programstudie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $programstudie = Programstudie::findOrFail($id);
        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();
        $student1 = $student;
        $institution = Institution::get();
        $departementins = DepartementIn::where('userEmail', $user->email)->get();
        $filiere = Filiere::where('userEmail', $user->email)->get();
        $niveau = Niveau::where('userEmail', $user->email)->get();
        $facultes = Faculte::where('userEmail', $user->email)->get();
        $programstudies = Programstudie::where('studentId', $student->id)->get();
        return view('backend/access.money-transfer-student.edit', compact('programstudie'), ['institution' => $institution, 'departementins' => $departementins,
            'niveaux' => $niveau, 'student1' => $student1, 'filiere' => $filiere, 'facultes' => $facultes, 'programstudies' => $programstudies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $programstudie = Programstudie::findOrFail($id);
        $programstudie->update($requestData);

        Session::flash('flash_message', 'Programstudie updated!');

        return redirect('admin/access/programstudie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        Programstudie::destroy($id);

        /*Session::flash('flash_message', 'Programstudie deleted!');

        return redirect('admin/access/programstudie');*/
        return $this->create($request);
    }
}
