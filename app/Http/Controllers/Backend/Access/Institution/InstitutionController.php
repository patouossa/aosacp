<?php

namespace App\Http\Controllers\Backend\Access\Institution;

use App\Events\Backend\Access\Institution\InstitutionCreated;
use App\Events\Backend\Access\User\UserCreated;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Institution\Institution;
use App\Models\Access\Region\Region;
use App\Models\Access\User\User;
use App\Models\Access\Ville\Ville;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Session;

class InstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {


        $user = \Auth::user();

        if (empty($user->type)) {
            $institution = Institution::paginate(25);
        } else {
            $institution = Institution::where('userEmail', $user->email)->paginate(25);
        }

        return view('backend/access.institution.index', ['student1'=> $user, 'institution'=> $institution]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();

        $region = Region::get();
        $ville = Ville::get();
        $institution = '';
        $edit = false;
        return view('backend/access.institution.create',['region' => $region, 'ville' => $ville, 'institution' => $institution, 'edit' => $edit, 'student1'=> $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        /*$user = \Auth::user();
        $requestData['userId'] = $user->id;
        $requestData['userEmail'] = $user->email;*/
        $lang = Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }

        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        $user_model = config('auth.providers.users.model');
        $user_model = new $user_model();


        $users = [
            'name' => $requestData["nom"],
            'email' => $requestData["userEmail"],
            'password' => bcrypt($requestData["password"]),
            'telephone' => $requestData["telephone"],
            'type' => 'manage-univ',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed' => true,
            'active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        DB::transaction(function () use ($users, $user_model, $requestData) {

            User::create($users);
            $email =$requestData["userEmail"];

            $id = DB::table('users')->when($email != '', function ($query) use ($email) {
                return $query->where('email', '=', $email);
            })->pluck('id');

            $user_model::find($id)->attachRole(4);
            $requestData['userId'] = $id;



            Institution::create($requestData);

            event(new InstitutionCreated($requestData));

            event(new UserCreated($user_model::find($id)));
        });


        Session::flash('flash_message', 'Institution added!');

        return redirect('admin/access/institution');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = \Auth::user();
        $institution = Institution::findOrFail($id);
        $region = Region::get();
        $ville = Ville::get();
        $edit = true;
        return view('backend/access.institution.show', compact('institution'),['region' => $region, 'ville' => $ville, 'edit' => $edit, 'student1' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = \Auth::user();
        $institution = Institution::findOrFail($id);
        $region = Region::get();
        $ville = Ville::get();
        $edit = true;
        return view('backend/access.institution.edit', compact('institution'),['region' => $region, 'ville' => $ville, 'edit' => $edit, 'student1' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        $lang = Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);

            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }
        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        $institution = Institution::findOrFail($id);

        /*$user_model = config('auth.providers.users.model');
        $user_model = new $user_model();



        $id = DB::table('users')->when($email != '', function ($query) use ($email) {
            return $query->where('email', '=', $email);
        })->pluck('id');

        $user = $user_model::find($id);

        $userData = [
            'name' => $requestData["nom"],
            'telephone' => $requestData["telephone"],
            'updated_at' => Carbon::now(),
        ];

        $user->update($userData);
        $requestData["password"] = $institution->password;*/

        $institution->update($requestData);

        Session::flash('flash_message', 'Institution updated!');

        return redirect('admin/access/institution');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Institution::destroy($id);

        Session::flash('flash_message', 'Institution deleted!');

        return redirect('admin/access/institution');
    }
}
