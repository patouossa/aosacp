<?php

namespace App\Http\Controllers\Backend\Access\ViewSubmitMyApplication;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Billing\Billing;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class ViewSubmitMyApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $billing = Billing::paginate(25);

        return view('backend/access.view-submit-application.index', ['student1'=> $user, 'billing'=> $billing ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();
        $institutions = Institution::get();
        $billing='';
        return view('backend/access.ViewSubmitMyApplication.create',['billing'=>$billing,'institutions'=>$institutions],['student1' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $institutionId = $requestData['institutionId'];

        $institution = Institution::where('id', $institutionId)->first();

        $requestData['institution'] = $institution->nom;
        
        Billing::create($requestData);

        Session::flash('flash_message', 'ViewSubmitMyApplication added!');

        return redirect('admin/access/ViewSubmitMyApplication');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $billing = Billing::findOrFail($id);
        return view('backend/access.ViewSubmitMyApplication.show', compact('ViewSubmitMyApplication'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $billing = Billing::findOrFail($id);
        $institutions = Institution::get();
        return view('backend/access.ViewSubmitMyApplication.edit', compact('ViewSubmitMyApplication'),['institutions'=>$institutions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $billing = Billing::findOrFail($id);

        $institutionId = $requestData['institutionId'];
        $institution = Institution::where('id', $institutionId)->first();
        $requestData['institution'] = $institution->nom;

        $billing->update($requestData);

        Session::flash('flash_message', 'ViewSubmitMyApplication updated!');

        return redirect('admin/access/ViewSubmitMyApplication');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Billing::destroy($id);

        Session::flash('flash_message', 'ViewSubmitMyApplication deleted!');

        return redirect('admin/access/ViewSubmitMyApplication');
    }
}
