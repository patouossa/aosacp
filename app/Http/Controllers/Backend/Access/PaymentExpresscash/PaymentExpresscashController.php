<?php

namespace App\Http\Controllers\Backend\Access\PaymentExpresscash;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\PaymentExpresscash\PaymentExpresscash;
use Illuminate\Http\Request;
use Session;

class PaymentExpresscashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $paymentexpresscash = PaymentExpresscash::paginate(25);

        return view('backend/access.payment-expresscash.index', compact('paymentexpresscash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.payment-expresscash.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('scanDepositSlip')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('scanDepositSlip')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('scanDepositSlip')->move($uploadPath, $fileName);
    $requestData['scanDepositSlip'] = $fileName;
}

        PaymentExpresscash::create($requestData);

        Session::flash('flash_message', 'PaymentExpresscash added!');

        return redirect('admin/access/payment-expresscash');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentexpresscash = PaymentExpresscash::findOrFail($id);

        return view('backend/access.payment-expresscash.show', compact('paymentexpresscash'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentexpresscash = PaymentExpresscash::findOrFail($id);

        return view('backend/access.payment-expresscash.edit', compact('paymentexpresscash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('scanDepositSlip')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('scanDepositSlip')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('scanDepositSlip')->move($uploadPath, $fileName);
    $requestData['scanDepositSlip'] = $fileName;
}

        $paymentexpresscash = PaymentExpresscash::findOrFail($id);
        $paymentexpresscash->update($requestData);

        Session::flash('flash_message', 'PaymentExpresscash updated!');

        return redirect('admin/access/payment-expresscash');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PaymentExpresscash::destroy($id);

        Session::flash('flash_message', 'PaymentExpresscash deleted!');

        return redirect('admin/access/payment-expresscash');
    }
}
