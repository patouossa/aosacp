<?php

namespace App\Http\Controllers\Backend\Access\PaymentMobile;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\PaymentMobile\PaymentMobile;
use Illuminate\Http\Request;
use Session;

class PaymentMobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $paymentmobile = PaymentMobile::paginate(25);

        return view('backend/access.payment-mobile.index', compact('paymentmobile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.payment-mobile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        PaymentMobile::create($requestData);

        Session::flash('flash_message', 'PaymentMobile added!');

        return redirect('admin/access/payment-mobile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentmobile = PaymentMobile::findOrFail($id);

        return view('backend/access.payment-mobile.show', compact('paymentmobile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentmobile = PaymentMobile::findOrFail($id);

        return view('backend/access.payment-mobile.edit', compact('paymentmobile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $paymentmobile = PaymentMobile::findOrFail($id);
        $paymentmobile->update($requestData);

        Session::flash('flash_message', 'PaymentMobile updated!');

        return redirect('admin/access/payment-mobile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PaymentMobile::destroy($id);

        Session::flash('flash_message', 'PaymentMobile deleted!');

        return redirect('admin/access/payment-mobile');
    }
}
