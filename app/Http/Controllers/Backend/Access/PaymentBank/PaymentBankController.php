<?php

namespace App\Http\Controllers\Backend\Access\PaymentBank;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BankAccount\BankAccount;
use App\Models\Access\Condition\Condition;
use App\Models\Access\Institution\Institution;
use App\Models\Access\PaymentBank\PaymentBank;
use App\Models\Access\Student\Student;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class PaymentBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $paymentbank = PaymentBank::paginate(25);

        return view('backend/access.payment-bank.index', compact('paymentbank'),['student1'=> $user, 'paymentbank'=> $paymentbank]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $conditionId = $request->get('id');
        $requestData = $request->all();

        if(empty($conditionId)){
            $conditionId = $requestData['conditionId'];
        }
        $user = \Auth::user();
        $student1 = Student::where('email', $user->email)->first();
        $condition = Condition::findOrfail($conditionId);
        $bankaccount = BankAccount::get();
        $institution = Institution::findOrfail($condition->institutionId);
        $userAdmin = User ::where('type',null)->first();
        $paymentbanks = PaymentBank::where('studentId',$student1->id)->paginate(10);
        $paymentbank = '';
        return view('backend/access.payment-bank.create',['bankaccounts' => $bankaccount,
            'paymentbanks' => $paymentbanks, 'paymentbank' => $paymentbank,
            'conditionId' => $conditionId,'institution' => $institution,
            'student1' => $student1,'userAdmin' => $userAdmin,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();



        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();

        $requestData['studentId'] = $student->id;
        $requestData['student'] = $student->nom;
        $requestData['studentPhone'] = $student->phone;
        $condition = Condition::findOrFail($requestData['conditionId']);

        $bank = BankAccount::findOrfail($requestData['bankaccountId']);
        $requestData['bankName'] = $bank->bankName;
        if(!empty($requestData['institutionId'])){
            //dd($requestData);
            $institution = Institution::where('id',($requestData['institutionId']))->first();

            if($institution != null){

                $requestData['institution'] = $institution->nom;
                $requestData['institutionEmail'] = $institution->email;
                $conditionData['status'] = 2;
                $conditionData['statusApp'] = 1;
                $condition->update($conditionData);

                $studentData['status'] = 3;
                $student->update($studentData);
            }else{

                $adminUser = User::where('email',$requestData['institutionId'])->first();
                $requestData['adminId'] = $adminUser->id;
                $requestData['institution'] = 'Tamco-Tech';
                $conditionData['status'] = 1;
                $conditionData['statusAdmin'] = 1;
                $condition->update($conditionData);
                $studentData['status'] = 3;
                $student->update($studentData);
            }

        }

        if ($request->hasFile('scanDepositSlip')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('scanDepositSlip')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('scanDepositSlip')->move($uploadPath, $fileName);
            $requestData['scanDepositSlip'] = '/uploads/'.$fileName;
        }else{
            $requestData['scanDepositSlip'] = '';
        }

        PaymentBank::create($requestData);

       /* if($condition->statusAdmin == 1 && $condition->statusApp == 1){
            return redirect('admin/access/condition');
        }else{
            return Redirect::to('/admin/access/condition/' . $requestData['conditionId']);
        }*/

       return $this->create($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentbank = PaymentBank::findOrFail($id);

        return view('backend/access.payment-bank.show', compact('paymentbank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentbank = PaymentBank::findOrFail($id);

        return view('backend/access.payment-bank.edit', compact('paymentbank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();


        if ($request->hasFile('scanDepositSlip')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('scanDepositSlip')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('scanDepositSlip')->move($uploadPath, $fileName);
            $requestData['scanDepositSlip'] = $fileName;
        }

        $paymentbank = PaymentBank::findOrFail($id);
        $paymentbank->update($requestData);

        Session::flash('flash_message', 'PaymentBank updated!');

        return redirect('admin/access/payment-bank');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PaymentBank::destroy($id);

        Session::flash('flash_message', 'PaymentBank deleted!');

        return redirect('admin/access/payment-bank');
    }
}
