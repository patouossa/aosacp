<?php

namespace App\Http\Controllers\Backend\Access\MobileTransfer;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Billing\Billing;
use App\Models\Access\Institution\Institution;
use App\Models\Access\MobileTransfer\MobileTransfer;
use Illuminate\Http\Request;
use Session;

class MobileTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        if(empty($user->type)){
            $mobiletransfer = MobileTransfer::paginate(25);
        }else{
            $mobiletransfer = MobileTransfer::where('institutionEmail',$user->email)->paginate(25);
        }

        return view('backend/access.mobile-transfer.index', compact('mobiletransfer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $mobiletransfer = '';
        $institution = Institution::get();
        return view('backend/access.mobile-transfer.create',['mobiletransfer'=>$mobiletransfer,'institution'=>$institution]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }
        
        MobileTransfer::create($requestData);

        Session::flash('flash_message', 'MobileTransfer added!');

        return redirect('admin/access/mobile-transfer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mobiletransfer = MobileTransfer::findOrFail($id);

        return view('backend/access.mobile-transfer.show', compact('mobiletransfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mobiletransfer = MobileTransfer::findOrFail($id);
        $institution = Institution::get();
        return view('backend/access.mobile-transfer.edit', compact('mobiletransfer'),['institution'=>$institution]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $mobiletransfer = MobileTransfer::findOrFail($id);
        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }

        $mobiletransfer->update($requestData);

        Session::flash('flash_message', 'MobileTransfer updated!');

        return redirect('admin/access/mobile-transfer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MobileTransfer::destroy($id);

        Session::flash('flash_message', 'MobileTransfer deleted!');

        return redirect('admin/access/mobile-transfer');
    }
}
