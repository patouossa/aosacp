<?php

namespace App\Http\Controllers\Backend\Access\Qualification;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Qualification\Qualification;
use Illuminate\Http\Request;
use Session;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $qualification = Qualification::paginate(25);

        return view('backend/access.qualification.index', compact('qualification'), ['student1' => $user, 'qualification' =>  $qualification]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();
        return view('backend/access.qualification.create',['student1'=> $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Qualification::create($requestData);

        Session::flash('flash_message', 'Qualification added!');

        return redirect('admin/access/qualification');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $qualification = Qualification::findOrFail($id);

        return view('backend/access.qualification.show', compact('qualification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $qualification = Qualification::findOrFail($id);

        return view('backend/access.qualification.edit', compact('qualification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $qualification = Qualification::findOrFail($id);
        $qualification->update($requestData);

        Session::flash('flash_message', 'Qualification updated!');

        return redirect('admin/access/qualification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Qualification::destroy($id);

        Session::flash('flash_message', 'Qualification deleted!');

        return redirect('admin/access/qualification');
    }
}
