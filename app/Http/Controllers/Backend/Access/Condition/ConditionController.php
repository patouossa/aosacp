<?php

namespace App\Http\Controllers\Backend\Access\Condition;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BankAccount\BankAccount;
use App\Models\Access\Condition\Condition;
use App\Models\Access\ExpressCashWallet\ExpressCashWallet;
use App\Models\Access\MobileTransfer\MobileTransfer;
use App\Models\Access\MoneyTransfer\MoneyTransfer;
use App\Models\Access\Student\Student;
use Illuminate\Http\Request;
use Session;

class ConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();

        $condition = Condition::paginate(25);

        $student1 = $student;

        foreach ($condition as $key=>$cond){
            $cond->studentId = $student->id;
            $condition[$key] = $cond;
        }

        return view('backend/access.condition.index', compact('condition'),['student1'=>$student1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.condition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Condition::create($requestData);

        Session::flash('flash_message', 'Condition added!');

        return redirect('admin/access/condition');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $condition = Condition::findOrFail($id);

        $bankaccount = BankAccount::where('institutionId',$condition->institutionId)->where('adminEmail',null)->get();
        $expresscash = ExpressCashWallet::where('institutionId',$condition->institutionId)->where('adminEmail',null)->get();
        $mobiletransfer = MobileTransfer::where('institutionId',$condition->institutionId)->where('adminEmail',null)->get();
        $moneytransfer = MoneyTransfer::where('institutionId',$condition->institutionId)->where('adminEmail',null)->get();

        //for Admin
        $bankaccountAdmin = BankAccount::where('adminEmail',$condition->adminId)->where('institutionId',$condition->institutionId)->get();
        $expresscashAdmin = ExpressCashWallet::where('adminEmail',$condition->adminId)->where('institutionId',$condition->institutionId)->get();
        $mobiletransferAdmin = MobileTransfer::where('adminEmail',$condition->adminId)->where('institutionId',$condition->institutionId)->get();
        $moneytransferAdmin = MoneyTransfer::where('adminEmail',$condition->adminId)->where('institutionId',$condition->institutionId)->get();

        $user = \Auth::user();
        $student1 = Student::where('email', $user->email)->first();

        return view('backend/access.condition.show', compact('condition'),
            ['bankaccount'=>$bankaccount,'expresscash'=>$expresscash,
                'mobiletransfer'=>$mobiletransfer,'moneytransfer'=>$moneytransfer,
                'bankaccountAdmin'=>$bankaccountAdmin,'expresscashAdmin'=>$expresscashAdmin,
                'mobiletransferAdmin'=>$mobiletransferAdmin,'moneytransferAdmin'=>$moneytransferAdmin,'student1'=>$student1,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $condition = Condition::findOrFail($id);

        return view('backend/access.condition.edit', compact('condition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $condition = Condition::findOrFail($id);
        $condition->update($requestData);

        Session::flash('flash_message', 'Condition updated!');

        return redirect('admin/access/condition');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Condition::destroy($id);

        Session::flash('flash_message', 'Condition deleted!');

        return redirect('admin/access/condition');
    }
}
