<?php

namespace App\Http\Controllers\Backend\Access\SchoolInformation;

use App\Http\Controllers\Backend\Access\Student\StudentController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Institution\Institution;
use App\Models\Access\Qualification\Qualification;
use App\Models\Access\SchoolInformation\SchoolInformation;
use App\Models\Access\Student\Student;
use Illuminate\Http\Request;
use Session;

class SchoolInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $schoolinformation = SchoolInformation::paginate(25);
        $user = \Auth::user();
        $student1 = Student::where('email', $user->email)->first();
        //return view('backend/access.school-information.create', compact('schoolinformation'),['student1'=>$student1]);
       return $this->create();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();
        $student1 = $student;
        $institution = Institution::get();
        $schoolinformations = SchoolInformation::where('studentId', $student->id)->paginate(25);
        $qualifications = Qualification::get();
        $schoolinformation = '';
        $count = SchoolInformation::where('studentId', $student->id)->count();
        return view('backend/access.school-information.create', compact('schoolinformations'),['schoolinformation'=>$schoolinformation,'institution'=>$institution,'count'=>$count,'qualifications'=>$qualifications,'student'=>$student,'student1'=>$student1]);
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $user = \Auth::user();
        $buttonValue = $requestData['close'];
        $email = $user->email;
        $student = Student::where('email', $email)->first();
        $requestData['studentId'] = $student->id;

        SchoolInformation::create($requestData);

        if($buttonValue == 'Save and Close'){
            return redirect('admin/access/summary');
        }else{
            return $this->create();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $schoolinformation = SchoolInformation::findOrFail($id);

        return view('backend/access.school-information.show', compact('schoolinformation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $schoolinformation = SchoolInformation::findOrFail($id);

        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $student1 = $student;
        $institution = Institution::get();

        $schoolinformations = SchoolInformation::where('studentId', $student->id)->paginate(25);
        $qualifications = Qualification::get();
        $count = SchoolInformation::where('studentId', $student->id)->count();

        return view('backend/access.school-information.edit', compact('schoolinformations'),['schoolinformation'=>$schoolinformation,'institution'=>$institution,'count'=>$count,'qualifications'=>$qualifications,'student1'=>$student1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $schoolinformation = SchoolInformation::findOrFail($id);
        $buttonValue = $requestData['close'];
        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;

        $schoolinformation->update($requestData);

        if($buttonValue == 'Save and Close'){
            return redirect('admin/access/summary');
        }else{
            return $this->create();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SchoolInformation::destroy($id);

        Session::flash('flash_message', 'SchoolInformation deleted!');

        return redirect('admin/access/school-information');
    }
}
