<?php

namespace App\Http\Controllers\Backend\Access\EtudiantInstitution;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\EtudiantInstitution\EtudiantInstitution;
use Illuminate\Http\Request;
use Session;

class EtudiantInstitutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $etudiantinstitution = EtudiantInstitution::paginate(25);

        return view('backend/access.etudiant-institution.index', compact('etudiantinstitution'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.etudiant-institution.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        EtudiantInstitution::create($requestData);

        Session::flash('flash_message', 'EtudiantInstitution added!');

        return redirect('admin/access/etudiant-institution');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $etudiantinstitution = EtudiantInstitution::findOrFail($id);

        return view('backend/access.etudiant-institution.show', compact('etudiantinstitution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $etudiantinstitution = EtudiantInstitution::findOrFail($id);

        return view('backend/access.etudiant-institution.edit', compact('etudiantinstitution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $etudiantinstitution = EtudiantInstitution::findOrFail($id);
        $etudiantinstitution->update($requestData);

        Session::flash('flash_message', 'EtudiantInstitution updated!');

        return redirect('admin/access/etudiant-institution');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EtudiantInstitution::destroy($id);

        Session::flash('flash_message', 'EtudiantInstitution deleted!');

        return redirect('admin/access/etudiant-institution');
    }
}
