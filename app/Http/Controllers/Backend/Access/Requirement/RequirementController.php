<?php

namespace App\Http\Controllers\Backend\Access\Requirement;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Institution\Institution;
use App\Models\Access\Requirement\Requirement;
use App\Models\Access\UploadRequirement\UploadRequirement;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Session;

class RequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $user = \Auth::user();

        if (empty($user->type)) {
            $uploadrequirement = UploadRequirement::paginate(25);

            foreach ($uploadrequirement as $key =>$upload){
                $institution = Institution::findOrFail($upload->institutionId);
                $upload->institution = $institution->nom;
                $uploadrequirement[$key] = $upload;

            }
            return view('backend/access.upload-requirement.index', compact('uploadrequirement'));

        } else if( strcmp ( $user->type , 'manage-univ' ) == 0){


            $institution = Institution::where('userEmail', $user->email)->first();


            $uploadRequirement = UploadRequirement::where('institutionId',$institution->id)->get();
            $ids = [];

            for ($i= 0; $i<count($uploadRequirement);$i++){
                $ids[]= array ('id'=>$uploadRequirement[$i]['requirementId']);
            }

            $requirement = Requirement::whereNotIn('id', $ids)->paginate(25);

        }

        return view('backend/access.requirement.index', compact('requirement'),['uploadrequirement'=>$uploadRequirement]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.requirement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        $institution = Institution::where('userEmail', $user->email)->first();

        foreach ($requestData['status'] as $status){
            $requirement = Requirement::findOrFail($status);
            $requestData['institutionId'] = $institution->id;
            $requestData['requirementId'] = $requirement->id;

            $lang = Config::get('app.locale');

                if (strcmp ( $lang , 'en' ) == 0){
                    $requestData['libelle'] = $requirement->libelleEN;
                }else{
                    $requestData['libelle'] = $requirement->libelleFR;
                }

            UploadRequirement::create($requestData);
        }
        //Requirement::create($requestData);

        Session::flash('flash_message', 'Requirement added!');

        return redirect('admin/access/requirement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $requirement = Requirement::findOrFail($id);

        return view('backend/access.requirement.show', compact('requirement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $requirement = Requirement::findOrFail($id);

        return view('backend/access.requirement.edit', compact('requirement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $requirement = Requirement::findOrFail($id);
        $requirement->update($requestData);

        Session::flash('flash_message', 'Requirement updated!');

        return redirect('admin/access/requirement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Requirement::destroy($id);

        Session::flash('flash_message', 'Requirement deleted!');

        return redirect('admin/access/requirement');
    }
}
