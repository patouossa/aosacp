<?php

namespace App\Http\Controllers\Backend\Access\PaymentTransfer;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Condition\Condition;
use App\Models\Access\Institution\Institution;
use App\Models\Access\MoneyTransfer\MoneyTransfer;
use App\Models\Access\PaymentTransfer\PaymentTransfer;
use App\Models\Access\Student\Student;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Session;

class PaymentTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $paymenttransfer = PaymentTransfer::paginate(25);

        return view('backend/access.payment-transfer.index', compact('paymenttransfer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $id = $request->get('id');
        $conditionId = $request->get('cond');

        $moneyTransfer = MoneyTransfer::findOrFail($id);

        return view('backend/access.payment-transfer.create', ['moneyTransfer' => $moneyTransfer,'conditionId' => $conditionId,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        dd($requestData);

        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();

        $requestData['studentId'] = $student->id;
        $requestData['student'] = $student->nom;
        $requestData['studentPhone'] = $student->phone;
        $condition = Condition::findOrFail($requestData['conditionId']);

        if(!empty($requestData['institutionId'])){
            $institution = Institution::findOrFail($requestData['institutionId']);
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->email;
            $conditionData['status'] = 2;
            $conditionData['statusApp'] = true;
            $condition->update($conditionData);

            $studentData['status'] = 3;
            $student->update($studentData);
        }

        if(!empty($requestData['adminEmail'])){
            $adminUser = User::where('email',$requestData['adminEmail'])->first();
            $requestData['adminId'] = $adminUser->id;
            $conditionData['status'] = 1;
            $conditionData['statusAdmin'] = true;
            $condition->update($conditionData);
        }
        
        PaymentTransfer::create($requestData);

        if($condition->statusAdmin == true && $condition->statusApp == true){
            return redirect('admin/access/condition');
        }else{
            return Redirect::to('/admin/access/condition/' . $requestData['conditionId']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymenttransfer = PaymentTransfer::findOrFail($id);

        return view('backend/access.payment-transfer.show', compact('paymenttransfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymenttransfer = PaymentTransfer::findOrFail($id);

        return view('backend/access.payment-transfer.edit', compact('paymenttransfer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $paymenttransfer = PaymentTransfer::findOrFail($id);
        $paymenttransfer->update($requestData);

        Session::flash('flash_message', 'PaymentTransfer updated!');

        return redirect('admin/access/payment-transfer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PaymentTransfer::destroy($id);

        Session::flash('flash_message', 'PaymentTransfer deleted!');

        return redirect('admin/access/payment-transfer');
    }
}
