<?php

namespace App\Http\Controllers\Backend\Access\MoneyTransfer;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Billing\Billing;
use App\Models\Access\Institution\Institution;
use App\Models\Access\MoneyTransfer\MoneyTransfer;
use Illuminate\Http\Request;
use Session;

class MoneyTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        if(empty($user->type)){
            $moneytransfer = MoneyTransfer::paginate(25);
        }else{
            $moneytransfer = MoneyTransfer::where('institutionEmail',$user->email)->paginate(25);
        }

        return view('backend/access.money-transfer.index', compact('moneytransfer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $institution = Institution::get();
        $moneytransfer='';
        return view('backend/access.money-transfer.create',['moneytransfer'=>$moneytransfer,'institution'=>$institution]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }
        
        MoneyTransfer::create($requestData);

        Session::flash('flash_message', 'MoneyTransfer added!');

        return redirect('admin/access/money-transfer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $moneytransfer = MoneyTransfer::findOrFail($id);

        return view('backend/access.money-transfer.show', compact('moneytransfer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $moneytransfer = MoneyTransfer::findOrFail($id);
        $institution = Institution::get();
        return view('backend/access.money-transfer.edit', compact('moneytransfer'),['institution'=>$institution]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $moneytransfer = MoneyTransfer::findOrFail($id);

        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }

        $moneytransfer->update($requestData);

        Session::flash('flash_message', 'MoneyTransfer updated!');

        return redirect('admin/access/money-transfer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MoneyTransfer::destroy($id);

        Session::flash('flash_message', 'MoneyTransfer deleted!');

        return redirect('admin/access/money-transfer');
    }
}
