<?php

namespace App\Http\Controllers\Backend\Access\Marital;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Marital\Marital;
use Illuminate\Http\Request;
use Session;

class MaritalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $marital = Marital::paginate(25);

        return view('backend/access.marital.index', compact('marital'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.marital.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Marital::create($requestData);

        Session::flash('flash_message', 'Marital added!');

        return redirect('admin/access/marital');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $marital = Marital::findOrFail($id);

        return view('backend/access.marital.show', compact('marital'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $marital = Marital::findOrFail($id);

        return view('backend/access.marital.edit', compact('marital'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $marital = Marital::findOrFail($id);
        $marital->update($requestData);

        Session::flash('flash_message', 'Marital updated!');

        return redirect('admin/access/marital');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Marital::destroy($id);

        Session::flash('flash_message', 'Marital deleted!');

        return redirect('admin/access/marital');
    }
}
