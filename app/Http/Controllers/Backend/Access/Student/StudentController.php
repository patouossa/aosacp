<?php

namespace App\Http\Controllers\Backend\Access\Student;

use App\Http\Controllers\Backend\Access\Certificate\CertificateController;
use App\Http\Controllers\Backend\Access\SchoolInformation\SchoolInformationController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\EtudiantInstitution\EtudiantInstitution;
use App\Models\Access\Examination\Examination;
use App\Models\Access\Filiere\Filiere;
use App\Models\Access\Institution\Institution;
use App\Models\Access\Marital\Marital;
use App\Models\Access\ProgramStudy\Programstudie;
use App\Models\Access\Region\Region;
use App\Models\Access\Religion\Religion;
use App\Models\Access\Requirement\Requirement;
use App\Models\Access\SchoolInformation\SchoolInformation;
use App\Models\Access\Student\Student;
use App\Models\Access\UploadRequirement\UploadRequirement;
use App\Models\Access\Ville\Ville;
use ClassPreloader\Config;
use Illuminate\Http\Request;
use Session;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = \Auth::user();
        $status = $request->get('status');
        $student1 = Student::where('email',$user->email)->first();

        if (empty($user->type)) {
            $student = Student::whereIn('status', array(1, 2, 3))->paginate(25);
        } else if( strcmp ( $user->type , 'manage-univ' ) == 0){
            if($status == 1){
             $student = Student::where('institutionEmail', $user->email)->whereIn('status', array(2, 3,4,5,6))->paginate(25);
            }else if($status == 2){

                $student = Student::where('institutionEmail', $user->email)->where('status',2)->paginate(25);
            }else if($status == 3){
                $student = Student::where('institutionEmail', $user->email)->where('status',3)->paginate(25);
            }else if($status == 4){
                $student = Student::where('institutionEmail', $user->email)->where('status',4)->paginate(25);
            }else if($status == 5){
                $student = Student::where('institutionEmail', $user->email)->where('status',5)->paginate(25);
            }else if($status == 6){
                $student = Student::where('institutionEmail', $user->email)->where('status',6)->paginate(25);
            }else{
                $student = Student::where('institutionEmail', $user->email)->paginate(25);
            }
        }else {
            $student = Student::where('email', $user->email)->paginate(25);
        }

        return view('backend/access.student.index', compact('student'),['student1'=>$student1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $institution = Institution::get();
        $student = '';
        $region = Region::get();
        $ville = Ville::get();
        $edit = false;
        $religion = Religion::get();
        $marital = Marital::get();
        return view('backend/access.student.create',['institution' => $institution,'student' => $student,'region' => $region, 'ville' => $ville, 'edit' => $edit,
            'marital' => $marital, 'religion' => $religion]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        $lang = \Illuminate\Support\Facades\Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }

        }

        if(!empty($requestData['ha'])){
            $marital = Marital::findOrFail($requestData['statusmatrimonialId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['statusmatrimonial'] = $marital->libelleEN;
            }else{
                $requestData['statusmatrimonial'] = $marital->libelleFR;
            }

        }

        if(!empty($requestData['religionId'])){
            $religion = Religion::findOrFail($requestData['religionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['religion'] = $religion->libelleEN;
            }else{
                $requestData['religion'] = $religion->libelleFR;
            }

        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        if(!empty($requestData['villeparentId'])){
            $ville = Ville::findOrFail($requestData['villeparentId']);
            $requestData['villeparent'] = $ville->libelle;
        }

        $requestData['userEmail'] = $user->email;
        
        Student::create($requestData);

        $schoolInfoController = new SchoolInformationController();


        return $schoolInfoController->create();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        //$student = Student::findOrFail($id);

        $institutionId = $request->get('institutionId');

        $certificateController = new CertificateController();

        return $certificateController->gotoDetails($id,$institutionId);

        //return view('backend/access.student.show', compact('student'));
    }

    public function reject($id){
        $student = Student::findOrFail($id);

        $requestData['status'] = 3;

        $student->update($requestData);

        return $this->index();
    }

    public function accept($id){
        $student = Student::findOrFail($id);

        $requestData['status'] = 2;

        $student->update($requestData);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = \Auth::user();
        $student = Student::findOrFail($id);
        $region = Region::get();
        $ville = Ville::get();
        $edit = true;
        $religion = Religion::get();
        $marital = Marital::get();
        $student1 = Student::where('email',$user->email)->first();
        return view('backend/access.student.edit', compact('student'),['region' => $region, 'ville' => $ville,
            'edit' => $edit,'marital' => $marital,
            'religion' => $religion,'student1'=>$student1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $student = Student::findOrFail($id);

        $user = \Auth::user();

        $lang = \Illuminate\Support\Facades\Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }

        }

        if(!empty($requestData['religionId'])){
            $religion = Religion::findOrFail($requestData['religionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['religion'] = $religion->libelleEN;
            }else{
                $requestData['religion'] = $religion->libelleFR;
            }

        }

        if(!empty($requestData['statusmatrimonialId'])){
            $marital = Marital::findOrFail($requestData['statusmatrimonialId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['statusmatrimonial'] = $marital->libelleEN;
            }else{
                $requestData['statusmatrimonial'] = $marital->libelleFR;
            }
        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        if(!empty($requestData['villeparentId'])){
            $ville = Ville::findOrFail($requestData['villeparentId']);
            $requestData['villeparent'] = $ville->libelle;
        }

        if ($request->hasFile('photo')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('photo')->move($uploadPath, $fileName);
            $requestData['photo'] = '/uploads/' . $fileName;
        } else {
            $requestData['photo'] = '';
        }

        $student->update($requestData);

        $schoolInfoController = new SchoolInformationController();
        //return $this->gotoSummary();

        return redirect('admin/access/summary');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Student::destroy($id);

        Session::flash('flash_message', 'Student deleted!');

        return redirect('admin/access/student');
    }

    protected function saveValueInSession($emailStudent)
    {
        // enregistre dans la session
        session(['emailStudent' => $emailStudent]);
    }

    public function getEmailStudentFromSession()
    {
        return session('emailStudent');
    }

    public function gotoSummary(){
        $user = \Auth::user();
        $student1 = Student::where('email',$user->email)->first();
        $countEducation = SchoolInformation::where('studentId',$student1->id)->count();
        $educationFirst = SchoolInformation::where('studentId',$student1->id)->first();
        $educationLast = SchoolInformation::where('studentId',$student1->id)->orderBy('created_at', 'desc')->first();
        $countExamination = Examination::where('studentId',$student1->id)->count();
        $examinationFirst = Examination::where('studentId',$student1->id)->first();
        $examinationEnd = Examination::where('studentId',$student1->id)->orderBy('created_at', 'desc')->first();;

        return view('backend/access.student.summary',['countEducation' => $countEducation,'educationFirst' => $educationFirst,'educationLast' => $educationLast,
            'countExamination' => $countExamination,'examinationFirst' => $examinationFirst,'examinationEnd' => $examinationEnd,'student1'=>$student1]);
    }

    public  function gotoInstitutionSummary(){
        $user = \Auth::user();
        $student1 = Student::where('email',$user->email)->first();
        $programestudy = Programstudie::where('studentId',$student1->id)->distinct('institution')->get();
        $nbStudy = Programstudie::where('studentId',$student1->id)->count();
        foreach ($programestudy as $key=>$study){
            $nbRequirement =  UploadRequirement::where('institutionId',$study->institutionId)->count();
            $study['nbChoise'] = $nbStudy;
            $study['nbRequirement'] = $nbRequirement;
            $programestudy[$key] = $study;
        }
        return view('backend/access.student.institutionSummary',['student1'=>$student1,'programstudie'=>$programestudy,]);

    }

    public function goToDetailInstitution($institutionId){

        $user = \Auth::user();
        $student1 = Student::where('email',$user->email)->first();
        $studyprogram = Programstudie::where('studentId',$student1->id)->where('institutionId',$institutionId)->first();
        $studyprograms = Programstudie::where('studentId',$student1->id)->where('institutionId',$institutionId)->get();
        $nbStudy = Programstudie::where('studentId',$student1->id)->count();

        foreach ($studyprograms as $key=>$study){
            $filiere = Filiere::findOrfail($study->filiereId);
            $institution = Institution::findOrfail($study->institutionId);
            $study['nbChoise'] = $institution->nbChoise;
            $study['nbPlace'] = $filiere->nbPlace;
            $studyprograms[$key] = $study;
        }
        return view('backend/access.student.institutionDetail',['student1'=>$student1,'studyprograms'=>$studyprograms,'studyprogram'=>$studyprogram,'nbStudy'=>$nbStudy,]);

    }
}
