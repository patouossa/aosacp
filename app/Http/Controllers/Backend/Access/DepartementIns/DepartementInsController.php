<?php

namespace App\Http\Controllers\Backend\Access\DepartementIns;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\DepartementIns\DepartementIn;
use App\Models\Access\Faculte\Faculte;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class DepartementInsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $departementins = DepartementIn::paginate(25);
        } else {
            $departementins = DepartementIn::where('userEmail', $user->email)->paginate(25);
        }

        return view('backend/access.departement-ins.index', [compact('departementins'), 'student1' => $user, 'departementins' => $departementins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $facultes = Faculte::get();
        } else {
            $facultes = Faculte::where('userEmail', $user->email)->get();
        }

        $departementin = '';
        return view('backend/access.departement-ins.create',['facultes'=>$facultes,'departementin'=>$departementin,'student1' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        $requestData['userId'] = $user->id;
        $requestData['userEmail'] = $user->email;

        $faculteId = $requestData['faculteId'];

        $faculte = Faculte::where('id', $faculteId)->first();

        $requestData['faculte'] = $faculte->nom;

        DepartementIn::create($requestData);

        Session::flash('flash_message', 'DepartementIn added!');

        return redirect('admin/access/departement-ins');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $departementin = DepartementIn::findOrFail($id);

        return view('backend/access.departement-ins.show', compact('departementin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $departementin = DepartementIn::findOrFail($id);
        $user = \Auth::user();

        if (empty($user->type)) {
            $facultes = Faculte::get();
        } else {
            $facultes = Faculte::where('userEmail', $user->email)->get();
        }

        return view('backend/access.departement-ins.edit', compact('departementin'),['facultes'=>$facultes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();

        $faculteId = $requestData['faculteId'];

        $faculte = Faculte::where('id', $faculteId)->first();

        $requestData['faculte'] = $faculte->nom;

        $departementin = DepartementIn::findOrFail($id);
        $departementin->update($requestData);

        Session::flash('flash_message', 'DepartementIn updated!');

        return redirect('admin/access/departement-ins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DepartementIn::destroy($id);

        Session::flash('flash_message', 'DepartementIn deleted!');

        return redirect('admin/access/departement-ins');
    }
}
