<?php

namespace App\Http\Controllers\Backend\Access\DeliberationList;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\DepartementIns\DepartementIn;
use App\Models\Access\Niveau\Niveau;
use Illuminate\Http\Request;
use Session;

class DeliberationListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $niveau = Niveau::paginate(25);
        } else {
            $niveau = Niveau::where('userEmail', $user->email)->paginate(25);
        }
        return view('backend/access.dlist.index',['student1'=> $user, 'niveau'=> $niveau]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $departementins = DepartementIn::get();
        } else {
            $departementins = DepartementIn::where('userEmail', $user->email)->get();
        }
        $niveau = '';
        return view('backend/access.dlist.create',['student1'=> $user, 'departement'=>$departementins,'niveau'=>$niveau]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        $requestData['userEmail'] = $user->email;

        $departementId = $requestData['departementId'];

        $departement = DepartementIn::where('id', $departementId)->first();

        $requestData['departement'] = $departement->libelle;
        
        Niveau::create($requestData);

        Session::flash('flash_message', 'Niveau added!');

        return redirect('admin/access/dlist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $niveau = Niveau::findOrFail($id);

        return view('backend/access.dlist.show', compact('niveau'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = \Auth::user();

        $niveau = Niveau::findOrFail($id);

        if (empty($user->type)) {
            $departementins = DepartementIn::get();
        } else {
            $departementins = DepartementIn::where('userEmail', $user->email)->get();
        }

        $requestData['userEmail'] = $user->email;

        return view('backend/access.niveau.edit', compact('niveau'),['departement'=>$departementins]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();

        $departementId = $requestData['departementId'];

        $departement = DepartementIn::where('id', $departementId)->first();

        $requestData['departement'] = $departement->libelle;
        
        $niveau = Niveau::findOrFail($id);
        $niveau->update($requestData);

        Session::flash('flash_message', 'Niveau updated!');

        return redirect('admin/access/dlist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Niveau::destroy($id);

        Session::flash('flash_message', 'Niveau deleted!');

        return redirect('admin/access/dlist');
    }
}
