<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Models\Access\Student\Student;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class AccountController.
 */
class ProfilController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request)
    {
        $this->user->updateProfile(access()->id(), $request->all());

        return redirect()->route('admin.access.profil.index')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $student1 = Student::where('email',$user->email)->first();

        return view('backend.access.user.account',['student1'=>$student1]);
    }


    /**
     * @param ChangePasswordRequest $request
     *
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $this->user->changePassword($request->all());

        return redirect()->route('admin.access.profil.index')->withFlashSuccess(trans('strings.frontend.user.password_updated'));
    }
}
