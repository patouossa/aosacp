<?php

namespace App\Http\Controllers\Backend\Access\BankAccount;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BankAccount\BankAccount;
use App\Models\Access\Billing\Billing;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {


        $user = \Auth::user();
        if(empty($user->type)){
            $bankaccount = BankAccount::paginate(25);

        }else{
            $bankaccount = BankAccount::where('institutionEmail',$user->email)->paginate(25);

        }


        return view('backend/access.bank-account.index', compact('bankaccount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $institution = Institution::get();
        $bankaccount = '';
        return view('backend/access.bank-account.create',['bankaccount'=>$bankaccount,'institution'=>$institution]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }

        BankAccount::create($requestData);

        Session::flash('flash_message', 'BankAccount added!');

        return redirect('admin/access/bank-account');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bankaccount = BankAccount::findOrFail($id);

        return view('backend/access.bank-account.show', compact('bankaccount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bankaccount = BankAccount::findOrFail($id);
        $institution = Institution::get();

        return view('backend/access.bank-account.edit', compact('bankaccount'),['institution'=>$institution]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $bankaccount = BankAccount::findOrFail($id);


        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $institution = Institution::where('id',$requestData['institutionId'])->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }
        $bankaccount->update($requestData);

        Session::flash('flash_message', 'BankAccount updated!');

        return redirect('admin/access/bank-account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BankAccount::destroy($id);

        Session::flash('flash_message', 'BankAccount deleted!');

        return redirect('admin/access/bank-account');
    }
}
