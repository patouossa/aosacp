<?php

namespace App\Http\Controllers\Backend\Access\Religion;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Religion\Religion;
use Illuminate\Http\Request;
use Session;

class ReligionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $religion = Religion::paginate(25);

        return view('backend/access.religion.index', compact('religion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.religion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Religion::create($requestData);

        Session::flash('flash_message', 'Religion added!');

        return redirect('admin/access/religion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $religion = Religion::findOrFail($id);

        return view('backend/access.religion.show', compact('religion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $religion = Religion::findOrFail($id);

        return view('backend/access.religion.edit', compact('religion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $religion = Religion::findOrFail($id);
        $religion->update($requestData);

        Session::flash('flash_message', 'Religion updated!');

        return redirect('admin/access/religion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Religion::destroy($id);

        Session::flash('flash_message', 'Religion deleted!');

        return redirect('admin/access/religion');
    }
}
