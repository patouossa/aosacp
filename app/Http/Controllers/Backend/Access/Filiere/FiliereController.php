<?php

namespace App\Http\Controllers\Backend\Access\Filiere;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\DepartementIns\DepartementIn;
use App\Models\Access\Filiere\Filiere;
use App\Models\Access\Niveau\Niveau;
use Illuminate\Http\Request;
use Session;

class FiliereController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $filiere = Filiere::paginate(25);
        } else {
            $filiere = Filiere::where('userEmail', $user->email)->paginate(25);
        }

        return view('backend/access.filiere.index', compact('filiere'),['student1'=> $user, 'filiere'=> $filiere]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = \Auth::user();

        if (empty($user->type)) {
            $departementins = DepartementIn::get();
            $niveau = Niveau::get();
        } else {
            $departementins = DepartementIn::where('userEmail', $user->email)->get();
            $niveau = Niveau::where('userEmail', $user->email)->get();
        }
        $filiere = '';
        return view('backend/access.filiere.create',['departement'=>$departementins,'filiere'=>$filiere,'niveau'=>$niveau],['student1' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        $requestData['userId'] = $user->id;
        $requestData['userEmail'] = $user->email;

        $departementId = $requestData['departementId'];

        $departement = DepartementIn::where('id', $departementId)->first();

        $requestData['departemnent'] = $departement->libelle;

        $niveauId = $requestData['niveauId'];

        $niveau = Niveau::where('id', $niveauId)->first();

        $requestData['niveau'] = $niveau->niveau;
        
        Filiere::create($requestData);

        Session::flash('flash_message', 'Filiere added!');

        return redirect('admin/access/filiere');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $filiere = Filiere::findOrFail($id);

        return view('backend/access.filiere.show', compact('filiere'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = \Auth::user();

        $filiere = Filiere::findOrFail($id);
        if (empty($user->type)) {
            $departementins = DepartementIn::get();
            $niveau = Niveau::get();
        } else {
            $departementins = DepartementIn::where('userEmail', $user->email)->paginate(25);
            $niveau = Niveau::where('userEmail', $user->email)->paginate(25);
        }

        return view('backend/access.filiere.edit', compact('filiere'),['departement'=>$departementins,'niveau'=>$niveau]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();

        $departementId = $requestData['departementId'];

        $departement = DepartementIn::where('id', $departementId)->first();

        $requestData['departemnent'] = $departement->libelle;

        $niveauId = $requestData['niveauId'];

        $niveau = Niveau::where('id', $niveauId)->first();

        $requestData['niveau'] = $niveau->niveau;

        $filiere = Filiere::findOrFail($id);
        $filiere->update($requestData);

        Session::flash('flash_message', 'Filiere updated!');

        return redirect('admin/access/filiere');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Filiere::destroy($id);

        Session::flash('flash_message', 'Filiere deleted!');

        return redirect('admin/access/filiere');
    }
}
