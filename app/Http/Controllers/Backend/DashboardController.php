<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Access\Student\Student;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        $global = Student::where('institutionEmail', $user->email)->whereIn('status', array(1, 2, 3,4,5,6))->count();
        $encours = Student::where('institutionEmail', $user->email)->where('status',2)->count();
        $active = Student::where('institutionEmail', $user->email)->where('status',3)->count();
        $paid = Student::where('institutionEmail', $user->email)->where('status',4)->count();
        $aknowledge = Student::where('institutionEmail', $user->email)->where('status',5)->count();
        $verified = Student::where('institutionEmail', $user->email)->where('status',6)->count();
        return view('backend.dashboard',['global'=>$global,'global'=>$global,'encours'=>$encours,
            'active'=>$active,'paid'=>$paid,'aknowledge'=>$aknowledge,'verified'=>$verified,]);
    }
}
