<?php

namespace App\Models\Access\ProgramStudy;

use Illuminate\Database\Eloquent\Model;

class Programstudie extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'programstudies';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId', 'institution','faculteId','faculte', 'departementId', 'departement', 'niveauId', 'niveau', 'filiereId', 'filiere','userEmail','studentId'];

    
}
