<?php

namespace App\Models\Access\Paymentmode;

use Illuminate\Database\Eloquent\Model;

class Paymentmode extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'paymentmodes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['typePaiementId', 'typePaiement', 'montant', 'institutionId', 'institution'];

    
}
