<?php

namespace App\Models\Access\Institution;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'institutions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'description', 'regionId', 'region', 'villeId','ville','bp', 'quartier', 'telephone', 'agreement', 'type','userId','userEmail','amountpreinscriptionLocal','amountpreinscriptionRemote','password','paieMoMo','paieBank','paieExpressCash','numCompte','numExpressCash','paie','beginDate','endDate',
    'nbChoise','contactName','contactPhone','contactEmail'];

    
}
