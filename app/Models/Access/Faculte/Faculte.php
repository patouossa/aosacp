<?php

namespace App\Models\Access\Faculte;

use Illuminate\Database\Eloquent\Model;

class Faculte extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facultes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'localisation','userEmail','institutionId','institution'];

    
}
