<?php

namespace App\Models\Access\EtudiantInstitution;

use Illuminate\Database\Eloquent\Model;

class EtudiantInstitution extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'etudiant_institutions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['studentId', 'institutionId','studentEmail','institutionEmail'];

    
}
