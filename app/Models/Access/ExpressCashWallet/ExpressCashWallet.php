<?php

namespace App\Models\Access\ExpressCashWallet;

use Illuminate\Database\Eloquent\Model;

class ExpressCashWallet extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'express_cash_wallets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionName', 'accountName', 'accountNumber', 'institutionId', 'institution','reference','adminEmail','institutionEmail'];

    
}
