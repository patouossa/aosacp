<?php

namespace App\Models\Access\Requirement;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'requirements';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['libelleEN', 'libelleFR', 'status', 'institutionId'];

    
}
