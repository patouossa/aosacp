<?php

namespace App\Models\Access\UploadRequirement;

use Illuminate\Database\Eloquent\Model;

class UploadRequirement extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'upload_requirements';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId', 'libelle', 'requirementId'];

    
}
