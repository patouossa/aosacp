<?php

namespace App\Models\Access\MoneyTransfer;

use Illuminate\Database\Eloquent\Model;

class MoneyTransfer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'money_transfers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['compnayName', 'beneficaryName', 'mobile', 'institutionId', 'institution','adminEmail','institutionEmail'];

    
}
