<?php

namespace App\Models\Access\PaymentExpresscash;

use Illuminate\Database\Eloquent\Model;

class PaymentExpresscash extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_expresscashes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId','institutionEmail',  'institution', 'adminId', 'adminEmail','slipNumber', 'dateDepot', 'studentId', 'student', 'studentPhone', 'amount', 'reference', 'transactionNumber', 'scanDepositSlip'];

    
}
