<?php

namespace App\Models\Access\Marital;

use Illuminate\Database\Eloquent\Model;

class Marital extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'maritals';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['libelleEN', 'libelleFR'];

    
}
