<?php

namespace App\Models\Access\PaymentBank;

use Illuminate\Database\Eloquent\Model;

class PaymentBank extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_banks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId','institutionEmail', 'institution', 'adminId', 'adminEmail','slipNumber', 'dateDepot', 'studentId', 'student', 'studentPhone', 'amount', 'reference', 'transactionNumber', 'scanDepositSlip'];

    
}
