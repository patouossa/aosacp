<?php

namespace App\Models\Access\Examination;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'examinations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['examinationyear', 'candidatenumber', 'examinationcenter', 'resultobtained', 'qualification','userEmail','studentId','schoolInformationId','mathematic','english','englishLitt','french'
    ,'economic','geography','biology','humanBiology','chemistry','physic','futherMaths','history','computerTech','englishLang','frenchLang','frenchLitt','nbSitting'];

    
}
