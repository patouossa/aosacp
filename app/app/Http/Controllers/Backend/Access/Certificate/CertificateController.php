<?php

namespace App\Http\Controllers\Backend\Access\Certificate;

use App\Http\Controllers\Backend\Access\ProframStudy\ProgramstudieController;
use App\Http\Controllers\Backend\Access\Student\StudentController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BankAccount\BankAccount;
use App\Models\Access\Billing\Billing;
use App\Models\Access\BillingConfig\BillingConfig;
use App\Models\Access\Certificate\Certificate;
use App\Models\Access\Condition\Condition;
use App\Models\Access\Examination\Examination;
use App\Models\Access\ExpressCashWallet\ExpressCashWallet;
use App\Models\Access\Institution\Institution;
use App\Models\Access\MobileTransfer\MobileTransfer;
use App\Models\Access\MoneyTransfer\MoneyTransfer;
use App\Models\Access\ProgramStudy\Programstudie;
use App\Models\Access\SchoolInformation\SchoolInformation;
use App\Models\Access\Student\Student;
use App\Models\Access\User\User;
use Illuminate\Http\Request;
use Session;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $certificate = Certificate::paginate(25);

        return view('backend/access.certificate.index', compact('certificate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();

        $requestData['studentId'] = $student->id;

        if ($request->hasFile('handWriting')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('handWriting')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('handWriting')->move($uploadPath, $fileName);

            $requestData['handWriting'] = '/uploads/' . $fileName;
        } else {
            $requestData['handWriting'] = '';
        }


        if ($request->hasFile('certifiedQualification')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('certifiedQualification')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('certifiedQualification')->move($uploadPath, $fileName);

            $requestData['certifiedQualification'] = '/uploads/' . $fileName;
        } else {
            $requestData['certifiedQualification'] = '';
        }

        if ($request->hasFile('birth')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('birth')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('birth')->move($uploadPath, $fileName);

            $requestData['birth'] = '/uploads/' . $fileName;
        } else {
            $requestData['birth'] = '';
        }

        if ($request->hasFile('transcriptgcea')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('transcriptgcea')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('transcriptgcea')->move($uploadPath, $fileName);
            $requestData['transcriptgcea'] = '/uploads/' . $fileName;
        } else {
            $requestData['transcriptgcea'] = '';
        }

        if ($request->hasFile('applicationForm')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('applicationForm')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('applicationForm')->move($uploadPath, $fileName);
            $requestData['applicationForm'] = '/uploads/' . $fileName;
        } else {
            $requestData['applicationForm'] = '';
        }
        if ($request->hasFile('certificate')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('certificate')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('certificate')->move($uploadPath, $fileName);
            $requestData['certificate'] = '/uploads/' . $fileName;
        } else {
            $requestData['certificate'] = '';
        }

        $user = \Auth::user();
        $student = Student::where('email', $user->email)->first();


        $requestData['studentId'] = $student->id;
        $requestData['userEmail'] = $user->email;
        $studentData['status'] = '2';

        $student->update($studentData);
        Certificate::create($requestData);
        $institution = Institution::findOrFail($requestData['institutionId']);
        $userAdmin = User::where('type',null)->first();

        $billing = Billing::where('institutionId',$institution->id)->first();

        $bankaccount = BankAccount::where('institutionId',$institution->id)->first();
        $expresscash = ExpressCashWallet::where('institutionId',$institution->id)->first();
        $mobiletransfer = MobileTransfer::where('institutionId',$institution->id)->first();
        $moneytransfer = MoneyTransfer::where('institutionId',$institution->id)->first();



        if ($bankaccount != null){
            $conditionData['bankAccountId']=$bankaccount->id;
        }

        if ($expresscash != null){
            $conditionData['expresscashId']=$expresscash->id;
        }

        if ($mobiletransfer != null){
            $conditionData['mobileTransferId']=$mobiletransfer->id;
        }

        if ($moneytransfer != null){
            $conditionData['moneyTransferId']=$moneytransfer->id;
        }

        //for Admin

        $bankaccountAdmin = BankAccount::where('adminEmail',$userAdmin->email)->first();
        $expresscashAdmin = ExpressCashWallet::where('adminEmail',$userAdmin->email)->first();
        $mobiletransferAdmin = MobileTransfer::where('adminEmail',$userAdmin->email)->first();
        $moneytransferAdmin = MoneyTransfer::where('adminEmail',$userAdmin->email)->first();

        if ($bankaccount != null){
            $conditionData['bankAccountAdminId']=$bankaccountAdmin->id;
        }

        if ($expresscash != null){
            $conditionData['expresscashAdminId']=$expresscashAdmin->id;
        }

        if ($mobiletransfer != null){
            $conditionData['mobileTransferAdminId']=$mobiletransferAdmin->id;
        }

        if ($moneytransfer != null){
            $conditionData['moneyTransferAdminId']=$moneytransferAdmin->id;
        }

        $conditionData['institutionId'] = $requestData['institutionId'];
        $conditionData['institution'] = $institution->nom;
        $conditionData['amountInstitution'] = $institution->amountpreinscriptionLocal;
        $conditionData['adminId'] = $userAdmin->email;
        $conditionData['amountAdmin'] = $billing->montantSysteme;
        Condition::create($conditionData);

        $programstudyController = new ProgramstudieController();



        return $programstudyController->create();
    }

    public function gotoDetails($studentId)
    {

        $schoolinformations = SchoolInformation::where('studentId', $studentId)->paginate(25);
        $certificate = Certificate::where('studentId', $studentId)->first();

        if ($certificate == null) {
            $certificate = '';
        }

        $user = \Auth::user();

        $institution = Institution::where('userEmail', $user->email)->first();

        if($institution == null){
            $programStudy = Programstudie::where('studentId', $studentId)->paginate(8);
        }else{
            $programStudy = Programstudie::where('studentId', $studentId)->where('institutionId', $institution->id)->paginate(8);
        }

        $examination = Examination::where('studentId', $studentId)->paginate(8);
        $student = Student::findOrFail($studentId);
        $billingConfig = BillingConfig::first();
        return view('backend/access.details.details', ['schoolinformations' => $schoolinformations, 'certificate' => $certificate, 'programStudy' => $programStudy, 'examinations' => $examination, 'student' => $student, 'institution' => $institution, 'billingConfig' => $billingConfig,]);
    }

    public function gotoSucess($studentId)
    {


        $certificate = Certificate::where('studentId', $studentId)->first();
        $programStudy = Programstudie::where('studentId', $studentId)->first();
        $student = Student::findOrFail($studentId);
        $institution = Institution::findOrFail($programStudy->institutionId);

        return view('backend/access.details.success', ['certificate' => $certificate, 'programStudy' => $programStudy, 'student' => $student, 'institution' => $institution,]);
    }

    public function billing(Request $request)
    {

        $requestData = $request->all();

        $student = Student::findOrFail($requestData['studentId']);
        $studentData['status'] = 1;

        $student->update($studentData);


        Billing::create($requestData);


        return $this->gotoSucess($student->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('backend/access.certificate.show', compact('certificate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $certificate = Certificate::findOrFail($id);

        return view('backend/access.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();


        if ($request->hasFile('handWriting')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('handWriting')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('handWriting')->move($uploadPath, $fileName);
            $requestData['handWriting'] = $fileName;
        }


        if ($request->hasFile('certifiedQualification')) {
            $uploadPath = public_path('/uploads/');

            $extension = $request->file('certifiedQualification')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('certifiedQualification')->move($uploadPath, $fileName);
            $requestData['certifiedQualification'] = $fileName;
        }

        $certificate = Certificate::findOrFail($id);
        $certificate->update($requestData);

        Session::flash('flash_message', 'Certificate updated!');

        return redirect('admin/access/certificate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Certificate::destroy($id);

        Session::flash('flash_message', 'Certificate deleted!');

        return redirect('admin/access/certificate');
    }
}
