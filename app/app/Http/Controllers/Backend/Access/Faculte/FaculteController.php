<?php

namespace App\Http\Controllers\Backend\Access\Faculte;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Faculte\Faculte;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class FaculteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $faculte = Faculte::paginate(25);

        $user = \Auth::user();

        if (empty($user->type)) {
            $faculte = Faculte::paginate(25);
        } else {
            $faculte = Faculte::where('userEmail', $user->email)->paginate(25);
        }

        return view('backend/access.faculte.index', compact('faculte'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.faculte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        $requestData['userEmail'] = $user->email;

        $institution = Institution::where('userEmail', $requestData['userEmail'])->first();
        $requestData['institutionId'] = $institution->id;
        $requestData['institution'] = $institution->nom;

        Faculte::create($requestData);

        Session::flash('flash_message', 'Faculte added!');

        return redirect('admin/access/faculte');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $faculte = Faculte::findOrFail($id);

        return view('backend/access.faculte.show', compact('faculte'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $faculte = Faculte::findOrFail($id);

        return view('backend/access.faculte.edit', compact('faculte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $faculte = Faculte::findOrFail($id);
        $faculte->update($requestData);

        Session::flash('flash_message', 'Faculte updated!');

        return redirect('admin/access/faculte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Faculte::destroy($id);

        Session::flash('flash_message', 'Faculte deleted!');

        return redirect('admin/access/faculte');
    }
}
