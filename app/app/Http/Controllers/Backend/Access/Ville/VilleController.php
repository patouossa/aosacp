<?php

namespace App\Http\Controllers\Backend\Access\Ville;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Region\Region;
use App\Models\Access\Ville\Ville;
use Illuminate\Http\Request;
use Session;

class VilleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $ville = Ville::paginate(25);

        return view('backend/access.ville.index', compact('ville'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $region = Region::get();
        $ville = '';
        return view('backend/access.ville.create',['region'=>$region,'ville'=>$ville]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $regionId = $requestData['regionId'];

        $region = Region::where('id', $regionId)->first();

        $requestData['libelleRegion'] = $region->libelle;
        
        Ville::create($requestData);

        Session::flash('flash_message', 'Ville added!');

        return redirect('admin/access/ville');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ville = Ville::findOrFail($id);
        $region = Region::get();
        return view('backend/access.ville.show', compact('ville'),['region'=>$region]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ville = Ville::findOrFail($id);

        return view('backend/access.ville.edit', compact('ville'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        $regionId = $requestData['regionId'];

        $region = Region::where('id', $regionId)->first();

        $requestData['libelleRegion'] = $region->libelle;

        $ville = Ville::findOrFail($id);
        $ville->update($requestData);

        Session::flash('flash_message', 'Ville updated!');

        return redirect('admin/access/ville');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ville::destroy($id);

        Session::flash('flash_message', 'Ville deleted!');

        return redirect('admin/access/ville');
    }
}
