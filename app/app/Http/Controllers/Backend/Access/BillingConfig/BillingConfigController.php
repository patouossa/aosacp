<?php

namespace App\Http\Controllers\Backend\Access\BillingConfig;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BillingConfig\BillingConfig;
use Illuminate\Http\Request;
use Session;

class BillingConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $billingconfig = BillingConfig::paginate(25);

        return view('backend/access.billing-config.index', compact('billingconfig'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.billing-config.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        $requestData['createBy'] = $user->email;
        
        BillingConfig::create($requestData);

        Session::flash('flash_message', 'BillingConfig added!');

        return redirect('admin/access/billing-config');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $billingconfig = BillingConfig::findOrFail($id);

        return view('backend/access.billing-config.show', compact('billingconfig'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $billingconfig = BillingConfig::findOrFail($id);

        return view('backend/access.billing-config.edit', compact('billingconfig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $billingconfig = BillingConfig::findOrFail($id);
        $billingconfig->update($requestData);

        Session::flash('flash_message', 'BillingConfig updated!');

        return redirect('admin/access/billing-config');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        BillingConfig::destroy($id);

        Session::flash('flash_message', 'BillingConfig deleted!');

        return redirect('admin/access/billing-config');
    }
}
