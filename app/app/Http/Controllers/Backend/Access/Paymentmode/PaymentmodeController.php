<?php

namespace App\Http\Controllers\Backend\Access\Paymentmode;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\BillingConfig\BillingConfig;
use App\Models\Access\Institution\Institution;
use App\Models\Access\Paymentmode\Paymentmode;
use Illuminate\Http\Request;
use Session;

class PaymentmodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();


        $institution = Institution::where('userEmail',  $user->email)->first();

        if (empty($user->type)) {
            $paymentmode = Paymentmode::paginate(10);
        } else {
            $paymentmode = Paymentmode::where('institutionId', $institution->id)->paginate(10);
        }

        return view('backend/access.paymentmode.index', compact('paymentmode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $billingConfig = BillingConfig::get();
        $paymentmode = '';
        return view('backend/access.paymentmode.create',['paymentmode'=>$paymentmode,'billingConfig'=>$billingConfig]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();
        $requestData['userEmail'] = $user->email;

        $institution = Institution::where('userEmail', $requestData['userEmail'])->first();
        $requestData['institutionId'] = $institution->id;
        $requestData['institution'] = $institution->nom;

        $billingConfig = BillingConfig::where('id', $requestData['typePaiementId'])->first();
        $requestData['typePaiement'] = $billingConfig->type;

        Paymentmode::create($requestData);

        Session::flash('flash_message', 'Paymentmode added!');

        return redirect('admin/access/paymentmode');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentmode = Paymentmode::findOrFail($id);

        return view('backend/access.paymentmode.show', compact('paymentmode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentmode = Paymentmode::findOrFail($id);
        $billingConfig = BillingConfig::get();

        return view('backend/access.paymentmode.edit', compact('paymentmode'),['billingConfig'=>$billingConfig]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $paymentmode = Paymentmode::findOrFail($id);

        $user = \Auth::user();
        $requestData['userEmail'] = $user->email;

        $institution = Institution::where('userEmail', $requestData['userEmail'])->first();
        $requestData['institutionId'] = $institution->id;
        $requestData['institution'] = $institution->nom;

        $billingConfig = BillingConfig::where('id', $requestData['typePaiementId'])->first();
        $requestData['typePaiement'] = $billingConfig->type;

        $paymentmode->update($requestData);

        Session::flash('flash_message', 'Paymentmode updated!');

        return redirect('admin/access/paymentmode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Paymentmode::destroy($id);

        Session::flash('flash_message', 'Paymentmode deleted!');

        return redirect('admin/access/paymentmode');
    }
}
