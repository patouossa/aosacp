<?php

namespace App\Http\Controllers\Backend\Access\UploadRequirement;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\UploadRequirement\UploadRequirement;
use Illuminate\Http\Request;
use Session;

class UploadRequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {


        $user = \Auth::user();

       /* if (empty($user->type)) {
            $uploadrequirement = UploadRequirement::paginate(25);
        } else if( strcmp ( $user->type , 'manage-univ' ) == 0){
            $uploadrequirement = UploadRequirement::where('institutionId')->paginate(25);
        }*/

        return view('backend/access.upload-requirement.index', compact('uploadrequirement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.upload-requirement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        UploadRequirement::create($requestData);

        Session::flash('flash_message', 'UploadRequirement added!');

        return redirect('admin/access/upload-requirement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $uploadrequirement = UploadRequirement::findOrFail($id);

        return view('backend/access.upload-requirement.show', compact('uploadrequirement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $uploadrequirement = UploadRequirement::findOrFail($id);

        return view('backend/access.upload-requirement.edit', compact('uploadrequirement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $uploadrequirement = UploadRequirement::findOrFail($id);
        $uploadrequirement->update($requestData);

        Session::flash('flash_message', 'UploadRequirement updated!');

        return redirect('admin/access/upload-requirement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        UploadRequirement::destroy($id);

        Session::flash('flash_message', 'UploadRequirement deleted!');

        return redirect('admin/access/upload-requirement');
    }
}
