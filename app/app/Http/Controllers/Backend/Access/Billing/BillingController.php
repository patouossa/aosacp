<?php

namespace App\Http\Controllers\Backend\Access\Billing;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Billing\Billing;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $billing = Billing::paginate(25);

        return view('backend/access.billing.index', compact('billing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $institutions = Institution::get();
        $billing='';
        return view('backend/access.billing.create',['billing'=>$billing,'institutions'=>$institutions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $institutionId = $requestData['institutionId'];

        $institution = Institution::where('id', $institutionId)->first();

        $requestData['institution'] = $institution->nom;
        
        Billing::create($requestData);

        Session::flash('flash_message', 'Billing added!');

        return redirect('admin/access/billing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $billing = Billing::findOrFail($id);
        return view('backend/access.billing.show', compact('billing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $billing = Billing::findOrFail($id);
        $institutions = Institution::get();
        return view('backend/access.billing.edit', compact('billing'),['institutions'=>$institutions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $billing = Billing::findOrFail($id);

        $institutionId = $requestData['institutionId'];
        $institution = Institution::where('id', $institutionId)->first();
        $requestData['institution'] = $institution->nom;

        $billing->update($requestData);

        Session::flash('flash_message', 'Billing updated!');

        return redirect('admin/access/billing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Billing::destroy($id);

        Session::flash('flash_message', 'Billing deleted!');

        return redirect('admin/access/billing');
    }
}
