<?php

namespace App\Http\Controllers\Backend\Access\Student;

use App\Http\Controllers\Backend\Access\Certificate\CertificateController;
use App\Http\Controllers\Backend\Access\SchoolInformation\SchoolInformationController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Institution\Institution;
use App\Models\Access\Marital\Marital;
use App\Models\Access\Region\Region;
use App\Models\Access\Religion\Religion;
use App\Models\Access\Student\Student;
use App\Models\Access\Ville\Ville;
use ClassPreloader\Config;
use Illuminate\Http\Request;
use Session;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = \Auth::user();
        $status = $request->get('status');


        if (empty($user->type)) {
            $student = Student::whereIn('status', array(1, 2, 3))->paginate(25);
        } else if( strcmp ( $user->type , 'manage-univ' ) == 0){
            if($status == 1){
             $student = Student::where('institutionEmail', $user->email)->whereIn('status', array(2, 3,4,5,6))->paginate(25);
            }else if($status == 2){

                $student = Student::where('institutionEmail', $user->email)->where('status',2)->paginate(25);
            }else if($status == 3){
                $student = Student::where('institutionEmail', $user->email)->where('status',3)->paginate(25);
            }else if($status == 4){
                $student = Student::where('institutionEmail', $user->email)->where('status',4)->paginate(25);
            }else if($status == 5){
                $student = Student::where('institutionEmail', $user->email)->where('status',5)->paginate(25);
            }else if($status == 6){
                $student = Student::where('institutionEmail', $user->email)->where('status',6)->paginate(25);
            }else{
                $student = Student::where('institutionEmail', $user->email)->paginate(25);
            }
        }else {
            $student = Student::where('email', $user->email)->paginate(25);
        }


        return view('backend/access.student.index', compact('student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $institution = Institution::get();
        $student = '';
        $region = Region::get();
        $ville = Ville::get();
        $edit = false;
        $religion = Religion::get();
        $marital = Marital::get();
        return view('backend/access.student.create',['institution' => $institution,'student' => $student,'region' => $region, 'ville' => $ville, 'edit' => $edit,
            'marital' => $marital, 'religion' => $religion]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        $lang = \Illuminate\Support\Facades\Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }

        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        if(!empty($requestData['villeparentId'])){
            $ville = Ville::findOrFail($requestData['villeparentId']);
            $requestData['villeparent'] = $ville->libelle;
        }

        $requestData['userEmail'] = $user->email;
        
        Student::create($requestData);

        $schoolInfoController = new SchoolInformationController();


        return $schoolInfoController->create();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        //$student = Student::findOrFail($id);

        $certificateController = new CertificateController();

        return $certificateController->gotoDetails($id);

        //return view('backend/access.student.show', compact('student'));
    }

    public function reject($id){
        $student = Student::findOrFail($id);

        $requestData['status'] = 3;

        $student->update($requestData);

        return $this->index();
    }

    public function accept($id){
        $student = Student::findOrFail($id);

        $requestData['status'] = 2;

        $student->update($requestData);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $student = Student::findOrFail($id);
        $region = Region::get();
        $ville = Ville::get();
        $edit = true;
        $religion = Religion::get();
        $marital = Marital::get();
        return view('backend/access.student.edit', compact('student'),['region' => $region, 'ville' => $ville, 'edit' => $edit,
            'marital' => $marital, 'religion' => $religion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $student = Student::findOrFail($id);

        $user = \Auth::user();

        $lang = \Illuminate\Support\Facades\Config::get('app.locale');

        if(!empty($requestData['regionId'])){
            $region = Region::findOrFail($requestData['regionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['region'] = $region->libelleEN;
            }else{
                $requestData['region'] = $region->libelleFR;
            }

        }

        if(!empty($requestData['religionId'])){
            $religion = Religion::findOrFail($requestData['religionId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['religion'] = $religion->libelleEN;
            }else{
                $requestData['religion'] = $religion->libelleFR;
            }

        }

        if(!empty($requestData['statusmatrimonialId'])){
            $marital = Marital::findOrFail($requestData['statusmatrimonialId']);
            if (strcmp ( $lang , 'en' ) == 0){
                $requestData['statusmatrimonial'] = $marital->libelleEN;
            }else{
                $requestData['statusmatrimonial'] = $marital->libelleFR;
            }

        }

        if(!empty($requestData['villeId'])){
            $ville = Ville::findOrFail($requestData['villeId']);
            $requestData['ville'] = $ville->libelle;
        }

        if(!empty($requestData['villeparentId'])){
            $ville = Ville::findOrFail($requestData['villeparentId']);
            $requestData['villeparent'] = $ville->libelle;
        }

        $student->update($requestData);

        $schoolInfoController = new SchoolInformationController();

        return $schoolInfoController->create();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Student::destroy($id);

        Session::flash('flash_message', 'Student deleted!');

        return redirect('admin/access/student');
    }

    protected function saveValueInSession($emailStudent)
    {
        // enregistre dans la session
        session(['emailStudent' => $emailStudent]);
    }

    public function getEmailStudentFromSession()
    {
        return session('emailStudent');
    }
}
