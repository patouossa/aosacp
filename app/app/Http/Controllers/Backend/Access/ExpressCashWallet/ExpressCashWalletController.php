<?php

namespace App\Http\Controllers\Backend\Access\ExpressCashWallet;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Billing\Billing;
use App\Models\Access\ExpressCashWallet\ExpressCashWallet;
use App\Models\Access\Institution\Institution;
use Illuminate\Http\Request;
use Session;

class ExpressCashWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::user();
        if(empty($user->type)){
            $expresscashwallet = ExpressCashWallet::where('adminEmail',$user->email)->paginate(25);
        }else{
            $expresscashwallet = ExpressCashWallet::where('institutionEmail',$user->email)->paginate(25);
        }

        return view('backend/access.express-cash-wallet.index', compact('expresscashwallet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/access.express-cash-wallet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        if(empty($user->type)){
            $requestData['adminEmail'] = $user->email;
            $billing = Billing::where('institutionId',$requestData['institutionId'])->first();
            $requestData['amount'] = $billing->montantSysteme;
        }else{
            $institution = Institution::where('userEmail',$user->email)->first();
            $requestData['institutionId'] = $institution->id;
            $requestData['institution'] = $institution->nom;
            $requestData['institutionEmail'] = $institution->userEmail;
            $requestData['amount'] = $institution->amountpreinscriptionLocal;
        }
        
        ExpressCashWallet::create($requestData);

        Session::flash('flash_message', 'ExpressCashWallet added!');

        return redirect('admin/access/express-cash-wallet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $expresscashwallet = ExpressCashWallet::findOrFail($id);

        return view('backend/access.express-cash-wallet.show', compact('expresscashwallet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $expresscashwallet = ExpressCashWallet::findOrFail($id);

        return view('backend/access.express-cash-wallet.edit', compact('expresscashwallet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $expresscashwallet = ExpressCashWallet::findOrFail($id);
        $expresscashwallet->update($requestData);

        Session::flash('flash_message', 'ExpressCashWallet updated!');

        return redirect('admin/access/express-cash-wallet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ExpressCashWallet::destroy($id);

        Session::flash('flash_message', 'ExpressCashWallet deleted!');

        return redirect('admin/access/express-cash-wallet');
    }
}
