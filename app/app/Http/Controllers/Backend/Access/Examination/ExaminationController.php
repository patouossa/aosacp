<?php

namespace App\Http\Controllers\Backend\Access\Examination;

use App\Http\Controllers\Backend\Access\Student\StudentController;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Access\Examination\Examination;
use App\Models\Access\SchoolInformation\SchoolInformation;
use App\Models\Access\Student\Student;
use Illuminate\Http\Request;
use Session;

class ExaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $examination = Examination::paginate(25);

        return view('backend/access.examination.index', compact('examination'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $studentController = new StudentController();

        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;

        $examinations = Examination::where('studentId', $student->id)->paginate(25);
        $schoolInformations = SchoolInformation::where('studentId', $student->id)->paginate(25);

        $examination = '';
        $count = Examination::where('studentId', $student->id)->count();
        return view('backend/access.examination.create', compact('examinations'),['examination'=>$examination,'schoolInformations'=>$schoolInformations,'count'=>$count]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;
        $schoolInformationId = $requestData['schoolInformationId'];

        $schoolInformation = SchoolInformation::where('qualification', $schoolInformationId)->first();

        $requestData['qualification'] = $schoolInformation->qualification;

        if($student->status == 0){
            $studentData['status'] = '1';
            $student->update($studentData);
        }

        for ($i = 0;$i<$requestData['nbSitting'];++$i){
            $requestData1['studentId'] = $student->id;
            $requestData1['qualification'] = $schoolInformation->qualification;
            $requestData1['examinationyear'] = $requestData['examinationyear'][$i];
            $requestData1['candidatenumber'] = $requestData['candidatenumber'][$i];
            $requestData1['examinationcenter'] = $requestData['examinationcenter'][$i];
            $requestData1['resultobtained'] = $requestData['resultobtained'][$i];
            $requestData1['mathematic'] = $requestData['mathematic'][$i];
            $requestData1['english'] = $requestData['english'][$i];
            $requestData1['englishLitt'] = $requestData['englishLitt'][$i];
            $requestData1['french'] = $requestData['french'][$i];
            $requestData1['economic'] = $requestData['economic'][$i];
            $requestData1['geography'] = $requestData['geography'][$i];
            $requestData1['biology'] = $requestData['biology'][$i];
            $requestData1['humanBiology'] = $requestData['humanBiology'][$i];
            $requestData1['chemistry'] = $requestData['chemistry'][$i];
            $requestData1['physic'] = $requestData['physic'][$i];
            $requestData1['futherMaths'] = $requestData['futherMaths'][$i];
            $requestData1['history'] = $requestData['history'][$i];
            $requestData1['computerTech'] = $requestData['computerTech'][$i];
            $requestData1['englishLang'] = $requestData['englishLang'][$i];
            $requestData1['frenchLang'] = $requestData['frenchLang'][$i];
            $requestData1['frenchLitt'] = $requestData['frenchLitt'][$i];
            $requestData1['nbSitting'] = $i;

            Examination::create($requestData1);
        }

        Session::flash('flash_message', 'Examination added!');

        //return redirect('admin/access/examination');
        return $this->create();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $examination = Examination::findOrFail($id);

        return view('backend/access.examination.show', compact('examination'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $examination = Examination::findOrFail($id);
        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;

        $examinations = Examination::where('studentId', $student->id)->paginate(25);
        $schoolInformations = SchoolInformation::where('studentId', $student->id)->paginate(25);

        $count = Examination::where('studentId', $student->id)->count();
        return view('backend/access.examination.edit', compact('examinations'),['examination'=>$examination,'schoolInformations'=>$schoolInformations,'count'=>$count]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $examination = Examination::findOrFail($id);
        $user = \Auth::user();

        $student = Student::where('email', $user->email)->first();
        $requestData['studentId'] = $student->id;

        $schoolInformationId = $requestData['schoolInformationId'];

        $schoolInformation = SchoolInformation::where('id', $schoolInformationId)->first();

        $requestData['qualification'] = $schoolInformation->qualification;

        $examination->update($requestData);

        Session::flash('flash_message', 'Examination updated!');

        //return redirect('admin/access/examination');

        return $this->create();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Examination::destroy($id);

        Session::flash('flash_message', 'Examination deleted!');

        return redirect('admin/access/examination');
    }
}
