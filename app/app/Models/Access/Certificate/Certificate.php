<?php

namespace App\Models\Access\Certificate;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'certificates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['passport', 'handWriting','qualification','birth','transcriptgcea','applicationForm','cni', 'studentId','userEmail','institutionId'];

    
}
