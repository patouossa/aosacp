<?php

namespace App\Models\Access\Niveau;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'niveaus';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['niveau', 'departementId','departement','userEmail'];

    
}
