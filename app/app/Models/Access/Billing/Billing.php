<?php

namespace App\Models\Access\Billing;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'billings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId','institution', 'studentId', 'montantInstitution', 'montantSysteme'];

    
}
