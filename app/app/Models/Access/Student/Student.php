<?php

namespace App\Models\Access\Student;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'students';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'dateNaiss', 'email', 'pobox', 'phone', 'validite', 'country', 'region', 'regionId', 'ville', 'religion', 'statusmatrimonial','statusmatrimonialId', 'sexe', 'countryparent', 'villeparent', 'adresseparent', 'occupationmere', 'occupationpere', 'telephonemere', 'telephonepere', 'emailmere', 'emailpere', 'userEmail','status', 'regionId', 'region', 'villeId','ville',
        'institutionEmail','fatherName','motherName','parentOcuppation',
    'parentTelephone','parentEmail','lieuNaissance'];

}
