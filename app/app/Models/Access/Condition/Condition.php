<?php

namespace App\Models\Access\Condition;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conditions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId', 'institution', 'adminId', 'amountInstitution', 'amountAdmin'
        ,'bankAccountId','mobileTransferId','expresscashId','moneyTransferId',
        'bankAccountAdminId','mobileTransferAdminId','expresscashAdminId','moneyTransferAdminId','status'];

    
}
