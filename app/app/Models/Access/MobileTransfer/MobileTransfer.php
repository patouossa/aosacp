<?php

namespace App\Models\Access\MobileTransfer;

use Illuminate\Database\Eloquent\Model;

class MobileTransfer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mobile_transfers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['operatorName', 'mobileAccountName', 'mobile', 'institutionId', 'institution','adminEmail','institutionEmail'];

    
}
