<?php

namespace App\Models\Access\DepartementIns;

use Illuminate\Database\Eloquent\Model;

class DepartementIn extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departement_ins';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['libelle', 'faculteId', 'faculte','userId','userEmail','userEmail','studentId'];

    
}
