<?php

namespace App\Models\Access\SchoolInformation;

use Illuminate\Database\Eloquent\Model;

class SchoolInformation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'school_informations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['startyear', 'endyear', 'nameschool', 'qualification','userEmail','studentId','institutionId'];

    
}
