<?php

namespace App\Models\Access\PaymentTransfer;

use Illuminate\Database\Eloquent\Model;

class PaymentTransfer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_transfers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['institutionId', 'institution','institutionEmail',  'adminId', 'adminEmail', 'dateDepot', 'studentId', 'student', 'studentPhone', 'amount', 'reference'];

    
}
