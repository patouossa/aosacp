<?php

namespace App\Models\Access\BillingConfig;

use Illuminate\Database\Eloquent\Model;

class BillingConfig extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'billing_configs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'createBy'];

    
}
