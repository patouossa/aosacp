<?php

namespace App\Models\Access\Filiere;

use Illuminate\Database\Eloquent\Model;

class Filiere extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'filieres';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['libelle', 'code', 'departementId', 'departemnent','userId','userEmail','niveauId','niveau','nbPlace','duration'];

    
}
