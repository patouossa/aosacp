<?php

/**
 * Created by PhpStorm.
 * User: samuel
 * Date: 21/11/17
 * Time: 16:05
 */
namespace App\Events\Backend\Access\Institution;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class CompagnieCreated.
 */
class InstitutionCreated extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $institution;

    /**
     * @param $compagnie
     */
    public function __construct($institution)
    {
        $this->institution = $institution;
    }
}
