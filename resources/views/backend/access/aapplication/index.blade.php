@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.niveau.management') . ' | ' . trans('labels.backend.access.niveau.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.niveau.management') }}
    <small>{{ trans('labels.backend.access.niveau.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Acknowledged Applications (Status = I-Acknowledged)</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/aapplication/create') }}" class="btn btn-primary " title="Add New Niveau"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">

        <div class="form-group">

            <table class="table table-borderless">

                <tr bgcolor="#a9a9a9" >
                    <th>
                        <label  >  Start Date:</label><input class="form-control" type="date" name="" />

                    </th>
                    <th>

                        <label  > End Date:</label><input class="form-control" type="date" name="" />

                    </th>

                    <th>
                        <label >  Sort By:</label>
                        <select class="form-control"  required>
                            <option value="" selected disabled>  </option>

                        </select>
                    </th>

                    </th>
                </tr>

        </div>

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>

                    <tr>
                        <th>Application ID</th>
                        <th>Applicant</th>
                        <th>Payment Date </th>
                        <th>Payment Source </th>
                        <th>Payment Destination </th>
                        <th>Payment Method </th>
                        <th>Payment Amount</th>
                        <th>Payment Upload</th>
                        <th>Actions</th>
                    </tr>

                </thead>
                <tbody>
                @foreach($niveau as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->niveau }}</td><td>{{ $item->departement }}</td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td>
                            <a href="{{ url('/admin/access/aapplication/' . $item->id) }}" class="btn btn-success btn-xs" title="View Niveau"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/aapplication/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Niveau"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/aapplication', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Niveau" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Niveau',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $niveau->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection