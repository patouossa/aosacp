@extends ('backend.layouts.app')


@section ('title', 'My Application Fee Summary')


@section('page-header')
    <h1>
        My Application Fee Summary
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">My Application Fee Summary</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th> Institution</th>
                        <th> Application Fees (Status)</th>
                        <th> Web Admin Fees (Status)</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($condition as $item)
                        <tr>
                            <td>{{ $item->institution }}

                            </td>

                            <td>{{ $item->amountInstitution }}

                                @if($item->statusApp == 0)
                                    (Pending)
                                @elseif($item->status == 1)
                                    (Deposited)
                                @else
                                     (Paid)
                                @endif


                            </td>


                            <td>{{ $item->amountAdmin }}

                                @if($item->statusApp == 0)
                                    (Pending)
                                @elseif($item->status == 1)
                                    (Deposited)
                                @else
                                     (Paid)
                                @endif
                            </td>
                            <td>

                                <a href="{{ url('/admin/access/condition/' . $item->id) }}" class="btn btn-success btn-xs" title="Payment Options">
                                    <span class="fa fa-money" aria-hidden="true"/></a>

                                <a href="#" class="btn btn-success btn-xs" title="Bank Account Payment">
                                    <span class="fa fa-bank" aria-hidden="true"/></a>

                                <a href="#" class="btn btn-success btn-xs" title="Express Cash Wallet">
                                    <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"/></a>

                                <a href="#" class="btn btn-success btn-xs" title="Mobile Payment">
                                    <span class="fa fa-mobile" aria-hidden="true"/></a>

                                <a href="#" class="btn btn-success btn-xs" title="Money Transfer Payment">
                                    <span class="glyphicon glyphicon-transfer" aria-hidden="true"/></a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $condition->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection