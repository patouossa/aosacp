@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.condition.management') . ' | ' . trans('labels.backend.access.condition.show'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.condition.management') }}
        <small>{{ trans('labels.backend.access.condition.show') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">

        <div class="box-body">
            @if($condition->statusApp ==0)
                <div id="copy-area" class="copy-area">
                    <div class="register-form-extra">
                        <label class="accordion">Institution Application Fees</label>
                        <div class="panel1">
                            @if($bankaccount)
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Bank Account</caption>
                                        <thead>
                                        <tr>
                                            <th> Bank Name</th>
                                            <th> Account Name</th>
                                            <th> Bank Code</th>
                                            <th> Account Number</th>
                                            <th> RIP</th>
                                            <th> Reference</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bankaccount as $item)
                                            <tr>
                                                <td>{{ $item->bankName }}</td>
                                                <td>{{ $item->accountName }}</td>
                                                <td>{{ $item->bankCode }}</td>
                                                <td>{{ $item->accountNumber }}</td>
                                                <td>{{ $item->rip }}</td>
                                                <td>{{ $item->reference }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#exampleModalBank">Pay</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            @if($expresscash)
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Express Cash Account</caption>
                                        <thead>
                                        <tr>
                                            <th> Institution Name</th>
                                            <th> Account Name</th>
                                            <th> Account Number</th>
                                            <th> Reference</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($expresscash as $item)
                                            <tr>
                                                <td>{{ $item->institutionName }}</td>
                                                <td>{{ $item->accountName }}</td>
                                                <td>{{ $item->accountNumber }}</td>
                                                <td>{{ $item->reference }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                            @if($moneytransfer)

                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Money Transfer</caption>
                                        <thead>
                                        <tr>
                                            <th> Compnay Name</th>
                                            <th> Beneficary Name</th>
                                            <th> Mobile</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($moneytransfer as $item)
                                            <tr>
                                                <td>{{ $item->compnayName }}</td>
                                                <td>{{ $item->beneficaryName }}</td>
                                                <td>{{ $item->mobile }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#exampleModalTransfer">Pay</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                            @if($mobiletransfer)

                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Mobile Tranfer</caption>
                                        <thead>
                                        <tr>
                                            <th> Operator Name</th>
                                            <th> Mobile Account Name</th>
                                            <th> Mobile</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($mobiletransfer as $item)
                                            <tr>
                                                <td>{{ $item->operatorName }}</td>
                                                <td>{{ $item->mobileAccountName }}</td>
                                                <td>{{ $item->mobile }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            @endif
            @if($condition->statusAdmin ==0)

                <div id="copy-area" class="copy-area">
                    <div class="register-form-extra">
                        <label class="accordion">Admin Application Fees</label>
                        <div class="panel1">
                            @if($bankaccountAdmin)

                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Bank Account</caption>

                                        <thead>
                                        <tr>
                                            <th> Bank Name</th>
                                            <th> Account Name</th>
                                            <th> Bank Code</th>
                                            <th> Account Number</th>
                                            <th> RIP</th>
                                            <th> Reference</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bankaccountAdmin as $item)
                                            <tr>

                                                <td>{{ $item->bankName }}</td>
                                                <td>{{ $item->accountName }}</td>
                                                <td>{{ $item->bankCode }}</td>
                                                <td>{{ $item->accountNumber }}</td>
                                                <td>{{ $item->rip }}</td>
                                                <td>{{ $item->reference }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#exampleModalBankAdmin">Pay</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif


                            @if($expresscashAdmin)
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Express Cash Account</caption>
                                        <thead>
                                        <tr>
                                            <th> Institution Name</th>
                                            <th> Account Name</th>
                                            <th> Account Number</th>
                                            <th> Reference</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($expresscashAdmin as $item)
                                            <tr>
                                                <td>{{ $item->institutionName }}</td>
                                                <td>{{ $item->accountName }}</td>
                                                <td>{{ $item->accountNumber }}</td>
                                                <td>{{ $item->reference }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                            @if($moneytransferAdmin)

                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Money Transfer</caption>
                                        <thead>
                                        <tr>
                                            <th> Compnay Name</th>
                                            <th> Beneficary Name</th>
                                            <th> Mobile</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($moneytransferAdmin as $item)
                                            <tr>
                                                <td>{{ $item->compnayName }}</td>
                                                <td>{{ $item->beneficaryName }}</td>
                                                <td>{{ $item->mobile }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#exampleModalTransferAdmin">Pay</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif

                            @if($mobiletransferAdmin)

                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <caption>Mobile Tranfer</caption>
                                        <thead>
                                        <tr>
                                            <th> Operator Name</th>
                                            <th> Mobile Account Name</th>
                                            <th> Mobile</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($mobiletransferAdmin as $item)
                                            <tr>
                                                <td>{{ $item->operatorName }}</td>
                                                <td>{{ $item->mobileAccountName }}</td>
                                                <td>{{ $item->mobile }}</td>
                                                <td>
                                                    <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#exampleModalTransferAdmin">Pay</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div> <!-- /.box-body -->

 <!-- For BankAccount -->
        <div class="modal fade" id="exampleModalBank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Bank Account Payment</h4>
                    </div>
                    {!! Form::open(['url' => '/admin/access/payment-bank', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="modal-body">

                        <div class="form-group {{ $errors->has('slipNumber') ? 'has-error' : ''}}">
                            {!! Form::label('slipNumber', 'Slip Number', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('slipNumber', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('slipNumber', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
                            {!! Form::label('dateDepot', 'Date', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
                            {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('reference', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('transactionNumber') ? 'has-error' : ''}}">
                            {!! Form::label('transactionNumber', 'Transaction Number', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('transactionNumber', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('transactionNumber', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('scanDepositSlip') ? 'has-error' : ''}}">
                            {!! Form::label('scanDepositSlip', 'Scan Deposit Slip', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::file('scanDepositSlip', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('scanDepositSlip', '<p class="help-block">:message</p>') !!}
                            </div>


                           </div>

                        @foreach($bankaccount as $item)

                                <input type="hidden" name="institutionId" value="{{$item->institutionId}}">
                                <input type="hidden" name="adminEmail" value="{{$item->adminEmail}}">
                                <input type="hidden" name="conditionId" value="{{$condition->id}}">
                                <input type="hidden" name="amount" value="{{$item->amount}}">

                        @endforeach


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary pull-right']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>



        <!-- For Money Transfer -->
        <div class="modal fade" id="exampleModalTranfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Money Tranfer Payment</h4>
                    </div>
                    {!! Form::open(['url' => '/admin/access/payment-tranfer', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="modal-body">

                        <div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
                            {!! Form::label('dateDepot', 'Datedepot', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
                            {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('reference', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        @foreach($mobiletransfer as $item)

                            <input type="hidden" name="institutionId" value="{{$item->institutionId}}">
                            <input type="hidden" name="adminEmail" value="{{$item->adminEmail}}">
                            <input type="hidden" name="conditionId" value="{{$condition->id}}">
                            <input type="hidden" name="amount" value="{{$item->amount}}">

                        @endforeach


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary pull-right']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>



        <!-- For Bank Account Admin-->
        <div class="modal fade" id="exampleModalBankAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                    </div>
                    {!! Form::open(['url' => '/admin/access/payment-bank', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="modal-body">

                        <div class="form-group {{ $errors->has('slipNumber') ? 'has-error' : ''}}">
                            {!! Form::label('slipNumber', 'Slip Number', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('slipNumber', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('slipNumber', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
                            {!! Form::label('dateDepot', 'Date', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
                            {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('reference', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('transactionNumber') ? 'has-error' : ''}}">
                            {!! Form::label('transactionNumber', 'Transaction Number', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('transactionNumber', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('transactionNumber', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('scanDepositSlip') ? 'has-error' : ''}}">
                            {!! Form::label('scanDepositSlip', 'Scan Deposit Slip', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::file('scanDepositSlip', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('scanDepositSlip', '<p class="help-block">:message</p>') !!}
                            </div>


                        </div>

                        @foreach($bankaccountAdmin as $item)

                            <input type="hidden" name="institutionId" value="{{$item->institutionId}}">
                            <input type="hidden" name="adminEmail" value="{{$item->adminEmail}}">
                            <input type="hidden" name="conditionId" value="{{$condition->id}}">
                            <input type="hidden" name="amount" value="{{$item->amount}}">

                        @endforeach


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary pull-right']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>



        <!-- For Money Transfer Admin -->
        <div class="modal fade" id="exampleModalTransferAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                    </div>
                    {!! Form::open(['url' => '/admin/access/payment-transfer', 'class' => 'form-horizontal', 'files' => true]) !!}

                    <div class="modal-body">

                        <div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
                            {!! Form::label('dateDepot', 'Datedepot', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
                            {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('reference', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        @foreach($moneytransferAdmin as $item)

                            <input type="hidden" name="institutionId" value="{{$item->institutionId}}">
                            <input type="hidden" name="adminEmail" value="{{$item->adminEmail}}">
                            <input type="hidden" name="conditionId" value="{{$condition->id}}">
                            <input type="hidden" name="amount" value="{{$item->amount}}">

                        @endforeach


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary pull-right']) }}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script>

        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

    </script>

@endsection
