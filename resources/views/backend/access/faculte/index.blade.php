@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.faculte.management') . ' | ' . trans('labels.backend.access.faculte.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.faculte.management') }}
    <small>{{ trans('labels.backend.access.faculte.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Our Faculties & Schools</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/faculte/create') }}" class="btn btn-primary " title="Add New Faculte"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->


    <div class="box-body">
        <div class="form-group">

            <table class="table table-borderless">
                <tr >
                    <th>Faculty ID:</th> <th>System Generated:</th> <th>Custom Generated:</th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                <th>

                    <div class="form-group">
                        <label  >Faculty Name:</label><input class="form-control" type="text" name="" />
                    </div>

                </th>
                    <th>

                        <div class="form-group">
                            <label  >Faculty Name:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                <th>

                    <div class="form-group">
                        <label  >Dean of Faculty:</label><input class="form-control" type="text" name="" />
                    </div>
                </th>
                <th>
                        Faculty Region:<br/>
                        <select class="form-control"  required>
                            <option value="" selected disabled>  </option>

                        </select>
                </th>

                </th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                    <th>
                            Faculty Location:<br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>    </option>

                            </select>
                    </th>
                    <th>
                        Faculty Town:<br/>
                        <select class="form-control"  required>
                            <option value="" selected disabled>    </option>

                        </select>
                    </th>
                    <th>

                        <div class="form-group">
                            <label  >Faculty Phone:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>

                    <th>

                        <div class="form-group">
                            <label  >Faculty Email:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                </tr>
                <tr>
                    <th> </th>
                    <th> </th>
                    <th>Status: </th>
                    <th>Active / Inactive </th>
                </tr>
        </div>

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Faculty ID</th>
                        <th>Faculty Name</th>
                        <th> Dean </th>
                        <th> Phone # </th>
                        <th>Town</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($faculte as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td></td><td></td>
                        <td></td>

                        <td>  </td>
                        <td>   </td>
                        <td> <a href="{{ url('/admin/access/faculte/' . $item->id) }}" class="btn btn-success btn-xs" title="View Faculte"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/faculte/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Faculte"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/faculte', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Faculte" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Faculte',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!} </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $faculte->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection