@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.departement-ins.management') . ' | ' . trans('labels.backend.access.departement-ins.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.departement-ins.management') }}
    <small>{{ trans('labels.backend.access.departement-ins.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Our Departments</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/departement-ins/create') }}" class="btn btn-primary " title="Add New DepartementIn"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">

            <table class="table table-borderless">
                <tr >
                    <th>Department ID:</th> <th>System Generated:</th> <th>Custom Generated:</th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                    <th>

                        <div class="form-group">
                            <label  >  Department Name:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                    <th>

                        <div class="form-group">
                            <label  > Department Address:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                    <th>


                        <div class="form-group">
                            <label  >Head of Department:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                    <th>
                        Department Region:<br/>
                        <select class="form-control"  required>
                            <option value="" selected disabled>  </option>

                        </select>
                    </th>

                    </th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                    <th>
                        Department Location:<br/>
                        <select class="form-control"  required>
                            <option value="" selected disabled>    </option>

                        </select>
                    </th>
                    <th>
                        Faculty Town:<br/>
                        <select class="form-control"  required>
                            <option value="" selected disabled>    </option>

                        </select>
                    </th>
                    <th>

                        <div class="form-group">
                            <label  > Department Phone:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>

                    <th>

                        <div class="form-group">
                            <label  >  Department Email:</label><input class="form-control" type="text" name="" />
                        </div>
                    </th>
                </tr>
                <tr>
                    <th> </th>
                    <th> </th>
                    <th>Status: </th>
                    <th>Active / Inactive </th>
                </tr>
        </div>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Dept.ID</th>
                        <th> Dept.Name </th>
                        <th> HOD</th>
                        <th> Phone #</th>
                        <th> Town</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($departementins as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelle }}</td><td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="{{ url('/admin/access/departement-ins/' . $item->id) }}" class="btn btn-success btn-xs" title="View DepartementIn"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/departement-ins/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit DepartementIn"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/departement-ins', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete DepartementIn" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete DepartementIn',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $departementins->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection