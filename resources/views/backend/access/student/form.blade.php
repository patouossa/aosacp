<div class="col-md-7">

    <div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
        {!! Form::label('nom', trans('labels.frontend.nom'), ['class' => 'col-md-4 control-label','placeholder' => trans('labels.frontend.name_with_bird')]) !!}
        <div class="col-md-6">
            {!! Form::text('nom', null, ['class' => 'form-control']) !!}
            {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('dateNaiss') ? 'has-error' : ''}}">
        {!! Form::label('dateNaiss',trans('labels.frontend.date_naiss'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::date('dateNaiss', null, ['class' => 'form-control']) !!}
            {!! $errors->first('dateNaiss', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email',trans('labels.frontend.email'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group {{ $errors->has('pobox') ? 'has-error' : ''}}">
        {!! Form::label('pobox',trans('labels.frontend.bp'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('pobox', null, ['class' => 'form-control']) !!}
            {!! $errors->first('pobox', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('validite') ? 'has-error' : ''}}">
        {!! Form::label('validite',trans('labels.frontend.disability'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control validite" id="validite" name="validite" required>
                <option value="" selected disabled>{{trans('labels.frontend.select_disability')}}</option>

                <option value="">{{trans('labels.frontend.none')}}</option>
                <option value="Deaf" {{ ($student != '' && $student->validite==  'Deaf') ? 'selected="selected"' : '' }} >
                    Deaf
                </option>
                <option value="Dumb" {{ ($student != '' && $student->validite==  'Dumb') ? 'selected="selected"' : '' }} >
                    Dumb
                </option>
                <option value="Blind" {{ ($student != '' && $student->validite==  'Blind') ? 'selected="selected"' : '' }} >
                    Blind
                </option>
                <option value="Deaf & Dumb" {{ ($student != '' && $student->validite==  'Deaf & Dumb') ? 'selected="selected"' : '' }} >
                    Deaf & Dumb
                </option>
                <option value="Deaf & Blind" {{ ($student != '' && $student->validite==  'Deaf & Blind') ? 'selected="selected"' : '' }} >
                    Deaf & Blind
                </option>
                <option value="Dumb & Blind" {{ ($student != '' && $student->validite==  'Dumb & Blind') ? 'selected="selected"' : '' }} >
                    Dumb & Blind
                </option>
                <option value="Cannot climb Stairs" {{ ($student != '' && $student->validite==  'Cannot climb Stairs') ? 'selected="selected"' : '' }} >
                    Cannot climb Stairs
                </option>

            </select>
        </div>
    </div>

    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('ville', trans('labels.frontend.country_birth'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control country" id="country" name="country" required>
                <option value="" selected disabled>{{trans('labels.frontend.select_country')}}</option>

                @if ( Config::get('app.locale') == 'en')
                    <option value="Cameroon" {{ ($student != '' && $student->country==  'Cameroon') ? 'selected="selected"' : '' }} >
                        Cameroon
                    </option>
                @else
                    <option value="Cameroun" {{ ($student != '' && $student->country==  'Cameroun') ? 'selected="selected"' : '' }} >
                        Cameroun
                    </option>
                @endif

            </select>
            {!! $errors->first('ville', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('ville', trans('labels.frontend.nationality'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control country" id="country" name="nationality" required>
                <option value="" selected disabled>{{trans('labels.frontend.select_nationality')}}</option>

                @if ( Config::get('app.locale') == 'en')
                    <option value="Cameroonian" {{ ($student != '' && $student->nationality==  'Cameroonian') ? 'selected="selected"' : '' }} >
                        Cameroonian
                    </option>
                @else
                    <option value="Camerounais" {{ ($student != '' && $student->nationality==  'Camerounais') ? 'selected="selected"' : '' }} >
                        Camerounais
                    </option>
                @endif

            </select>
            {!! $errors->first('ville', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('region') ? 'has-error' : ''}}">
        {!! Form::label('region', trans('labels.frontend.region'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control region" name="regionId" required>
                <option value="" selected disabled>{{ trans('labels.frontend.select_region') }}</option>
                @foreach($region as $item)

                    @if ( Config::get('app.locale') == 'en')
                        <option value="{{$item->id}}" {{ ($student != '' && $student->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleEN}}</option>
                    @else
                        <option value="{{$item->id}}" {{ ($student != '' && $student->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleFR}}</option>
                    @endif

                @endforeach
            </select>
            {!! $errors->first('region', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
        {!! Form::label('phone',trans('labels.frontend.phone'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('ville', trans('labels.frontend.ville'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control ville" id="ville" name="villeId" required>
                <option value="" selected disabled>{{ trans('labels.frontend.select_ville') }}</option>
                @foreach($ville as $item)
                    <option value="{{$item->id}}" {{ ($student != '' && $student->villeId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
                @endforeach
            </select>
            {!! $errors->first('ville', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('religion') ? 'has-error' : ''}}">
        {!! Form::label('religion', trans('labels.frontend.religion'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control religion" id="religion" name="religionId">
                <option value="" selected disabled>{{ trans('labels.frontend.select_religion') }}</option>
                @foreach($religion as $item)
                    @if ( Config::get('app.locale') == 'en')
                        <option value="{{$item->id}}" {{ ($student != '' && $student->religionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleEN}}</option>
                    @else
                        <option value="{{$item->id}}" {{ ($student != '' && $student->religionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleFR}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group {{ $errors->has('statusmatrimonial') ? 'has-error' : ''}}">
        {!! Form::label('statusmatrimonial',trans('labels.frontend.marital_status'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">

            <select class="form-control statusmatrimonial" id="statusmatrimonial" name="statusmatrimonialId">
                <option value="" selected disabled>{{ trans('labels.frontend.select_marital_status') }}</option>
                @foreach($marital as $item)
                    @if ( Config::get('app.locale') == 'en')
                        <option value="{{$item->id}}" {{ ($student != '' && $student->statusmatrimonialId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleEN}}</option>
                    @else
                        <option value="{{$item->id}}" {{ ($student != '' && $student->statusmatrimonialId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleFR}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group {{ $errors->has('language') ? 'has-error' : ''}}">
        {!! Form::label('language','Prefered Language of institution', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">

            <select class="form-control language" id="language" name="language">
                <option value="" selected disabled>{{ trans('labels.frontend.select_marital_status') }}</option>
                @if ( Config::get('app.locale') == 'fr')
                    <option value="Anglais" {{ ($student != '' && $student->language==  'Anglais') ? 'selected="selected"' : '' }} >
                        Anglais
                    </option>
                    <option value="Français" {{ ($student != '' && $student->language==  'Français') ? 'selected="selected"' : '' }} >
                        Français
                    </option>
                @else
                    <option value="English" {{ ($student != '' && $student->language==  'English') ? 'selected="selected"' : '' }} >
                        English
                    </option>
                    <option value="French" {{ ($student != '' && $student->language==  'French') ? 'selected="selected"' : '' }} >
                        French
                    </option>
                @endif

            </select>

        </div>
    </div>

    <div class="form-group {{ $errors->has('sexe') ? 'has-error' : ''}}">
        {!! Form::label('sexe',trans('labels.frontend.sexe'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <div class="checkbox col-md-3">
                <label>{!! Form::radio('sexe', trans('labels.frontend.male')) !!} {{trans('labels.frontend.male')}}</label>
            </div>
            <div class="checkbox  col-md-3">
                <label>{!! Form::radio('sexe', trans('labels.frontend.female'), true) !!} {{trans('labels.frontend.female')}}</label>
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
        {!! Form::label('photo', 'Upload Applicant Photo', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::file('photo', null, ['class' => 'form-control']) !!}
            {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>


<div class="col-md-5">
    <div class="form-group {{ $errors->has('fatherName') ? 'has-error' : ''}}">
        {!! Form::label('fatherName',trans('labels.frontend.fathername') , ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('fatherName', null, ['class' => 'form-control']) !!}
            {!! $errors->first('fatherName', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('motherName') ? 'has-error' : ''}}">
        {!! Form::label('motherName',trans('labels.frontend.mothername') , ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('motherName', null, ['class' => 'form-control']) !!}
            {!! $errors->first('motherName', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('countryparent') ? 'has-error' : ''}}">
        {!! Form::label('countryparent',trans('labels.frontend.country_parent'), ['class' => 'col-md-4 control-label']) !!}

        <div class="col-md-6">
            <select class="form-control countryparent" id="countryparent" name="countryparent" required>
                <option value="" selected disabled>{{trans('labels.frontend.select_country') }}</option>

                @if ( Config::get('app.locale') == 'en')
                    <option value="Cameroon" {{ ($student != '' && $student->countryparent==  'Cameroon') ? 'selected="selected"' : '' }} >
                        Cameroon
                    </option>
                @else
                    <option value="Cameroun" {{ ($student != '' && $student->countryparent==  'Cameroun') ? 'selected="selected"' : '' }} >
                        Cameroun
                    </option>
                @endif

            </select>
        </div>
    </div>

    <div class="form-group {{ $errors->has('villeparent') ? 'has-error' : ''}}">
        {!! Form::label('villeparent',trans('labels.frontend.ville_parent'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control villeparentId" id="villeparentId" name="villeparentId" required>
                <option value="" selected disabled>{{ trans('labels.frontend.select_ville') }}</option>
                @foreach($ville as $item)
                    <option value="{{$item->id}}" {{ ($student != '' && $student->villeId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
                @endforeach
            </select>
        </div>
    </div>


    <div class="form-group {{ $errors->has('adresseparent') ? 'has-error' : ''}}">
        {!! Form::label('adresseparent', trans('labels.frontend.adresseparent'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('adresseparent', null, ['class' => 'form-control']) !!}
            {!! $errors->first('adresseparent', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('parentOcuppation') ? 'has-error' : ''}}">
        {!! Form::label('parentOcuppation', trans('labels.frontend.parent_occupation'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('parentOcuppation', null, ['class' => 'form-control']) !!}
            {!! $errors->first('parentOcuppation', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('parentTelephone') ? 'has-error' : ''}}">
        {!! Form::label('parentTelephone',trans('labels.frontend.parent_phone'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('parentTelephone', null, ['class' => 'form-control']) !!}
            {!! $errors->first('parentTelephone', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('parentEmail') ? 'has-error' : ''}}">
        {!! Form::label('parentEmail', trans('labels.frontend.parent_email'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('parentEmail', null, ['class' => 'form-control']) !!}
            {!! $errors->first('parentEmail', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

    jQuery(document).ready(function ($) {
        $('.region').change(function () {
            $.get("{{ url('admin/access/region/ville/all')}}",
                {region: $(this).val()},
                function (data) {
                    var model = $('#ville');
                    model.empty();
                    $.each(data, function (index, element) {

                        model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                    });
                });
        });
    });

</script>