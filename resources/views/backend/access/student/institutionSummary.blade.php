@extends ('backend.layouts.app')


@section ('title', 'Institution Summary')


@section('page-header')
    <h1>
        Institution Summary
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">My Institution Summary</h3>
            <div class="box-tools pull-right">

                <a href="{{ url('/admin/access/programstudie/create') }}"
                   class="btn btn-primary" title="Add Institution"><span
                            class="glyphicon glyphicon-book" aria-hidden="true"/>Add Institution</a>


                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Institution </th><th> Number of Uploads </th><th> Number of Choices </th><th> Start date </th><th> Last modification </th><th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($programstudie as $item)
                        <tr>
                            <td>{{ $item->institution }}</td>
                            <td>{{ $item->nbRequirement }}</td>
                            <td>{{ $item->nbChoise }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->updated_at }}</td>
                            <td>
                                <a href="{{ url('/admin/access/institution-detail/' . $item->institutionId) }}" class="btn btn-success btn-xs" title="View Programstudie"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                <a href="{{ url('/admin/access/programstudie/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Programstudie"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection