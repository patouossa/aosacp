@extends ('backend.layouts.app')


@section ('title', 'Application Summary')


@section('page-header')
    <h1>
        Application Summary
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">My Application Summary</h3>
            <div class="box-tools pull-right">

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Information Type</th><th> Start date </th><th> Last modification </th><th> Status </th><th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Demographic Information</td>
                            <td>{{ $student1->created_at }}</td><td>{{ $student1->updated_at }}</td><td>Done</td>
                            <td>
                                <a href="{{ url('/admin/access/student/') }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            </td>
                        </tr>

                        <tr>
                            <td>Education Information</td>
                            <td>{{ $educationFirst->created_at }}</td><td>{{ $educationLast->updated_at }}</td><td>@if($countEducation != 0) In Progress @endif</td>
                            <td>
                                <a href="{{ url('/admin/access/school-information/create') }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            </td>
                        </tr>

                        <tr>
                            <td>Exams|Results</td>
                            <td>{{ $examinationFirst->created_at }}</td><td>{{ $examinationEnd->updated_at }}</td><td>@if($countExamination != 0) In Progress @endif</td>
                            <td>
                                <a href="{{ url('/admin/access/examination/create') }}" class="btn btn-success btn-xs" title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection