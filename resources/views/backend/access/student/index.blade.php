@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.student.management') . ' | ' . trans('labels.backend.access.student.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.student.management') }}
        <small>{{ trans('labels.backend.access.student.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Student</h3>
            <div class="box-tools pull-right">
                {{--@if(access()->user()->type == 'manage-student')
                <a href="{{ url('/admin/access/student/create') }}" class="btn btn-primary " title="Add New Student"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.to_preinscription')}}</a>
                @endif--}}
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th> Nom</th>
                        <th> Date Naissance</th>
                        <th> Email</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($student as $item)
                        <tr>

                            <td>{{ $item->nom }}</td>
                            <td>{{ $item->dateNaiss }}</td>
                            <td>{{ $item->email }}</td>
                            {{--<td><a href="{{ url('/admin/access/school-information/create/' . $item->id) }}" class="btn btn-success btn-xs" title="School Information">Step2</a></td>
                            <td><a href="{{ url('/admin/access/examinantion/create/' . $item->id) }}" class="btn btn-success btn-xs" title="Examinantion">Step3</a></td>
                            <td><a href="{{ url('/admin/access/programstudie/create/' . $item->id) }}" class="btn btn-success btn-xs" title="Program Study">Step4</a></td>
                            <td><a href="{{ url('/admin/access/certificate/create/' . $item->id) }}" class="btn btn-success btn-xs" title="Certificate">Step5</a></td>--}}
                            <td>
                                @if($item->status == 1 || $item->status == 0)
                                    <span class="label label-warning">In progres to Send</span>
                                @endif

                                    @if($item->status == 2)
                                        <span class="label label-warning">In progres</span>
                                    @endif

                                    @if($item->status == 3)
                                        <span class="label label-warning">Paid</span>
                                    @endif

                            </td>

                            <td>

                                <a href="{{ url('/admin/access/student/' . $item->id) }}" class="btn btn-success btn-xs"
                                   title="View Details Student"><span class="glyphicon glyphicon-eye-open"
                                                                      aria-hidden="true"/></a>

                                @if(access()->user()->type == 'manage-student')

                                        <a href="{{ url('/admin/access/student/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Demographic informations"><span
                                                    class="glyphicon glyphicon-info-sign" aria-hidden="true"/></a>

                                            <a href="{{ url('/admin/access/programstudie/create') }}"
                                               class="btn btn-primary btn-xs" title="Select institution"><span
                                                        class="glyphicon glyphicon-book" aria-hidden="true"/></a>

                                        <a href="{{ url('/admin/access/condition') }}"
                                           class="btn btn-primary btn-xs" title="Select Payment options"><span
                                                    class="glyphicon glyphicon-usd" aria-hidden="true"/></a>

                                @endif

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $student->render() !!} </div>
            </div>


        </div> <!-- /.box-body -->


    </div>
    @if(access()->user()->type == 'manage-student')
    <div class="row">
        <div class="col-md-6"><a href="#"><img width="500" height="150" src="/img/phase1.png" alt=""></a></div>
        <div class="col-md-6"><a href="#"><img width="500" height="150" src="/img/phase2.png" alt=""></a></div>
    </div>

    <div class="row" style="margin-top:10px; ">
        <div class="col-md-6"><a href="#"><img width="500" height="150" src="/img/phase3.png" alt=""></a></div>
        <div class="col-md-6"><a href="#"><img width="500" height="150" src="/img/phase4.png" alt=""></a></div>
    </div>
    @endif
@endsection