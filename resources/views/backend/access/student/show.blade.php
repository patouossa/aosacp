@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.student.management') . ' | ' . trans('labels.backend.access.student.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.student.management') }}
    <small>{{ trans('labels.backend.access.student.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.student.show') }} - {{ $student->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/student/' . $student->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Student"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/student', $student->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Student',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $student->id }}</td>
                    </tr>
                    <tr><th> Nom </th><td> {{ $student->nom }} </td></tr><tr><th> DateNaiss </th><td> {{ $student->dateNaiss }} </td></tr><tr><th> Email </th><td> {{ $student->email }} </td></tr><tr><th> Pobox </th><td> {{ $student->pobox }} </td></tr><tr><th> Phone </th><td> {{ $student->phone }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.student.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
