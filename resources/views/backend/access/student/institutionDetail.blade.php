@extends ('backend.layouts.app')


@section ('title', 'Institution Summary')


@section('page-header')
    <h1>
        View Institution Details

        <a href="{{ url('/admin/access/institution-summary/') }}" class="btn btn-primary pull-right" title="Back to My  Application Summary">Close</a>

    </h1>

@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"> Institution Name: <strong>{{$studyprogram->institution}}</strong></h3>
            <h3 class="box-title pull-right"> Number of Study Program: <strong>{{$nbStudy}}</strong></h3>
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Faculty </th><th> Departement </th><th> Study program </th><th> Available Place </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($studyprograms as $item)
                        <tr>
                            <td>{{ $item->faculte }}</td>
                            <td>{{ $item->departement }}</td>
                            <td>{{ $item->filiere }}</td>
                            <td>{{ $item->nbPlace }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection