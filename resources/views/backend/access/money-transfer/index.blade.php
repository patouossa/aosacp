@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.money-transfer.management') . ' | ' . trans('labels.backend.access.money-transfer.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.money-transfer.management') }}
        <small>{{ trans('labels.backend.access.money-transfer.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">money-transfer</h3>
            <div class="box-tools pull-right">

                <a href="{{ url('/admin/access/money-transfer/create') }}" class="btn btn-primary "
                   title="Add New MoneyTransfer"><span class="glyphicon glyphicon-plus"
                                                       aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Compnay Name</th>
                        <th> Beneficary Name</th>
                        <th> Mobile</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($moneytransfer as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->compnayName }}</td>
                            <td>{{ $item->beneficaryName }}</td>
                            <td>{{ $item->mobile }}</td>

                            @if( empty(access()->user()->type))
                                @if($item->adminEmail != null)
                                    <td>
                                        <a href="{{ url('/admin/access/money-transfer/' . $item->id) }}"
                                           class="btn btn-success btn-xs" title="View MoneyTransfer"><span
                                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        <a href="{{ url('/admin/access/money-transfer/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit MoneyTransfer"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/access/money-transfer', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete MoneyTransfer" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete MoneyTransfer',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                @endif

                            @else

                                <td>
                                    <a href="{{ url('/admin/access/money-transfer/' . $item->id) }}"
                                       class="btn btn-success btn-xs" title="View MoneyTransfer"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('/admin/access/money-transfer/' . $item->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit MoneyTransfer"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/access/money-transfer', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete MoneyTransfer" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete MoneyTransfer',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $moneytransfer->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection