@permissions(['manage-users', 'manage-roles'])
<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Institution', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control institution" id="institution" name="institutionId" required>
            <option value="" selected disabled>Selectionner l'institution</option>
            @foreach($institution as $item)
                <option value="{{$item->id}}" {{ ($moneytransfer != '' && $moneytransfer->institutionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
            @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endauth

<div class="form-group {{ $errors->has('compnayName') ? 'has-error' : ''}}">
    {!! Form::label('compnayName', 'Company name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('compnayName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('compnayName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('beneficaryName') ? 'has-error' : ''}}">
    {!! Form::label('beneficaryName', 'Beneficary name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('beneficaryName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('beneficaryName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
