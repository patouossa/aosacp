@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.niveau.management') . ' | ' . trans('labels.backend.access.niveau.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.niveau.management') }}
    <small>{{ trans('labels.backend.access.niveau.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Our Qualifications</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/niveau/create') }}" class="btn btn-primary " title="Add New Niveau"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 
    <p class="display-1">
         <p class="table-bordered">  From the list of qualifications below, select a qualification and click on the right arrow to move it to your list
            of qualifications. The reverse is true. You can select multiple qualifications by holding down the Ctrl Key while
                clicking on the qualification.</p></div>


        <div class="table-responsive">
            <div class="box-body">

                <table class="table table-borderless">
                    <tr >
                        <th>List of Qualifications</th>   <th>Our Qualifications</th>
                    </tr>
                    <tr>
                        <th>
                            <div class="form-group" >
                               1
                          </div>
                        </th>
                        <th>
                            <div class="form-group" >
                                   2
                            </div>
                        </th>
                    </tr>
                </table>

        </div>
            <div class="box-footer">
                {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-primary pull-right']) }}
            </div>
    </div> <!-- /.box-body -->
     
</div> 

@endsection