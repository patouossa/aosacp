@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.create'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.programstudie.management') }}
        <small>{{ trans('labels.backend.access.programstudie.create') }}</small>
    </h1>
@endsection

@section('content')

    {!! Form::open(['url' => '/admin/access/institutionupload', 'class' => 'form-horizontal', 'files' => true]) !!}
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            @include ('backend/access.bank-account-student.form')

        </div> <!-- /.box-body -->

        <div class="box-footer">
                {{--{{ Form::submit('Add Institution', ['class' => 'btn btn-primary pull-right']) }}--}}
        </div>   <!-- /.box-footer -->
        {!! Form::close() !!}

        <div class="box-footer">

                {{ Form::submit('Add Bank ', ['class' => 'btn btn-primary pull-right']) }}
            </div>

        <div class="table-responsive">

            <table class="table table-borderless">

                <thead>
                <tr bgcolor="#a9a9a9">
                    <th>
                        <div class="col-md-18">
                            Institution Name<br/><br/>
                            <select class="form-control institution" id="institution" name="institutionId" required onchange="loadBanks();">
                                <option value="" selected disabled>Selectionner l'institution</option>
                                @foreach($institution as $item)
                                    <option value="{{$item->id}}"  >{{$item->nom}}</option>
                                @endforeach

                            </select>
                            {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
                        </div>
                    <th>
                        <div class="col-md-22">
                            Bank Name<br/><br/>
                            <select class="form-control"  required name="bank_name" id="bank_name" onchange="loadAccounts()">
                                <option value="" selected disabled>Bank Name</option>

                            </select>
                        </div>

                    </th>
                    <th>
                        <div class="col-md-20">
                            Bank Account #<br/><br/>
                            <select class="form-control"  required name="bank_account" id="bank_account" onchange="loadAccounts()">
                                <option value="" selected disabled>Bank Name</option>

                            </select>
                        </div>

                    </th>
                    <th>
                        <div class="col-md-20">
                            Bank Account Name<br/><br/>
                            <div class="form-control">
                                <input type="text" name="bank_account_name" id="bank_account_name" />
                            </div>
                        </div>
                    </th>
                    <th>
                        <div class="col-md-20">
                            Bank Code<br/><br/>

                            <div class="form-control">
                                <input type="text" name="bank_code" id="bank_code" />
                            </div>
                        </div>

                    </th>
                    </th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                    <th>
                        <div class="col-md-14">
                            Branch Code<br/><br/>
                            <div class="form-control">
                                <input type="text" name="branch_code" id="branch_code" />
                            </div>
                        </div>

                    </th>
                    <th>
                        <div class="col-md-14">
                            RIB #<br/><br/>
                            <div class="form-control">
                                <input type="text" name="rib" id="rib" />
                            </div>
                        </div>

                    </th>
                    <th>
                        <div class="col-md-14">
                            Amount<br/><br/>
                            <div class="form-control">
                                <input type="text" name="amount" id="amount" />
                            </div>
                        </div>
                    </th>

                    <th>
                        <div class="col-md-14">
                            UploadLink<br/><br/>
                            <input type="file">
                        </div>
                    </th>

                    <th>
                        <div class="col-md-14">
                            Payment Date<br/><br/>
                            <div class="form-control">
                                <input type="date" name="date" />
                            </div>
                        </div>

                    </th>

                </thead>
                </tr>
                <tr >
                    <th>ID {{$student1->id}}</th>
                    <th> Institution Name</th>
                    <th> Bank Name</th>
                    <th> Account #</th>
                    <th> Amount</th>
                    <th>Upload</th>
                    <th>Action</th>

                </tr>
                @foreach($payments as $p)
                <tr >
                    <th> {{$p->id}} </th>
                    <th> {{$p->institution}}</th>
                    <th> {{$p->institution}}</th>
                    <th> {{$p->amount}}</th>
                    <th> {{$p->institution}}</th>
                    <th>{{$p->institution}}</th>
                    <th>

                        <a href="{{ url('admin/access/addbankaccount' . $p->id . '/edit') }}" class="btn btn-primary btn-xs" title="View/Edit Payment Details"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>

                    </th>
                </tr>
                @endforeach
           </div>



            </table>


            </thead>
                <tbody>
                {{--@foreach($programstudies as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->institution }}</td>
                        <td>{{ $item->faculte }}</td>
                        <td>{{ $item->departement }}</td>
                        <td>
                            <a href="{{ url('/admin/access/programstudie/' . $item->id . '/edit') }}"
                               class="btn btn-primary btn-xs" title="Edit Programstudie"><span
                                        class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/programstudie', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Programstudie" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Programstudie',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach--}}
                </tbody>
            </table>

        </div>

    </div>


@endsection