<div class="form-group {{ $errors->has('studentId') ? 'has-error' : ''}}">
    {!! Form::label('studentId', 'Studentid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('studentId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('studentId', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('institutionId') ? 'has-error' : ''}}">
    {!! Form::label('institutionId', 'Institutionid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('institutionId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('institutionId', '<p class="help-block">:message</p>') !!}
    </div>
</div>

