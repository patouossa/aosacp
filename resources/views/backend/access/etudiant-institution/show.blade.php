@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.etudiantinstitution.management') . ' | ' . trans('labels.backend.access.etudiantinstitution.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.etudiantinstitution.management') }}
    <small>{{ trans('labels.backend.access.etudiantinstitution.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.etudiantinstitution.show') }} - {{ $etudiantinstitution->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/etudiantinstitution/' . $etudiantinstitution->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit EtudiantInstitution"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/etudiantinstitution', $etudiantinstitution->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'EtudiantInstitution',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $etudiantinstitution->id }}</td>
                    </tr>
                    <tr><th> StudentId </th><td> {{ $etudiantinstitution->studentId }} </td></tr><tr><th> InstitutionId </th><td> {{ $etudiantinstitution->institutionId }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.etudiantinstitution.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
