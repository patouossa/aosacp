<div class="form-group {{ $errors->has('institutionId') ? 'has-error' : ''}}">
    {!! Form::label('institutionId', 'Institutionid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('institutionId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('institutionId', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Libelle', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('requirementId') ? 'has-error' : ''}}">
    {!! Form::label('requirementId', 'Requirementid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('requirementId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('requirementId', '<p class="help-block">:message</p>') !!}
    </div>
</div>

