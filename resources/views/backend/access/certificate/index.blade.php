@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.certificate.management') . ' | ' . trans('labels.backend.access.certificate.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.certificate.management') }}
    <small>{{ trans('labels.backend.access.certificate.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Certificate</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/certificate/create') }}" class="btn btn-primary " title="Add New Certificate"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Passport </th><th> Certificate </th><th> StudentId </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($certificate as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->passport }}</td><td>{{ $item->certificate }}</td><td>{{ $item->studentId }}</td>
                        <td>
                            <a href="{{ url('/admin/access/certificate/' . $item->id) }}" class="btn btn-success btn-xs" title="View Certificate"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/certificate/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Certificate"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/certificate', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Certificate" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Certificate',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $certificate->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection