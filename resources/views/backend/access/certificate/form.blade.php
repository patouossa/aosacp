<table class="table table-borderless">
    <thead>
    <tr>
        <th> Upload Name </th><th> Browser </th><th> File uploaded </th><th> Uploaded file Name </th><th>File size</th><th>Max File size</th>
    </tr>
    </thead>
    <tbody>
    @foreach($uploadrequirement as $item)
        @if($item->requirementId == 1)
        <tr>
            <td>{{ $item->libelle }}</td>
            <td>

                {!! Form::file('handWriting', null, ['class' => 'form-control']) !!}

            </td>
            <td><img src="{{ ($certificate!='' ? $certificate->handWriting : '')}}" alt=""></td>
            <td>{{ ($certificate!='' ? $certificate->handWritingName : '') }}</td>
            <td>{{ ($certificate!='' ? $certificate->handWritingSize :'') }}</td>
            <td>4M</td>
        </tr>
        @endif

        @if($item->requirementId == 2)
            <tr>
                <td>{{ $item->libelle }}</td>
                <td>

                    {!! Form::file('certifiedQualification', null, ['class' => 'form-control']) !!}

                </td>

                <td><img src="{{ ($certificate!='' ? $certificate->certifiedQualification : '')}}" alt=""></td>
                <td>{{($certificate!='' ? $certificate->certifiedQualificationName : '') }}</td>
                <td>{{($certificate!='' ? $certificate->certifiedQualificationSize : '') }}</td>
                <td>4M</td>
            </tr>
        @endif

        @if($item->requirementId == 3)
            <tr>
                <td>{{ $item->libelle }}</td>
                <td>

                    {!! Form::file('birth', null, ['class' => 'form-control']) !!}

                </td>
                <td><img src="{{ ($certificate!='' ? $certificate->birth : '')}}" alt=""></td>
                <td>{{ ($certificate!='' ? $certificate->birthName : '') }}</td>
                <td>{{ ($certificate!='' ? $certificate->birthSize : '') }}</td>
                <td>4M</td>
            </tr>
        @endif

        @if($item->requirementId == 4)
            <tr>
                <td>{{ $item->libelle }}</td>
                <td>

                    {!! Form::file('transcriptgcea', null, ['class' => 'form-control']) !!}

                </td>
                <td><img src="{{($certificate!='' ? $certificate->transcriptgcea : '')}}" alt=""></td>
                <td>{{ ($certificate!='' ? $certificate->transcriptgceaName : '') }}</td>
                <td>{{ ($certificate!='' ? $certificate->transcriptgceaSize :'') }}</td>
                <td>4M</td>
            </tr>
        @endif

        @if($item->requirementId == 5)
            <tr>
                <td>{{ $item->libelle }}</td>
                <td>

                    {!! Form::file('applicationForm', null, ['class' => 'form-control']) !!}

                </td>
                <td><img src="{{($certificate!='' ? $certificate->applicationForm : '')}}" alt=""></td>
                <td>{{ ($certificate!='' ? $certificate->applicationFormName: '')}}</td>
                <td>{{ ($certificate!='' ? $certificate->applicationFormSize : '') }}</td>
                <td>4M</td>
            </tr>
        @endif

        @if($item->requirementId == 5)
            <tr>
                <td>{{ $item->libelle }}</td>
                <td>

                    {!! Form::file('cni', null, ['class' => 'form-control']) !!}

                </td>
                <td><img src="{{($certificate!='' ? $certificate->cni : '')}}" alt=""></td>
                <td>{{ ($certificate!='' ? $certificate->cniName : '') }}</td>
                <td>{{ ($certificate!='' ? $certificate->cniSize : '') }}</td>
                <td>4M</td>


            </tr>
        @endif

    @endforeach

    <input type="hidden" value="{{$institution->id}}" name="institutionId">
    </tbody>
</table>