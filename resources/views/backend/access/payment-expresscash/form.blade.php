<div class="form-group {{ $errors->has('slipNumber') ? 'has-error' : ''}}">
    {!! Form::label('slipNumber', 'Slip Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('slipNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('slipNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
    {!! Form::label('dateDepot', 'Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
        {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
    {!! Form::label('amount', 'Amount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('amount', null, ['class' => 'form-control']) !!}
        {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('transactionNumber') ? 'has-error' : ''}}">
    {!! Form::label('transactionNumber', 'Transaction Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('transactionNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('transactionNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('scanDepositSlip') ? 'has-error' : ''}}">
    {!! Form::label('scanDepositSlip', 'Scan Deposit Slip', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('scanDepositSlip', null, ['class' => 'form-control']) !!}
        {!! $errors->first('scanDepositSlip', '<p class="help-block">:message</p>') !!}
    </div>
</div>
