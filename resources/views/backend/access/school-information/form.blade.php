<div class="form-group {{ $errors->has('startyear') ? 'has-error' : ''}}">
    {!! Form::label('startyear', 'Start year', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="month" name="startyear" value="{{ $schoolinformation != '' ? $schoolinformation->startyear : '' }}" class="form-control">
        {{--{!! Form::date('startyear', null, ['class' => 'form-control']) !!}--}}
        {!! $errors->first('startyear', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('endyear') ? 'has-error' : ''}}">
    {!! Form::label('endyear', 'End year', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="month" name="endyear" value="{{ $schoolinformation != '' ? $schoolinformation->endyear : '' }}" class="form-control">
        {{--{!! Form::date('endyear', null, ['class' => 'form-control']) !!}--}}
        {!! $errors->first('endyear', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nameschool') ? 'has-error' : ''}}">
    {!! Form::label('nameschool', 'Name of school', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nameschool', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nameschool', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('qualification') ? 'has-error' : ''}}">
    {!! Form::label('qualification', 'Qualification', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control qualification" id="qualification" name="qualification" required>
            <option value="" selected disabled>Select a Qualification</option>
            @foreach($qualifications as $item)
                <option value="{{$item->type}}" {{ ($schoolinformation != '' && $schoolinformation->qualification==  $item->type) ? 'selected="selected"' : '' }} >{{$item->type}}</option>
            @endforeach
        </select>
    </div>
</div>
