@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.school-information.management') . ' | ' . trans('labels.backend.access.school-information.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.school-information.management') }}
    <small>{{ trans('labels.backend.access.school-information.edit') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::model($schoolinformation, [
            'method' => 'PATCH',
            'url' => ['/admin/access/school-information', $schoolinformation->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.school-information.edit') }} - {{ $schoolinformation->id }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        @include ('backend/access.school-information.form')

        <div class="box-footer">

            <!-- Single button -->
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-primary pull-right dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        {{ Form::submit('Save and Close', ['class' => 'btn btn-default','name' => 'close']) }}
                    </li>
                    <li>
                        {{ Form::submit('Enter another School', ['class' => 'btn btn-default ','name' => 'close']) }}
                    </li>
                </ul>
            </div>

        </div>   <!-- /.box-footer -->

        {!! Form::close() !!}
    </div> <!-- /.box-body -->
        <div class="box-body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                     <th> Start year </th><th> End year </th><th> Name of school </th><th> Qualification </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schoolinformations as $item)
                    <tr>

                        <td>{{ $item->startyear }}</td><td>{{ $item->endyear }}</td><td>{{ $item->nameschool }}</td><td>{{ $item->qualification }}</td>
                        <td>
                            <a href="{{ url('/admin/access/school-information/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SchoolInformation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $schoolinformations->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     

    
</div> 


@endsection