@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.school-information.management') . ' | ' . trans('labels.backend.access.school-information.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.school-information.management') }}
    <small>{{ trans('labels.backend.access.school-information.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Schoolinformation</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/school-information/create') }}" class="btn btn-primary " title="Add New SchoolInformation"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                         <th> Start year </th><th> End year </th><th> Nameschool </th><th> Qualification </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($schoolinformation as $item)
                    <tr>
                        <td>{{ $item->startyear }}</td><td>{{ $item->endyear }}</td><td>{{ $item->nameschool }}</td><td>{{ $item->qualification }}</td>
                        <td>
                            <a href="{{ url('/admin/access/school-information/' . $item->id) }}" class="btn btn-success btn-xs" title="View SchoolInformation"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/school-information/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit SchoolInformation"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $schoolinformation->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection