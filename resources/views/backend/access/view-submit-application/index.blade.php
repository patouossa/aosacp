@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.billing.management') . ' | ' . trans('labels.backend.access.billing.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.billing.management') }}
    <small>{{ trans('labels.backend.access.billing.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">My View/Submit Application</h3>
        <div class="box-tools pull-right">
        

            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th> Institution </th><th> Application Status </th><th>Action</th>
                        
                </thead>
                <tbody>
                @foreach($billing as $item)
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="{{ url('/admin/access/viewsubmitApp/' . $item->id) }}" class="btn btn-success btn-xs" title="View Application"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/viewsubmitApp', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="View Application" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Billing',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $billing->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection