@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.payment-mobile.management') . ' | ' . trans('labels.backend.access.payment-mobile.edit'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.payment-mobile.management') }}
    <small>{{ trans('labels.backend.access.payment-mobile.edit') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::model($payment-mobile, [
            'method' => 'PATCH',
            'url' => ['/admin/access/payment-mobile', $paymentmobile->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.payment-mobile.edit') }} - {{ $paymentmobile->id }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        @include ('backend/access.payment-mobile.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{ link_to_route('admin.access.payment-mobile.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}
        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary pull-right']) }}                
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection