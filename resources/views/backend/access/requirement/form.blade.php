<div class="form-group {{ $errors->has('libelleEN') ? 'has-error' : ''}}">
    {!! Form::label('libelleEN', 'Libelleen', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelleEN', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelleEN', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('libelleFR') ? 'has-error' : ''}}">
    {!! Form::label('libelleFR', 'Libellefr', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelleFR', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelleFR', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('status', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('status', '0', true) !!} No</label>
</div>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('institutionId') ? 'has-error' : ''}}">
    {!! Form::label('institutionId', 'Institutionid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('institutionId', null, ['class' => 'form-control']) !!}
        {!! $errors->first('institutionId', '<p class="help-block">:message</p>') !!}
    </div>
</div>

