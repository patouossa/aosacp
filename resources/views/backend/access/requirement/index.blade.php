@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.requirement.management') . ' | ' . trans('labels.backend.access.requirement.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.requirement.management') }}
    <small>{{ trans('labels.backend.access.requirement.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Requirement</h3>
        <div class="box-tools pull-right">
        
            {{--<a href="{{ url('/admin/access/requirement/create') }}" class="btn btn-primary " title="Add New Requirement"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>--}}
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">
        {!! Form::open(['url' => '/admin/access/requirement', 'class' => 'form-horizontal', 'files' => true]) !!}
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th><th> LibelleEN </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($requirement as $item)
                    <tr>
                        <td> <input name="status[]" type="checkbox" {{ (($item->institutionId != null || $item->institutionId !=  '') && $item->status !=  1) ? 'checked="checked"' : '' }} value="{{$item->id}}"></td>
                        <td>{{ $item->libelleEN }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $requirement->render() !!} </div>
        </div>
        <div class="box-footer">
            {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-primary pull-right']) }}
        </div>   <!-- /.box-footer -->

    </div>
    {!! Form::close() !!}
    @if(access()->user()->type == 'manage-univ')

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>ID</th><th> Libelle </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($uploadrequirement as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelle }}</td>
                        <td>
                           {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/upload-requirement', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete UploadRequirement" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete UploadRequirement',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        @endif


</div> <!-- /.box-body -->


@endsection