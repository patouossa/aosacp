@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.requirement.management') . ' | ' . trans('labels.backend.access.requirement.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.requirement.management') }}
    <small>{{ trans('labels.backend.access.requirement.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.requirement.show') }} - {{ $requirement->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/requirement/' . $requirement->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Requirement"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/requirement', $requirement->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Requirement',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $requirement->id }}</td>
                    </tr>
                    <tr><th> LibelleEN </th><td> {{ $requirement->libelleEN }} </td></tr><tr><th> LibelleFR </th><td> {{ $requirement->libelleFR }} </td></tr><tr><th> Status </th><td> {{ $requirement->status }} </td></tr><tr><th> InstitutionId </th><td> {{ $requirement->institutionId }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.requirement.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
