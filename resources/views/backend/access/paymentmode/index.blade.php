@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.paymentmode.management') . ' | ' . trans('labels.backend.access.paymentmode.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.paymentmode.management') }}
    <small>{{ trans('labels.backend.access.paymentmode.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Paymentmode</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/paymentmode/create') }}" class="btn btn-primary " title="Add New Paymentmode"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> TypePaiementId </th><th> TypePaiement </th><th> Montant </th><th> InstitutionId </th><th> Institution </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($paymentmode as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->typePaiementId }}</td><td>{{ $item->typePaiement }}</td><td>{{ $item->montant }}</td><td>{{ $item->institutionId }}</td><td>{{ $item->institution }}</td>
                        <td>
                            <a href="{{ url('/admin/access/paymentmode/' . $item->id) }}" class="btn btn-success btn-xs" title="View Paymentmode"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/paymentmode/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Paymentmode"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/paymentmode', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Paymentmode" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Paymentmode',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $paymentmode->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection