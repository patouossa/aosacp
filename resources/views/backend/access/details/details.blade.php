@extends ('backend.layouts.app')


@section('content')

    <div class="box box-success">

        <div class="box-body">

            @include ('backend/access.details.submissiondetails')

        </div> <!-- /.box-body -->

    </div>

@endsection