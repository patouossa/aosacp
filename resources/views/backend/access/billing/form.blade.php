<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', trans('labels.frontend.institution'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control institution" id="institution" name="institutionId" required>
                <option value="" selected disabled>Selectionner l'institution</option>
                @foreach($institutions as $item)
                    <option value="{{$item->id}}" {{ ($billing != '' && $billing->institutionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
                @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('montantSysteme') ? 'has-error' : ''}}">
    {!! Form::label('montantSysteme', 'Web Admin Fees', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('montantSysteme', null, ['class' => 'form-control']) !!}
        {!! $errors->first('montantSysteme', '<p class="help-block">:message</p>') !!}
    </div>
</div>

