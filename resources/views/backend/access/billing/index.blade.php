@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.billing.management') . ' | ' . trans('labels.backend.access.billing.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.billing.management') }}
    <small>{{ trans('labels.backend.access.billing.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Our Bills</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/billing/create') }}" class="btn btn-primary " title="Add New Billing"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">
        <div class="box-body">

            <form method="post" action="traitement.php">
                <div class="form-group">
                       <label  > National Application Fee:</label><input class="form-control" type="text" name="" />
                </div>

                <div class="form-group">
                       <label > International Application Fee:</label><input class="form-control" type="text" name="" />
                </div>

                    {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-primary pull-right']) }}

            </form>

        </div>

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Application Fee ID </th>
                        <th> Fee Type </th>
                        <th>Amount</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($billing as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td></td><td></td>
                        <td>
                            <a href="{{ url('/admin/access/billing/' . $item->id) }}" class="btn btn-success btn-xs" title="View Billing"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/billing/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Billing"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/billing', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Billing" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Billing',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $billing->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection