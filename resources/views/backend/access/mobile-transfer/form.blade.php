@permissions(['manage-users', 'manage-roles'])
<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Institution', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control institution" id="institution" name="institutionId" required>
            <option value="" selected disabled>Selectionner l'institution</option>
            @foreach($institution as $item)
                <option value="{{$item->id}}" {{ ($mobiletransfer != '' && $mobiletransfer->institutionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
            @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endauth

<div class="form-group {{ $errors->has('operatorName') ? 'has-error' : ''}}">
    {!! Form::label('operatorName', 'Operator name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('operatorName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('operatorName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobileAccountName') ? 'has-error' : ''}}">
    {!! Form::label('mobileAccountName', 'Mobile account name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobileAccountName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobileAccountName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>

