@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.examination.management') . ' | ' . trans('labels.backend.access.examination.create')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.examination.management') }}
    <small>{{ trans('labels.backend.access.examination.create') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/examination', 'class' => 'form-horizontal', 'files' => true]) !!} 
<div class="box box-success">
    <div class="box-header with-border">

        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">        
        
        @include ('backend/access.examination.form')
        
    </div> <!-- /.box-body -->

    <div class="box-footer">

        <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-primary pull-right dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li>
                    {{ Form::submit('Save and Close', ['class' => 'btn btn-default','name' => 'close']) }}
                </li>
                <li>
                    {{ Form::submit('Enter another School', ['class' => 'btn btn-default ','name' => 'close']) }}
                </li>
            </ul>
        </div>

    </div>   <!-- /.box-footer -->

    {!! Form::close() !!}

    <div class="box-body">

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th> Qualification </th> <th> Sitting </th><th> Examination year </th><th> Examination center </th><th> Candidate number </th><th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($examinations as $item)
                    <tr>
                        <td>{{ $item->qualification }}</td><td>{{ $item->nbSitting }}</td><td>{{ $item->examinationyear }}</td><td>{{ $item->examinationcenter }}</td><td>{{ $item->candidatenumber }}</td>
                        <td>
                            <a href="{{ url('/admin/access/examination/' . $item->id) }}" class="btn btn-success btn-xs" title="View Examination"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/examination/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Examination"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $examinations->render() !!} </div>
        </div>

    </div> <!-- /.box-body -->
</div>

@endsection