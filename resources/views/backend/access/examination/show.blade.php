@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.examination.management') . ' | ' . trans('labels.backend.access.examination.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.examination.management') }}
    <small>{{ trans('labels.backend.access.examination.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.examination.show') }} - {{ $examination->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/examination/' . $examination->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Examination"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/examination', $examination->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Examination',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $examination->id }}</td>
                    </tr>
                    <tr><th> Examinationyear </th><td> {{ $examination->examinationyear }} </td></tr><tr><th> Candidatenumber </th><td> {{ $examination->candidatenumber }} </td></tr><tr><th> Examinationcenter </th><td> {{ $examination->examinationcenter }} </td></tr><tr><th> Resultobtained </th><td> {{ $examination->resultobtained }} </td></tr><tr><th> Qualification </th><td> {{ $examination->qualification }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.examination.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
