
@permissions(['manage-users', 'manage-roles'])
<div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
    {!! Form::label('institution', 'Institution', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control institution" id="institution" name="institutionId" required>
            <option value="" selected disabled>Selectionner l'institution</option>
            @foreach($institution as $item)
                <option value="{{$item->id}}" {{ ($bankaccount != '' && $bankaccount->institutionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->nom}}</option>
            @endforeach
        </select>
        {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endauth

<div class="form-group {{ $errors->has('bankName') ? 'has-error' : ''}}">
    {!! Form::label('bankName', 'Bank name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bankName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bankName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('accountName') ? 'has-error' : ''}}">
    {!! Form::label('accountName', 'Account name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('accountName', null, ['class' => 'form-control']) !!}
        {!! $errors->first('accountName', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('bankCode') ? 'has-error' : ''}}">
    {!! Form::label('bankCode', 'Bank code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bankCode', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bankCode', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('accountNumber') ? 'has-error' : ''}}">
    {!! Form::label('accountNumber', 'Account number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('accountNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('accountNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('rip') ? 'has-error' : ''}}">
    {!! Form::label('rip', 'Rip', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('rip', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rip', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('reference', null, ['class' => 'form-control','min'=>1,'max'=>5,'placeholder' => 'Please enter this reference number to identify these deposit. Length 6']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>

