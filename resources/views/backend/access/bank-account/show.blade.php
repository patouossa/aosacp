@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.bank-account.management') . ' | ' . trans('labels.backend.access.bank-account.show'))


@section('page-header')
<h1>
    {{ trans('labels.backend.access.bank-account.management') }}
    <small>{{ trans('labels.backend.access.bank-account.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.bank-account.show') }} - {{ $bankaccount->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/bank-account/' . $bankaccount->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit BankAccount"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/bank-account', $bankaccount->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'bank-account',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $bankaccount->id }}</td>
                    </tr>
                    <tr><th> BankName </th><td> {{ $bankaccount->bankName }} </td></tr><tr><th> AccountName </th><td> {{ $bankaccount->accountName }} </td></tr><tr><th> BankCode </th><td> {{ $bankaccount->bankCode }} </td></tr><tr><th> BrankCode </th><td> {{ $bankaccount->brankCode }} </td></tr><tr><th> AccountNumber </th><td> {{ $bankaccount->accountNumber }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.bank-account.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
