<div class="form-group {{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', trans('labels.frontend.libelle') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('libelleRegion') ? 'has-error' : ''}}">
    {!! Form::label('Region', trans('labels.frontend.region') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control region" name="regionId" id="region" required>
            <option selected disabled>{{trans('labels.frontend.select_region') }}</option>
            @foreach($region as $item)
                @if ( Config::get('app.locale') == 'en')
                    <option value="{{$item->id}}" {{ ($ville != '' && $ville->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleEN}}</option>
                @else
                    <option value="{{$item->id}}" {{ ($ville != '' && $ville->regionId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelleFR}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>

    jQuery(document).ready(function ($) {
        $('.region').change(function () {
            region = $(this).val();
            $.get("{{ url('admin/access/region/departement/all')}}",
                    {region: $(this).val()},
                    function (data) {
                        var model = $('#departement');
                        model.empty();
                        model.append("<option value = '' selected>{{ trans('labels.frontend.select_departement') }}</option>");

                        $.each(data, function (index, element) {



                            model.append("<option value='" + element.id + "'  >" + element.libelle + "</option>");

                        });
                    });
        });
    });

    jQuery(document).ready(function ($) {

        $('.departement').change(function () {
            $.get("{{ url('admin/access/departement/arrondissement/all')}}",
                    {departement: $(this).val()},
                    function (data) {
                        var model = $('#arrondissement');
                        model.empty();
                        model.append("<option value = '' selected>{{ trans('labels.frontend.select_arrondissement') }}</option>");

                        $.each(data, function (index, element) {


                            model.append("<option value='" + element.id + "'  >" + element.libelle + "</option>");

                        });
                    });
        });
    });


</script>