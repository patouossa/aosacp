<div class="form-group {{ $errors->has('libelleEN') ? 'has-error' : ''}}">
    {!! Form::label('libelleEN', 'Libelleen', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelleEN', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelleEN', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('libelleFR') ? 'has-error' : ''}}">
    {!! Form::label('libelleFR', 'Libellefr', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelleFR', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelleFR', '<p class="help-block">:message</p>') !!}
    </div>
</div>

