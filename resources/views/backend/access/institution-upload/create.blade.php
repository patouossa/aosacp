@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.create'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.programstudie.management') }}
        <small>{{ trans('labels.backend.access.programstudie.create') }}</small>
    </h1>
@endsection

@section('content')

    {!! Form::open(['url' => '/admin/access/institutionupload', 'class' => 'form-horizontal', 'files' => true]) !!}
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            @include ('backend/access.institution-upload.form')

        </div> <!-- /.box-body -->

        <div class="box-footer">
                {{--{{ Form::submit('Add Institution', ['class' => 'btn btn-primary pull-right']) }}--}}
        </div>   <!-- /.box-footer -->
        {!! Form::close() !!}

        <div class="table-responsive">

            <table class="table table-borderless">

                <thead>

                <tr  bgcolor="#a9a9a9" >
                    <th >Institution Name:</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th># of Uploads</th>
                    <th>6</th>
                </tr>

                <tr>
                    <th>ID</th>
                    <th> Upload Name</th>
                    <th> Browse</th>
                    <th> Uploaded File Name</th>
                    <th>File Size</th>
                    <th>Max File Size</th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th>
                        <input  type="file"  name="avatar" accept="image/png, image/jpeg">
                    </th>
                </tr>
                </thead>
                <tbody>

                {{--@foreach($programstudies as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->institution }}</td>
                        <td>{{ $item->faculte }}</td>
                        <td>{{ $item->departement }}</td>
                        <td>
                            <a href="{{ url('/admin/access/programstudie/' . $item->id . '/edit') }}"
                               class="btn btn-primary btn-xs" title="Edit Programstudie"><span
                                        class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/programstudie', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Programstudie" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Programstudie',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach--}}
                </tbody>
            </table>
            <div class="box-footer">

                {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-primary pull-right']) }}
            </div>

        </div>

    </div>

<script>

</script>
@endsection