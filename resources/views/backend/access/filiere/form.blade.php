<div class="form-group {{ $errors->has('libelle') ? 'has-error' : ''}}">
    {!! Form::label('libelle', 'Program Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
        {!! $errors->first('libelle', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('departement') ? 'has-error' : ''}}">
    {!! Form::label('departement', trans('labels.frontend.departement'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control departement" id="departement" name="departementId" required>
            <option value="" selected disabled>{{ trans('labels.frontend.select_departement') }}</option>
            @foreach($departement as $item)
                <option value="{{$item->id}}" {{ ($filiere != '' && $filiere->departementId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
            @endforeach
        </select>
        {!! $errors->first('departement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('departement') ? 'has-error' : ''}}">
    {!! Form::label('departement', 'Anticipated Qualification', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control niveau" id="niveau" name="niveauId" required>
            <option value="" selected disabled>Anticipated Qualification</option>
            @foreach($niveau as $item)
                <option value="{{$item->id}}" {{ ($filiere != '' && $filiere->niveauId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->niveau}}</option>
            @endforeach
        </select>
        {!! $errors->first('departement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('nbPlace', 'Nombre de place', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('nbPlace', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nbPlace', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
    {!! Form::label('duration', 'Duration', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('duration', null, ['class' => 'form-control']) !!}
        {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

    jQuery(document).ready(function ($) {
        $('.departement').change(function () {
            $.get("{{ url('admin/access/departement/niveau/all')}}",
                    {departement: $(this).val()},
                    function (data) {
                        var model = $('#niveau');
                        model.empty();
                        $.each(data, function (index, element) {

                            model.append("<option value='" + element.id + "'>" + element.niveau + "</option>");
                        });
                    });
        });
    });

</script>
