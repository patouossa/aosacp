@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.filiere.management') . ' | ' . trans('labels.backend.access.filiere.show')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.filiere.management') }}
    <small>{{ trans('labels.backend.access.filiere.show') }}</small>
</h1> 
@endsection 

@section('content')


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.filiere.show') }} - {{ $filiere->id }}</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('admin/access/filiere/' . $filiere->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Filiere"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
            
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['admin/access/filiere', $filiere->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Filiere',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
                        
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>ID</th><td>{{ $filiere->id }}</td>
                    </tr>
                    <tr><th> Libelle </th><td> {{ $filiere->libelle }} </td></tr><tr><th> Code </th><td> {{ $filiere->code }} </td></tr><tr><th> Departemnent </th><td> {{ $filiere->departemnent }} </td></tr>
                </tbody>
            </table>
        </div>)
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        
        {{ link_to_route('admin.access.filiere.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-default pull-right']) }}
        
    </div>   <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection
