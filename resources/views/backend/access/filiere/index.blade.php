@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.filiere.management') . ' | ' . trans('labels.backend.access.filiere.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.filiere.management') }}
    <small>{{ trans('labels.backend.access.filiere.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Our Study Programs (Courses)</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/filiere/create') }}" class="btn btn-primary " title="Add New Filiere"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <div class="form-group">

                <table class="table table-borderless">
                    <tr >
                        <th>Study Program ID:</th> <th>System Generated:</th> <th>Custom Generated:</th>
                    </tr>
                    <tr bgcolor="#a9a9a9" >

                        <th>

                            <div class="form-group">
                                <label  >Study Program Name:</label><input class="form-control" type="text" name="" />
                            </div>
                        </th>

                        <th>
                            Study Program Dept:<br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>  </option>

                            </select>
                        </th>

                        </th>
                    </tr>
                    <tr bgcolor="#a9a9a9" >
                        <th>
                            Study Program Faculty:<br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>    </option>

                            </select>
                        </th>
                        <th>
                            Anticipated Qualification:<br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>    </option>

                            </select>
                        </th>

                    </tr>
                    <tr>
                        <th> </th>
                        <th> </th>
                        <th>Status: </th>
                        <th>Active / Inactive </th>
                    </tr>
            </div>
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>SP. ID</th>
                    <th>Study Program Name:</th>
                    <th>Study Program Faculty: </th>
                    <th> Study Program Dept: </th>
                    <th>Anticipated Qualification:</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($filiere as $item)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->libelle }}</td><td>{{ $item->code }}</td><td></td><td>{{ $item->departemnent }}</td><td>{{ $item->nbPlace }}</td><td>{{ $item->duration }}</td>
                        <td>
                            <a href="{{ url('/admin/access/filiere/' . $item->id) }}" class="btn btn-success btn-xs" title="View Filiere"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/filiere/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Filiere"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/filiere', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Filiere" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Filiere',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <div class="box-footer">
                    {{ Form::submit(trans('buttons.general.crud.save'), ['class' => 'btn btn-primary pull-right']) }}
                </div>
            </table>
            <div class="pagination-wrapper"> {!! $filiere->render() !!} </div>
        </div>

    </div> <!-- /.box-body -->
     
</div> 

@endsection