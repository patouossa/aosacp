@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.create'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.programstudie.management') }}
        <small>{{ trans('labels.backend.access.programstudie.create') }}</small>
    </h1>
@endsection

@section('content')

    {!! Form::open(['url' => '/admin/access/institutionupload', 'class' => 'form-horizontal', 'files' => true]) !!}
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            @include ('backend/access.money-transfer-student.form')

        </div> <!-- /.box-body -->
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr bgcolor="#a9a9a9">
                    <th>
                        <div class="col-md-18">
                            Institution Name<br/><br/>
                            <select class="form-control institution" id="institution" name="institutionId" required>
                                <option value="" selected disabled>Selectionner l'institution</option>
                                @foreach($institution as $item)
                                    <option value="{{$item->id}}"  >{{$item->nom}}</option>
                                @endforeach

                            </select>
                            {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
                        </div>

                    <th>
                        <div class="col-md-20">
                            Money Transfer Name<br/><br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>  </option>

                            </select>
                        </div>
                    </th>
                    <th>
                        <div class="col-md-20">
                            Beneficiary Name<br/><br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled></option>

                            </select>
                        </div>

                    </th>
                    <th>
                        <div class="col-md-18">
                            Beneficiary Town<br/><br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>          </option>

                            </select>
                        </div>

                    </th>
                    </th>
                </tr>
                <tr bgcolor="#a9a9a9" >
                    <th>
                        <div class="col-md-14">
                            Beneficiary Phone #<br/><br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>    </option>

                            </select>
                        </div>

                    </th>
                    <th>

                            Upload Transfer Receipt<br/><br/>
                    <input  type="file">

                    </th>
                    <th>
                        <div class="col-md-14">
                            Amount Deposited<br/><br/>
                            <select class="form-control"  required>
                                <option value="" selected disabled>    </option>

                            </select>
                        </div>
                    </th>

                    <th>

                            Transfer Date<br/><br/>
                            <form class="form-control" action="traitement.php">
                                <input type="date" name="date" />
                            </form>


                    </th>
                </tr>
                <tr>

                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th></th>
                    <th>
                        <div class="box-footer">
                            {{ Form::submit('ADD TRANSFER PAYMENT ', ['class' => 'btn btn-primary pull-right']) }}
                        </div>
                    </th>
                </tr>
                <tr>

                    <th>Institution </th>
                    <th>Transfer Company</th>
                    <th>Beneficiary Name </th>
                    <th>Beneficiary #</th>
                    <th>Amount</th>
                    <th>UploadLink</th>
                    <th>Action</th>

                </tr>
                <tr >
                    <th></th>
                    <th> </th>
                    <th> </th>
                    <th> </th>
                    <th></th>
                    <th></th>
                    <th>
                        <a href="{{ url('admin/access/money-transfer-student' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="View/Edit Payment Details"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                    </th>

                </tr>


        </div>
        </th>

        </tr>
        </thead>

        </table>


    </div>


@endsection