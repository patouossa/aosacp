@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.programstudie.management') }}
    <small>{{ trans('labels.backend.access.programstudie.edit') }}</small>
</h1> 
@endsection 

@section('content')

{!! Form::model($programstudie, [
            'method' => 'PATCH',
            'url' => ['/admin/access/programstudie', $programstudie->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.access.programstudie.edit') }} - {{ $programstudie->id }}</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        @include ('backend/access.programstudie.form')
        
    </div> <!-- /.box-body -->
     
    <div class="box-footer">   
        {{ link_to_route('admin.access.programstudie.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}
        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary pull-right']) }}
    </div>   <!-- /.box-footer -->
    {!! Form::close() !!}


    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
            <tr>
                <th>ID</th><th> Institution </th><th> Faculté </th><th> Departement </th><th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($programstudies as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->institution }}</td><td>{{ $item->faculte }}</td><td>{{ $item->departement }}</td>
                    <td>
                        <a href="{{ url('/admin/access/programstudie/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Programstudie"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/access/programstudie', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Programstudie" />', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete Programstudie',
                                'onclick'=>'return confirm("Confirm delete?")'
                        )) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

</div> 


@endsection