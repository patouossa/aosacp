@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.programstudie.management') . ' | ' . trans('labels.backend.access.programstudie.create'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.programstudie.management') }}
        <small>{{ trans('labels.backend.access.programstudie.create') }}</small>
    </h1>
@endsection

@section('content')

    {!! Form::open(['url' => '/admin/access/institutionupload', 'class' => 'form-horizontal', 'files' => true]) !!}
    <div class="box box-success">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            @include ('backend/access.view-payment-options.form')


        </div> <!-- /.box-body -->

        <div class="box-footer">
                {{--{{ Form::submit('Add Institution', ['class' => 'btn btn-primary pull-right']) }}--}}
        </div>   <!-- /.box-footer -->
        {!! Form::close() !!}

        <div class="box-footer">

            {{ Form::submit(trans('buttons.general.crud.print'), ['class' => 'btn btn-primary pull-right']) }}
        </div>
        <div class="table-responsive">

            <style>
                table, tr, td {
                    border: 1px solid black;
                }
            </style>
            <div id="options">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th colspan="6" bgcolor="#a9a9a9">Bank Account Deposit</th>
                    </tr>
                    <tr>
                        <td >Bank Name</td>
                        <td>Account name</td>
                        <td>Bank Code</td>
                        <td>Branch Code</td>
                        <td>Account #</td>
                        <td>RIB #</td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="6" bgcolor="#a9a9a9">Money Transfer</th>
                    </tr>
                    <tr>
                        <td >Company Name</td>
                        <td>Beneficiary Name</td>
                        <td>Town</td>
                        <td>Phone #</td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="6" bgcolor="#a9a9a9">Mobile Operator Transfer</th>
                    </tr>
                    <tr>
                        <td >Mobile Operator Name</td>
                        <td>Mobile Account Name</td>
                        <td>Mobile # </td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th colspan="6" bgcolor="#a9a9a9">Debit Wallet Operator</th>
                    </tr>
                    <tr>
                        <td >Debit Wallet Operator Name</td>
                        <td>Merchant Name</td>
                        <td>Merchant Account # </td>
                        <td>Merchant Phone #</td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($programstudies as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->institution }}</td>
                            <td>{{ $item->faculte }}</td>
                            <td>{{ $item->departement }}</td>
                            <td>
                                <a href="{{ url('/admin/access/programstudie/' . $item->id . '/edit') }}"
                                   class="btn btn-primary btn-xs" title="Edit Programstudie"><span
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/access/programstudie', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Programstudie" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Programstudie',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach--}}
                    </tbody>
                </table>
            </div>


        </div>

    </div>


@endsection