<div><h2>My Payment Options</h2></div><br/>
<div class="col-md-6">
    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('institution', trans('labels.frontend.institution'), ['class' => 'col-md-4 control-label']) !!}

        <div class="col-md-6">
            <select class="form-control institution" id="institution" name="institutionId" required onchange="loadOptions();">

                    <option value="" selected disabled>Selectionner l'institution</option>
                    @foreach($institution as $item)
                        <option value="{{$item->id}}"  >{{$item->nom}}</option>
                    @endforeach

            </select>
            {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

    /*  var acc = document.getElementsByClassName("accordion");
      var i;

      acc[0].addEventListener("click", function () {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
          } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
          }
      });*/

    var institution = $('.institution').val();

    jQuery(document).ready(function ($) {


        $.get("{{ url('admin/access/institution/count/choise')}}",
            {institution: institution},
            function (data) {
                $('.nbstudy').val(data.nbChoise);
            });

    })


    function loadOptions() {
        $.ajax({
            url: '{{url("admin/access/paymentoptions")}}?institution_id='+$("#institution").val(),
            type: 'GET',
            dataType:'json',
            success: function (r) {
                console.log(r);

                var data = '<table class="table table-borderless">\n' +
                    '                <tr>\n' +
                    '                    <th colspan="6" bgcolor="#a9a9a9">Bank Account Deposit</th>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td >Bank Name</td>\n' +
                    '                    <td>Account name</td>\n' +
                    '                    <td>Bank Code</td>\n' +
                    '                    <td>Branch Code</td>\n' +
                    '                    <td>Account #</td>\n' +
                    '                    <td>RIB #</td>\n' +
                    '                </tr>';
                for (var i = 0; i < r.bank_accounts.length; i++) {
                    data += '<tr>' +
                        '                    <td>' + r.bank_accounts[i].bankName + '</td>' +
                        '                    <td>' + r.bank_accounts[i].accountName + '</td>' +
                        '                    <td>' + r.bank_accounts[i].bankCode + '</td>' +
                        '                    <td>' + r.bank_accounts[i].brankCode + '</td>' +
                        '                    <td>' + r.bank_accounts[i].accountNumber + '</td>' +
                        '                    <td>' + r.bank_accounts[i].rip + '</td>' +
                        '                </tr>';
                }

                data += '<tr>\n' +
                    '                    <th colspan="6" bgcolor="#a9a9a9">Money Transfer</th>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td >Company Name</td>\n' +
                    '                    <td>Beneficiary Name</td>\n' +
                    '                    <td>Phone #</td>\n' +
                    '                </tr>';


                for (var i = 0; i < r.money_transfer.length; i++) {
                    data += '<tr>' +
                        '                    <td>' + r.money_transfer[i].compnayName + '</td>' +
                        '                    <td>' + r.money_transfer[i].beneficaryName + '</td>' +
                        '                    <td>' + r.money_transfer[i].mobile + '</td>' +
                        '                </tr>';
                }

                data += '<tr>\n' +
                    '                    <th colspan="6" bgcolor="#a9a9a9">Mobile Operator Transfer</th>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td >Mobile Operator Name</td>\n' +
                    '                    <td>Mobile Account Name</td>\n' +
                    '                    <td>Mobile # </td>\n' +
                    '                </tr>';


                for (var i = 0; i < r.mobile_operator.length; i++) {
                    data += '<tr>' +
                        '                    <td>' + r.mobile_operator[i].operatorName + '</td>' +
                        '                    <td>' + r.mobile_operator[i].mobileAccountName + '</td>' +
                        '                    <td>' + r.mobile_operator[i].mobile + '</td>' +
                        '                </tr>';
                }

                data += '<tr>\n' +
                    '                    <th colspan="6" bgcolor="#a9a9a9">Debit Wallet Operator</th>\n' +
                    '                </tr>\n' +
                    '                <tr>\n' +
                    '                    <td >Debit Wallet Operator Name</td>\n' +
                    '                    <td>Merchant Name</td>\n' +
                    '                    <td>Merchant Account # </td>\n' +
                    '                </tr>';


                for (var i = 0; i < r.wallet.length; i++) {
                    data += '<tr>' +
                        '                    <td>' + r.wallet[i].institutionName + '</td>' +
                        '                    <td>' + r.wallet[i].accountName + '</td>' +
                        '                    <td>' + r.wallet[i].accountNumber + '</td>' +
                        '                </tr>';
                }

                data += '</thead>\n' +
                    '                </table>';

                $("#options").html(data);
            }
        });
    }


    $('.faculte').change(function () {

        $.get("{{ url('admin/access/faculte/departement/all')}}",
            {faculte: $(this).val()},
            function (data) {
                var model = $('#departement');
                model.empty();
                model.append("<option value=''>Selectionner le departement</option>");
                $.each(data, function (index, element) {
                    model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                });
            });
    });

    $('.departement').change(function () {
        $.get("{{ url('admin/access/departement/niveau/all')}}",
            {departement: $(this).val()},
            function (data) {
                var model = $('#niveau');
                model.empty();
                model.append("<option value=''>Selectionner le Diplôme</option>");
                $.each(data, function (index, element) {

                    model.append("<option value='" + element.id + "'>" + element.niveau + "</option>");
                });
            });
    });

    $('.niveau').change(function () {
        $.get("{{ url('admin/access/niveau/filiere/all')}}",
            {niveau: $(this).val()},
            function (data) {
                var model = $('#filiere');
                model.empty();
                model.append("<option value=''>Selectionner la filière</option>");
                $.each(data, function (index, element) {

                    model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                });
            });
    });

    $('.filiere').change(function () {
        $.get("{{ url('admin/access/filiere/place/all')}}",
            {filiere: $(this).val()},
            function (data) {

                console.log(data)

                $('.place').val(data.nbPlace);
                /*

                                    model.append('<label class="label label-warning" id="place">' + data.nbPlace + ' place(s) disponibles</label>');

                */

            });
    });

    $('.institution').change(function () {
        $.get("{{ url('admin/access/institution/count/choise')}}",
            {institution: $(this).val()},
            function (data) {
                $('.nbstudy').val(data.nbChoise);
            });
    });

</script>
