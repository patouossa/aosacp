<div class="form-group {{ $errors->has('niveau') ? 'has-error' : ''}}">
    {!! Form::label('niveau', 'Anticipated Qualification', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('niveau', null, ['class' => 'form-control']) !!}
        {!! $errors->first('niveau', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('departement') ? 'has-error' : ''}}">
    {!! Form::label('departement', trans('labels.frontend.departement'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select class="form-control" name="departementId" required>
            <option value="" selected disabled>{{ trans('labels.frontend.select_departement') }}</option>
            @foreach($departement as $item)
                <option value="{{$item->id}}" {{ ($niveau != '' && $niveau->departementId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->libelle}}</option>
            @endforeach
        </select>
        {!! $errors->first('departement', '<p class="help-block">:message</p>') !!}
    </div>
</div>