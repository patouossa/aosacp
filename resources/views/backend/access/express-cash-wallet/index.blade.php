@extends ('backend.layouts.app')


@section ('title', trans('labels.backend.access.express-cash-wallet.management') . ' | ' . trans('labels.backend.access.express-cash-wallet.edit'))


@section('page-header')
    <h1>
        {{ trans('labels.backend.access.express-cash-wallet.management') }}
        <small>{{ trans('labels.backend.access.express-cash-wallet.edit') }}</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Expresscashwallet</h3>
            <div class="box-tools pull-right">

                <a href="{{ url('/admin/access/express-cash-wallet/create') }}" class="btn btn-primary "
                   title="Add New ExpressCashWallet"><span class="glyphicon glyphicon-plus"
                                                           aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>

                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div> <!--box-tools pull-right-->
        </div> <!-- /.box-header -->
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Institution Name</th>
                        <th> Account Name</th>
                        <th> Account Number</th>
                        <th> Reference</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($expresscashwallet as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->institutionName }}</td>
                            <td>{{ $item->accountName }}</td>
                            <td>{{ $item->accountNumber }}</td>
                            <td>{{ $item->reference }}</td>

                            @if( empty(access()->user()->type))
                                @if($item->adminEmail != null)
                                    <td>
                                        <a href="{{ url('/admin/access/express-cash-wallet/' . $item->id) }}"
                                           class="btn btn-success btn-xs" title="View ExpressCashWallet"><span
                                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        <a href="{{ url('/admin/access/express-cash-wallet/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs" title="Edit ExpressCashWallet"><span
                                                    class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/access/express-cash-wallet', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete ExpressCashWallet" />', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete ExpressCashWallet',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}

                                    </td>
                                @endif

                            @else
                                <td>
                                    <a href="{{ url('/admin/access/express-cash-wallet/' . $item->id) }}"
                                       class="btn btn-success btn-xs" title="View ExpressCashWallet"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                    <a href="{{ url('/admin/access/express-cash-wallet/' . $item->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit ExpressCashWallet"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/access/express-cash-wallet', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete ExpressCashWallet" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete ExpressCashWallet',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}

                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $expresscashwallet->render() !!} </div>
            </div>

        </div> <!-- /.box-body -->

    </div>

@endsection