<div class="col-md-6">
    <h2>My Debit Wallet Payments</h2>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

    /*  var acc = document.getElementsByClassName("accordion");
      var i;

      acc[0].addEventListener("click", function () {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
          } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
          }
      });*/

    var institution = $('.institution').val();

    jQuery(document).ready(function ($) {


        $.get("{{ url('admin/access/institution/count/choise')}}",
            {institution: institution},
            function (data) {
                $('.nbstudy').val(data.nbChoise);
            });

    })

    $('.institution').change(function () {
        $.get("{{ url('admin/access/institution/faculte/all')}}",
            {institution: $(this).val()},
            function (data) {
                var model = $('#faculte');
                model.empty();
                model.append("<option value=''>Selectionner la faculté</option>");
                $.each(data, function (index, element) {
                    model.append("<option value='" + element.id + "'>" + element.nom + "</option>");
                });
            });
    });


    $('.faculte').change(function () {

        $.get("{{ url('admin/access/faculte/departement/all')}}",
            {faculte: $(this).val()},
            function (data) {
                var model = $('#departement');
                model.empty();
                model.append("<option value=''>Selectionner le departement</option>");
                $.each(data, function (index, element) {
                    model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                });
            });
    });

    $('.departement').change(function () {
        $.get("{{ url('admin/access/departement/niveau/all')}}",
            {departement: $(this).val()},
            function (data) {
                var model = $('#niveau');
                model.empty();
                model.append("<option value=''>Selectionner le Diplôme</option>");
                $.each(data, function (index, element) {

                    model.append("<option value='" + element.id + "'>" + element.niveau + "</option>");
                });
            });
    });

    $('.niveau').change(function () {
        $.get("{{ url('admin/access/niveau/filiere/all')}}",
            {niveau: $(this).val()},
            function (data) {
                var model = $('#filiere');
                model.empty();
                model.append("<option value=''>Selectionner la filière</option>");
                $.each(data, function (index, element) {

                    model.append("<option value='" + element.id + "'>" + element.libelle + "</option>");
                });
            });
    });

    $('.filiere').change(function () {
        $.get("{{ url('admin/access/filiere/place/all')}}",
            {filiere: $(this).val()},
            function (data) {

                console.log(data)

                $('.place').val(data.nbPlace);
                /*

                                    model.append('<label class="label label-warning" id="place">' + data.nbPlace + ' place(s) disponibles</label>');

                */

            });
    });

    $('.institution').change(function () {
        $.get("{{ url('admin/access/institution/count/choise')}}",
            {institution: $(this).val()},
            function (data) {
                $('.nbstudy').val(data.nbChoise);
            });
    });

</script>
