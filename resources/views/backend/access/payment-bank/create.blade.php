@extends ('backend.layouts.app') 


@section ('title', 'Bank Account Payment')


@section('page-header')
<h1>
    Bank Account Payment
</h1>
@endsection 

@section('content')

{!! Form::open(['url' => '/admin/access/payment-bank', 'class' => 'form-horizontal', 'files' => true]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body">
        @include ('backend/access.payment-bank.form')

        <div class="box-footer">
            {{ Form::submit('Add Payment', ['class' => 'btn btn-primary pull-right']) }}
        </div>

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                <tr>
                    <th>Institution Name</th><th> Bank Name </th><th> Account Number </th><th> Amount </th><th> Scan deposit Slip </th>
                </tr>
                </thead>
                <tbody>
                @foreach($paymentbanks as $item)
                    <tr>
                        <td>{{ $item->institution }}</td>
                        <td>{{ $item->bankName }}</td>
                        <td>{{ $item->accountNumber }}</td>
                        <td>{{ $item->amount }}</td>
                        <td><a target="_blank" href="{{ $item->scanDepositSlip }}">Scan Deposit Slip</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $paymentbanks->render() !!} </div>
        </div>
    </div> <!-- /.box-body -->
     
     <!-- /.box-footer -->
    
</div> 
{!! Form::close() !!} 

@endsection