<div class="col-md-6">
    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('institution', 'Institution Name', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control institution" id="institution" name="institutionId" required>

                <option value="" selected disabled>Selectionner l'institution</option>

                <option value="{{$institution->id}}" {{ ($paymentbank != '' && $paymentbank->institutionId==  $institution->id) ? 'selected="selected"' : '' }} name="hello">{{$institution->nom}}</option>
                <option value="{{$userAdmin->email}}" {{ ($paymentbank != '' && $paymentbank->adminEmail==  $userAdmin->email) ? 'selected="selected"' : '' }} >Tamco-Tech</option>

            </select>
            {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('accountName') ? 'has-error' : ''}}">
        {!! Form::label('accountName', 'Bank Account Name', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('accountName', null, ['class' => 'form-control']) !!}
            {!! $errors->first('accountName', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('bankCode') ? 'has-error' : ''}}">
        {!! Form::label('bankCode', 'Bank code', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('bankCode', null, ['class' => 'form-control']) !!}
            {!! $errors->first('bankCode', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('rip') ? 'has-error' : ''}}">
        {!! Form::label('rip', 'Rip Number', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('rip', null, ['class' => 'form-control']) !!}
            {!! $errors->first('rip', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('scanDepositSlip') ? 'has-error' : ''}}">
        {!! Form::label('scanDepositSlip', 'Upload Deposit Slip', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::file('scanDepositSlip', null, ['class' => 'form-control']) !!}
            {!! $errors->first('scanDepositSlip', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>


<div class="col-md-6">
    <div class="form-group {{ $errors->has('ville') ? 'has-error' : ''}}">
        {!! Form::label('institution', 'Bank Name', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="form-control bankaccount" id="bankaccount" name="bankaccountId" required>
                <option value="" selected disabled>Selectionner Bank</option>
                @foreach($bankaccounts as $item)
                    <option value="{{$item->id}}" {{ ($paymentbank != '' && $paymentbank->bankaccountId==  $item->id) ? 'selected="selected"' : '' }} >{{$item->bankName}}</option>
                @endforeach
            </select>
            {!! $errors->first('institution', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('accountNumber') ? 'has-error' : ''}}">
        {!! Form::label('accountNumber', 'Bank Account Number', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('accountNumber', null, ['class' => 'form-control']) !!}
            {!! $errors->first('accountNumber', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


    <div class="form-group {{ $errors->has('branchcode') ? 'has-error' : ''}}">
        {!! Form::label('branchcode', 'Branch Code', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('branchcode', null, ['class' => 'form-control']) !!}
            {!! $errors->first('branchcode', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
        {!! Form::label('amount', 'Amount Deposit', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('amount', null, ['class' => 'form-control']) !!}
            {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('dateDepot') ? 'has-error' : ''}}">
        {!! Form::label('dateDepot', 'Payment Date', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::date('dateDepot', null, ['class' => 'form-control']) !!}
            {!! $errors->first('dateDepot', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<input type="hidden" name="conditionId" value="{{$conditionId}}">

{{--

<div class="form-group {{ $errors->has('slipNumber') ? 'has-error' : ''}}">
    {!! Form::label('slipNumber', 'Slip Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('slipNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('slipNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('reference') ? 'has-error' : ''}}">
    {!! Form::label('reference', 'Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('transactionNumber') ? 'has-error' : ''}}">
    {!! Form::label('transactionNumber', 'Transaction Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('transactionNumber', null, ['class' => 'form-control']) !!}
        {!! $errors->first('transactionNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div>
--}}



