@extends ('backend.layouts.app') 


@section ('title', trans('labels.backend.access.qualification.management') . ' | ' . trans('labels.backend.access.qualification.edit')) 


@section('page-header')
<h1>
    {{ trans('labels.backend.access.qualification.management') }}
    <small>{{ trans('labels.backend.access.qualification.edit') }}</small>
</h1> 
@endsection 

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Qualification</h3>
        <div class="box-tools pull-right">
        
            <a href="{{ url('/admin/access/qualification/create') }}" class="btn btn-primary " title="Add New Qualification"><span class="glyphicon glyphicon-plus" aria-hidden="true"/>{{trans('buttons.general.crud.add')}}</a>
            
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div> <!--box-tools pull-right-->
    </div> <!-- /.box-header -->
    <div class="box-body"> 

        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th> Type </th><th> Description </th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($qualification as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->type }}</td><td>{{ $item->description }}</td>
                        <td>
                            <a href="{{ url('/admin/access/qualification/' . $item->id) }}" class="btn btn-success btn-xs" title="View Qualification"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                            <a href="{{ url('/admin/access/qualification/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Qualification"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/access/qualification', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Qualification" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Qualification',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $qualification->render() !!} </div>
        </div>
        
    </div> <!-- /.box-body -->
     
</div> 

@endsection