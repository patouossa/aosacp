<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image"/>
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i
                            class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}
                </a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            @permissions(['manage-univ'])

            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/institution') }}">
                <a href="{{ route('admin.access.institution.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.institution') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/faculte') }}">
                <a href="{{ route('admin.access.faculte.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.faculte') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/departement-ins') }}">
                <a href="{{ route('admin.access.departement-ins.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.departement-ins') }}</span>
                </a>
            </li>


            <li class="{{ Active::pattern('admin/access/niveau') }}">
                <a href="{{ route('admin.access.niveau.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.niveau') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/filiere') }}">
                <a href="{{ route('admin.access.filiere.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.filiere') }}</span>
                </a>
            </li>

            {{--<li class="{{ Active::pattern('admin/access/Paymentmode') }}">
               <a href="{{ route('admin.access.paymentmode.index') }}">
                   <i class="fa fa-circle-o"></i>
                   <span>{{ trans('menus.backend.sidebar.paymentmode') }}</span>
               </a>
           </li>--}}


            <li class="{{ Active::pattern('admin/access/billing') }}">
                <a href="{{ route('admin.access.billing.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.billing') }}</span>
                </a>
            </li>


            <li class="{{ Active::pattern('admin/access/paymentmethod') }}">
                <a href="{{ route('admin.access.paymentmethod.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.paymentmethod') }}</span>
                </a>
            </li>


            <li class="{{ Active::pattern('admin/access/manageusers') }}">
                <a href="{{ route('admin.access.manageusers.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.manageusers') }}</span>
                </a>
            </li>


            <li class="{{ Active::pattern('admin/access/*') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Student Applications</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">

                    <li class="{{ Active::pattern('admin/access/studentpayment') }}">
                        <a href="{{ route('admin.access.studentpayment.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.studentpayment') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/aapplication') }}">
                        <a href="{{ route('admin.access.aapplication.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.aapplication') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/rapplication') }}">
                        <a href="{{ route('admin.access.rapplication.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.rapplication') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/dlist') }}">
                        <a href="{{ route('admin.access.dlist.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.dlist') }}</span>
                        </a>
                    </li>

                </ul>
            </li>


            <li class="{{ Active::pattern('admin/access/payment-bank') }}">
                <a href="{{ route('admin.access.payment-bank.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.payment-bank') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/payment-expresscash') }}">
                <a href="{{ route('admin.access.payment-expresscash.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.payment-expresscash') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/payment-mobile') }}">
                <a href="{{ route('admin.access.payment-mobile.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.payment-mobile') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/payment-transfer') }}">
                <a href="{{ route('admin.access.payment-transfer.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.payment-transfer') }}</span>
                </a>
            </li>
            @endauth

            @permissions(['manage-student','manage-univ'])

            @permissions(['manage-univ'])
            <li class="{{ Active::pattern('admin/access/student') }}">
                <a href="{{ route('admin.access.student.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.student') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/requirement') }}">
                <a href="{{ route('admin.access.requirement.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.requirement') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/*') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Payment Configuration</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">

                    <li class="{{ Active::pattern('admin/access/bank-account') }}">
                        <a href="{{ route('admin.access.bank-account.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.bank-account') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/express-cash-wallet') }}">
                        <a href="{{ route('admin.access.express-cash-wallet.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.express-cash-wallet') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/mobile-transfer') }}">
                        <a href="{{ route('admin.access.mobile-transfer.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.mobile-transfer') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/access/money-transfer') }}">
                        <a href="{{ route('admin.access.money-transfer.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.money-transfer') }}</span>
                        </a>
                    </li>

                </ul>
            </li>
            @endauth


            @if($student1 != null)
                <li class="header">The Process Flow</li>

                @if(access()->user()->type == 'manage-student')

                    <li class="{{ Active::pattern('admin/access/student') }}">
                        <a href="{{ route('admin.access.student.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.sidebar.preinscription') }}</span>
                        </a>
                    </li>
                @endif

                @if($student1->status >= 0)
                    <li class="{{ Active::pattern('admin/access/*') }} treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>My Application</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-close') }}"
                            style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">

                            <li class="{{ Active::pattern('admin/access/student1') }}">
                                <a href="{{ url('/admin/access/summary/') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Application Summary</span>
                                </a>
                            </li>

                            <li class="{{ Active::pattern('admin/access/student1') }}">
                                <a href="{{ url('/admin/access/student/' . $student1->id . '/edit') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Demographique informations</span>
                                </a>
                            </li>

                            <li class="{{ Active::pattern('admin/access/student1') }}">
                                <a href="{{ url('/admin/access/school-information/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Education Information</span>
                                </a>
                            </li>

                            <li class="{{ Active::pattern('admin/access/student1') }}">
                                <a href="{{ url('/admin/access/examination/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Exams/Results Information</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endif

                @if($student1->status >= 2)
                    <li class="{{ Active::pattern('admin/access/*') }} treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>My Institutions</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-close') }}"
                            style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">

                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/institution-summary') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Institution Summary</span>
                                </a>
                            </li>

                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/programstudie/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Institution</span>
                                </a>
                            </li>

                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/institutionupload/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Institution Upload</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Active::pattern('admin/access/*') }} treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>My Application Fee</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>

                        <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-close') }}"
                            style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">

                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/condition') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Application Fee Summary</span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/viewpaymentoptions') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>View Payment Options</span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/addbankaccount') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Add Bank Account Payment</span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/addmoneytransfer') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Add Money Transfer Payment</span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/addmobileoperator') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Add Mobile Operator Payment</span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/adddebitwallet') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Add Debit Wallet Payment</span>
                                </a>
                            </li>

                           </ul>
                            <li class="{{ Active::pattern('admin/access/institution') }} ">
                                <a href="{{ url('/admin/access/viewsubmitApp') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>View/Submit My Application </span>
                                </a>
                            </li>
                            <li class="{{ Active::pattern('admin/access/institution') }} ">
                            <a href="{{ url('/admin/access/PrintSubmitApplication') }}">
                                <i class="fa fa-circle-o"></i>
                                <span>Print/Submit My Application</span>
                            </a>
                            </li>

                          {{--
                            <li class="{{ Active::pattern('admin/access/institution') }}">
                                <a href="{{ url('/admin/access/programstudie/create') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Institution</span>
                                </a>
                            </li>
                           --}}
                        </ul>
                    </li>
                @endif

            @endif



            {{--<li class="{{ Active::pattern('admin/access/school-information') }}">
                <a href="{{ route('admin.access.school-information.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.school-information') }}</span>
                </a>
            </li>


            <li class="{{ Active::pattern('admin/access/examination') }}">
                <a href="{{ route('admin.access.examination.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.examination') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/programstudie') }}">
                <a href="{{ route('admin.access.programstudie.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.programstudie') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/access/certificate') }}">
                <a href="{{ route('admin.access.certificate.index') }}">
                    <i class="fa fa-circle-o"></i>
                    <span>{{ trans('menus.backend.sidebar.certificate') }}</span>
                </a>
            </li>--}}
            @endauth

            @permissions(['manage-users', 'manage-roles'])

            <ul class="sidebar-menu">

                {{--<li class="{{ Active::pattern('admin/access/qualification') }}">
                    <a href="{{ route('admin.access.qualification.index') }}">
                        <i class="fa fa-circle-o"></i>
                        <span>{{ trans('menus.backend.sidebar.qualification') }}</span>
                    </a>
                </li>

                <li class="{{ Active::pattern('admin/access/billing-config') }}">
                    <a href="{{ route('admin.access.billing-config.index') }}">
                        <i class="fa fa-circle-o"></i>
                        <span>{{ trans('menus.backend.sidebar.billing-config') }}</span>
                    </a>
                </li>--}}

                <li class="{{ Active::pattern('admin/access/ville') }}">
                    <a href="{{ route('admin.access.ville.index') }}">
                        <i class="fa fa-circle-o"></i>
                        <span>{{ trans('menus.backend.sidebar.ville') }}</span>
                    </a>
                </li>

                <li class="{{ Active::pattern('admin/access/region') }}">
                    <a href="{{ route('admin.access.region.index') }}">
                        <i class="fa fa-circle-o"></i>
                        <span>{{ trans('menus.backend.sidebar.region') }}</span>
                    </a>
                </li>

            </ul>


            <li class="{{ Active::pattern('admin/access/*') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">
                    @permission('manage-users')
                    <li class="{{ Active::pattern('admin/access/user*') }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>
                    @endauth

                    @permission('manage-roles')

                    <li class="{{ Active::pattern('admin/access/role*') }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                    @endauth
                </ul>
            </li>
            @endauth
            @permissions(['manage-users', 'manage-roles'])
            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            <li class="{{ Active::pattern('admin/log-viewer*') }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}"
                    style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
