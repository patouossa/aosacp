<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Access Management',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Create User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],
        ],

        'log-viewer' => [
            'main'      => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general'   => 'General',
            'system'    => 'System',
            'institution'    => 'Institutions',
            'filiere'    => 'Study Program',
            'region'    => 'Regions',
            'departement-ins'    => 'Departement',
            'ville'    => 'town',
            'niveau'    => 'Anticipated Qualification',
            'student'    => 'Students',
            'school-information'    => 'School Informations',
            'examination'    => 'Examinations',
            'programstudie'    => 'Program Studies',
            'certificate'    => 'Certificates',
            'billing-config'    => 'Billing Config',
            'billing'    => 'Billing',
            'preinscription'    => 'My preinscriptions',
            'faculte'    => 'Faculty',
            'paymentmode'    => 'Payment Mode',
            'qualification'    => 'Qualifications',
            'bank-account'    => 'Bank Account',
            'express-cash-wallet'    => 'Express Cash Account',
            'mobile-transfer'    => 'Mobile Transfer',
            'money-transfer'    => 'Money Transfer',
            'requirement'    => 'Upload Requirement',
            'payment-bank'    => 'Bank Payment',
            'payment-expresscash'    => 'Express Cash Payment',
            'payment-mobile'    => 'Mobile Payment',
            'payment-transfer'    => 'Money Trabsfer Payment',
            'condition'    => 'Terms & Conditions',
            'paymentmethod'    => 'Payment Method',
            'manageusers'    => 'Manage users',
            'studentpayment'    => 'Student Payments & Uploads',
            'aapplication'    => 'Acknowledged Applications',
            'rapplication'    => 'Rejected Applications',
            'dlist'    => 'Deliberation List',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => 'Arabic',
            'da'    => 'Danish',
            'de'    => 'German',
            'el'    => 'Greek',
            'en'    => 'English',
            'es'    => 'Spanish',
            'fr'    => 'French',
            'id'    => 'Indonesian',
            'it'    => 'Italian',
            'nl'    => 'Dutch',
            'pt_BR' => 'Brazilian Portuguese',
            'sv'    => 'Swedish',
            'th'    => 'Thai',
        ],
    ],
];
