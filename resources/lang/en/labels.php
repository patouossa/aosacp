<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'active' => 'Active',
        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
        ],
        'hide' => 'Hide',
        'inactive' => 'Inactive',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],

            'users' => [
                'active' => 'Active Users',
                'all_permissions' => 'All Permissions',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'management' => 'User Management',
                'no_permissions' => 'No Permissions',
                'no_roles' => 'No Roles to set.',
                'permissions' => 'Permissions',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'roles' => 'Roles',
                    'total' => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'status' => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],

            'institution' => [
                'create' => 'Create a institution',
                'show' => 'Institutions',
                'management' => 'Institutions Management',
                'edit' => 'update a institution',
            ],

            'departement-ins' => [
                'create' => 'Create a departement',
                'show' => 'Departements',
                'management' => 'Departements Management',
                'edit' => 'update a departement',
            ],

            'region' => [
                'create' => 'Create a region',
                'show' => 'Regions',
                'management' => 'Regions Management',
                'edit' => 'update a region',
            ],

            'ville' => [
                'create' => 'Create a town',
                'show' => 'Towns',
                'management' => 'Towns Management',
                'edit' => 'update a town',
            ],

            'filiere' => [
                'create' => 'Create a Program Study',
                'show' => 'Program Study',
                'management' => 'Program Study Management',
                'edit' => 'update a Program Study',
            ],

            'student' => [
                'create' => 'Create a student',
                'show' => 'Students',
                'management' => 'Students Management',
                'edit' => 'update a student',
            ],

            'school-information' => [
                'create' => 'Create a Education Information',
                'show' => 'Education Information',
                'management' => 'Education Information Management',
                'edit' => 'update a Education Information',
            ],

            'examination' => [
                'create' => 'Create a Exams/Results Information ',
                'show' => 'Exams/Results Information',
                'management' => 'Exams/Results Information Management',
                'edit' => 'update a Exams/Results Information',
            ],

            'programstudie' => [
                'create' => 'Create a Study Program',
                'show' => 'Study Program',
                'management' => 'Study Program Management',
                'edit' => 'update a Study Program',
            ],

            'certificate' => [
                'create' => 'Create a Upload Requested Documents',
                'show' => 'Upload Requested Documents',
                'management' => 'Upload Requested Documents Management',
                'edit' => 'update a Upload Requested Documents',
            ],

            'billing-config' => [
                'create' => 'Create a Billing Config',
                'show' => 'Billing Config',
                'management' => 'Billing Config Management',
                'edit' => 'update a Billing Config',
            ],

            'billing' => [
                'create' => 'Create a Billing',
                'show' => 'Billing',
                'management' => 'Billing Management',
                'edit' => 'Update a Billing',
            ],

            'niveau' => [
                'create' => 'Create a Anticipated Qualification',
                'show' => 'Anticipated Qualification',
                'management' => 'Anticipated Qualification Management',
                'edit' => 'Update a Anticipated Qualification',
            ],
            'faculte' => [
                'create' => 'Create a Faculty',
                'show' => 'Faculty',
                'management' => 'Faculty Management',
                'edit' => 'Update a Faculty',
            ],

            'paymentmode' => [
                'create' => 'Create a Payment mode',
                'show' => 'Payment mode',
                'management' => 'Payment mode Management',
                'edit' => 'Update a Payment mode',
            ],

            'qualification' => [
                'create' => 'Create a Qualification',
                'show' => 'Qualifications',
                'management' => 'Qualification Management',
                'edit' => 'Update a Qualification',
            ],

            'bank-account' => [
                'create' => 'Create a Bank Account',
                'show' => 'Bank Account',
                'management' => 'Bank Account Management',
                'edit' => 'Update a Bank Account',
            ],

            'express-cash-wallet' => [
                'create' => 'Create a Express Cash Account',
                'show' => 'Express Cash Account',
                'management' => 'Express Cash Account Management',
                'edit' => 'Update a Express Cash Account',
            ],

            'mobile-transfer' => [
                'create' => 'Create a Mobile Transfer',
                'show' => 'Mobile Transfer',
                'management' => 'Mobile Transfer Management',
                'edit' => 'Update a Mobile Transfer',
            ],

            'money-transfer' => [
                'create' => 'Create a Money Transfer',
                'show' => 'Money Transfer',
                'management' => 'Money Transfer Management',
                'edit' => 'Update a Money Transfer',
            ],

            'requirement' => [
                'create' => 'Create a Requirement',
                'show' => 'Requirement',
                'management' => 'Requirement Management',
                'edit' => 'Update a Requirementr',
            ],

            'payment-bank' => [
                'create' => 'Create a Bank Payment',
                'show' => 'Bank Payment',
                'management' => 'Bank Payment Management',
                'edit' => 'Update a Bank Payment',
            ],

            'payment-expresscash' => [
                'create' => 'Create a Express Cash Payment',
                'show' => 'Express Cash',
                'management' => 'Express Cash Management',
                'edit' => 'Update a Express Cash',
            ],

            'payment-mobile' => [
                'create' => 'Create a Mobile Payment',
                'show' => 'Mobile Payment',
                'management' => 'Mobile Payment Management',
                'edit' => 'Update a Mobile Payment',
            ],

            'payment-transfer' => [
                'create' => 'Create a Money Transfer Payment',
                'show' => 'Money Transfer Payment',
                'management' => 'Money Transfer Payment Management',
                'edit' => 'Update a Money Transfer Payment',
            ],

            'condition' => [
                'create' => 'Create a Terms & Conditions ',
                'show' => 'Terms & Conditions',
                'management' => 'Terms & Conditions Management',
                'edit' => 'Update a Terms & Conditions',
            ],
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button' => 'Register',
            'remember_me' => 'Remember Me',
        ],

        'passwords' => [
            'forgot_password' => 'Forgot Your Password?',
            'reset_password_box_title' => 'Reset Password',
            'reset_password_button' => 'Reset Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'macros' => [
            'country' => [
                'alpha' => 'Country Alpha Codes',
                'alpha2' => 'Country Alpha 2 Codes',
                'alpha3' => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us' => [
                    'us' => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed' => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'update_information' => 'Update Information',
            ],
        ],

        'nom' => 'Name',
        'name_with_bird' => 'Name of applicant as on their Birth Certificate',
        'date_naiss' => 'Date of Birth',
        'disability' => 'Disability',
        'select_disability' => 'Select Disability',
        'select_ville' => 'Select the Town',
        'all_ville' => 'All the Town',
        'select_region' => 'Select the Region',
        'all_region' => 'All Region',
        'none' => 'None',
        'country_birth' => 'Country of Residence',
        'select_country' => 'Select Country',
        'nationality' => 'Nationality',
        'select_nationality' => 'Select a Nationality',
        'religion' => 'Religion',
        'select_religion' => 'Select Religion',
        'annuler' => 'Cancel',
        'marital_status' => 'Marital Status',
        'select_marital_status' => 'Select Marital Status',
        'seemore' => 'See more',
        'region' => 'Region',
        'ville' => 'Town',
        'specialite' => 'Specialty',
        'lieu' => 'Location',
        'email' => 'Email',
        'phone' => 'Phone',
        'sexe' => 'Sex',
        'male' => 'Male',
        'female' => 'Female',
        'fathername' => 'Father\'s Names',
        'mothername' => 'Mother\'s Names',
        'country_parent' => 'Parent\'s Country',
        'ville_parent' => 'Parent\'s Town/City',
        'adresseparent' => 'Parent\'s Address (Quarter & Location)',
        'parent_occupation' => 'Parent\'s Occupation',
        'parent_phone' => 'Parent\'s Telephone',
        'parent_email' => 'Parent\'s Email',
        'sans_experience' => 'No experience',
        'patience' => 'Please wait while the map is loading...',
        'avis' => 'Tell Us Your Experience',
        'commentaire' => 'Comments',
        'avis_connect' => 'share your Experience',
        'your_comment' => 'Your Comments:',
        'send_your_comment' => 'Send',
        'back' => 'BacK',
        'titre' => 'Title',
        'close' => 'Close',
        'frais_consultation' => 'Consultation Fee :',
        'nbre_medecin' => 'Number of Doctors : ',
        'typecentre' => 'Type of centre : ',
        'nbre_infirmier' => ' Number of Nurses : ',
        'nbre_lit' => 'Number of Beds : ',
        'description' => 'Description',
        'approuve' => 'Approved',
        'localisation' => 'Location',
        'arrondissement' => 'District',
        'departement' => 'Department',
        'select_arrondissement' => 'Select the district',
        'all_arrondissement' => 'All the district',
        'select_departement' => 'Select the department',
        'all_departement' => 'All the department',
        'bp' => 'P.O. Box',
        'photo' => 'Picture',
        'libelle' => 'Label',
        'jour' => 'Days',
        'select_jour' => 'select the day',
        'plagehoraire' => 'Time Range',
        'lundi' => 'Monday',
        'mardi' => 'Tuesday',
        'mercredi' => 'Wednesday',
        'jeudi' => 'Thursday',
        'vendredi' => 'Friday',
        'samedi' => 'Saturday',
        'dimanche' => 'Sunday',
        'de' => 'To',
        'a' => 'From',

        'confirm_delete' => 'return confirm("Confirm delete?")',
        'delete' => 'Delete',
        'add' => 'Add',
        'view' => 'View',
        'edit' => 'Update',
        'cancel_pub' => 'Cancel Publication',
        'confirm_cancel_pub' => 'return confirm("Cancel publication?")',
        'publication' => 'Publication',
        'confirm_publication' => 'Confirm Publication',
        'donate' => 'Faire un don',
        'montant_usd' => 'Amount in USD',
        'thank_donate' => 'Thanks to make a donate',

        'status' => 'Status',
        'see_googlemaps' => 'See on Google Maps',
        'select_status' => 'Select a Status',
        'public' => 'PUBLIC',
        'private' => 'PRIVATE',
        'para-public' => 'PARA-PUBLIC',
        'longitude' => 'Longitude',
        'latitude' => 'Latitude',
        'cordx' => 'Cordx',
        'accept' => 'Accept',
        'reject' => 'Reject',
        'visitor' => 'Number of visitors',
        'pay_to_submit' => 'Payer pour soumettre votre dossier',
        'amount_platform' => 'Frais d\'utilisation de la platforme',
        'amount_institution' => 'Frais de depot de dossier',
        'pay' => 'Payer',
        'finish_preinscription' => 'Finaliser la preiscription',
        'select_institution' => 'Selectionner l\'institution',
        'institution' => 'Institution',

    ],
];
