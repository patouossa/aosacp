<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Activer',
                'change_password'    => 'Changer de mot de passe',
                'deactivate'         => 'Désactiver',
                'delete_permanently' => 'Supprimer définitivement',
                'login_as'           => 'Se connecter avec :user',
                'resend_email'       => 'Renvoyer le mail de confirmation',
                'restore_user'       => "Réactiver l'utilisateur",
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirmer le compte',
            'reset_password'  => 'Réinitialiser le mot de passe',
        ],
    ],

    'general' => [
        'cancel' => 'Annuler',

        'crud' => [
            'create' => 'Créer',
            'create_exam' => 'Créer les examens',
            'create_school' => 'Créer une information scolaire',
            'delete' => 'Supprimer',
            'edit'   => 'Editer',
            'update' => 'Mettre à jour',
            'view'   => 'Voir',
            'add'    => 'Ajouter',
            'next'    => 'Suivant',
            'print'    => 'Imprimer',
            'to_preinscription'    => 'Faire une preinscription',
            'save_phase1'    => 'Enregistrer la Phase 1',
            'save_phase2'    => 'Enregistrer la Phase 2',
            'save' => 'Sauvegarder',

            'go_to_school_info' => 'Go to School Information',
            'go_to_examination' => 'Go to Examination',
            'go_to_back_to_registration' => 'Go back to Registration',
            'go_to_back_to_school_info' => 'Go back to School Information',
        ],

        'save' => 'Sauvegarder',
        'view' => 'Voir',
    ],
];
