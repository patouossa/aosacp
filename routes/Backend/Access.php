<?php

/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix'     => 'access',
    'as'         => 'access.',
    'namespace'  => 'Access',
], function () {

    /*
     * User Management
     */
    Route::group([

    ], function () {



        Route::group(['namespace' => 'Ville'], function () {
            Route::resource('ville', 'VilleController');
        });

        Route::group(['namespace' => 'Region'], function () {
            Route::resource('region', 'RegionController');
        });

        Route::group(['namespace' => 'DepartementIns'], function () {
            Route::resource('departement-ins', 'DepartementInsController');
        });

        Route::group(['namespace' => 'Filiere'], function () {
            Route::resource('filiere', 'FiliereController');
        });

        Route::group(['namespace' => 'Institution'], function () {
            Route::resource('institution', 'InstitutionController');
        });

        Route::group(['namespace' => 'InstitutionUpload'], function () {
            Route::resource('institutionupload', 'InstitutionUploadController');
            //Route::controller('institutionupload', 'InstitutionUploadController');
            Route::get('requirements', ['as' => 'institutionupload_requirements', 'uses' => 'InstitutionUploadController@requirements']);
        });





        Route::group(['namespace' => 'PaymentOptions'], function () {
            Route::resource('viewpaymentoptions', 'ViewPaymentOptionsController');
            Route::get('paymentoptions', ['as' => 'institutionupload_options', 'uses' => 'ViewPaymentOptionsController@paymentoptions']);
            Route::get('banknames', ['as' => 'institutionupload_bankNames', 'uses' => 'ViewPaymentOptionsController@banknames']);
            Route::get('bankdetails', ['as' => 'institutionupload_bankdetails', 'uses' => 'ViewPaymentOptionsController@bankdetails']);
        });

        Route::group(['namespace' => 'PaymentOptions'], function () {
            Route::resource('addbankaccount', 'AddBankAccountController');
        });

        Route::group(['namespace' => 'PaymentOptions'], function () {
            Route::resource('addmoneytransfer', 'AddMoneyTransferController');
        });

        Route::group(['namespace' => 'PaymentOptions'], function () {
            Route::resource('addmobileoperator', 'AddMobileOperatorController');
        });

        Route::group(['namespace' => 'PaymentOptions'], function () {
            Route::resource('adddebitwallet', 'AddDebitWalletController');
        });





        Route::group(['namespace' => 'Niveau'], function () {
            Route::resource('niveau', 'NiveauController');
        });

        Route::group(['namespace' => 'PaymentMethod'], function () {
            Route::resource('paymentmethod', 'DeliberationListController');
        });

        Route::group(['namespace' => 'ManageUsers'], function () {
            Route::resource('manageusers', 'ManageUsersController');
        });

        Route::group(['namespace' => 'StudentPayment'], function () {
            Route::resource('studentpayment', 'StudentPaymentController');
        });

        Route::group(['namespace' => 'AcknowledgeApplication'], function () {
            Route::resource('aapplication', 'AcknowledgeApplicationController');
        });

        Route::group(['namespace' => 'RejectedApplication'], function () {
            Route::resource('rapplication', 'RejectedApplicationController');
        });

        Route::group(['namespace' => 'DeliberationList'], function () {
            Route::resource('dlist', 'DeliberationListController');
        });

        Route::group(['namespace' => 'ViewSubmitMyApplication'], function () {
            Route::resource('viewsubmitApp', 'ViewSubmitMyApplicationController');
        });

        Route::group(['namespace' => 'Student'], function () {
              Route::resource('student', 'StudentController');
              Route::get('summary', 'StudentController@gotoSummary')->name('summary');
              Route::get('institution-summary', 'StudentController@gotoInstitutionSummary')->name('institution-summary');
              Route::get('institution-detail/{institutionId}', 'StudentController@goToDetailInstitution')->name('institution-detail');
            Route::patch('student/reject/{id}', 'StudentController@reject')->name('reject');
            Route::patch('student/accept/{id}', 'StudentController@accept')->name('accept');
        });

        Route::group(['namespace' => 'SchoolInformation'], function () {
            Route::resource('school-information', 'SchoolInformationController');
            Route::patch('school-information/step2', 'SchoolInformationController@step2')->name('school-info.step2');;
        });

        Route::group(['namespace' => 'Examination'], function () {
            Route::resource('examination', 'ExaminationController');
        });

        Route::group(['namespace' => 'Faculte'], function () {
            Route::resource('faculte', 'FaculteController');
        });

        Route::group(['namespace' => 'ProframStudy'], function () {
            Route::resource('programstudie', 'ProgramstudieController');
        });

        Route::group(['namespace' => 'Certificate'], function () {
            Route::resource('certificate', 'CertificateController');
            Route::patch('checkout', 'CertificateController@billing')->name('checkout');;
        });

        Route::group(['namespace' => 'Billing'], function () {
            Route::resource('billing', 'BillingController');
        });

        Route::group(['namespace' => 'BillingConfig'], function () {
            Route::resource('billing-config', 'BillingConfigController');
        });
        Route::group(['namespace' => 'Paymentmode'], function () {
            Route::resource('paymentmode', 'PaymentmodeController');
        });

        Route::group(['namespace' => 'BankAccount'], function () {
            Route::resource('bank-account', 'BankAccountController');
        });

        Route::group(['namespace' => 'ExpressCashWallet'], function () {
            Route::resource('express-cash-wallet', 'ExpressCashWalletController');
        });

        Route::group(['namespace' => 'MobileTransfer'], function () {
            Route::resource('mobile-transfer', 'MobileTransferController');
        });

        Route::group(['namespace' => 'MoneyTransfer'], function () {
            Route::resource('money-transfer', 'MoneyTransferController');
        });

        Route::group(['namespace' => 'Requirement'], function () {
            Route::resource('requirement', 'RequirementController');
        });

        Route::group(['namespace' => 'PaymentBank'], function () {
            Route::resource('payment-bank', 'PaymentBankController');
        });

        Route::group(['namespace' => 'PaymentExpresscash'], function () {
            Route::resource('payment-expresscash', 'PaymentExpresscashController');
        });

        Route::group(['namespace' => 'PaymentMobile'], function () {
            Route::resource('payment-mobile', 'PaymentMobileController');
        });

        Route::group(['namespace' => 'PaymentTransfer'], function () {
            Route::resource('payment-transfer', 'PaymentTransferController');
        });

        Route::group(['namespace' => 'UploadRequirement'], function () {
            Route::resource('upload-requirement', 'UploadRequirementController');
        });

        Route::group(['namespace' => 'Condition'], function () {
            Route::resource('condition', 'ConditionController');
        });

        Route::get('region/ville/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('region');
            $ville = \App\Models\Access\Ville\Ville::where('regionId', $input)->get();
            return $ville;
        });

        Route::get('institution/faculte/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('institution');
            $facultes = \App\Models\Access\Faculte\Faculte::where('institutionId', $input)->get();
            return $facultes;
        });

        Route::get('faculte/departement/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('faculte');
            $departements = \App\Models\Access\DepartementIns\DepartementIn::where('faculteId', $input)->get();
            return $departements;
        });

        Route::get('departement/niveau/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('departement');
            $niveau = \App\Models\Access\Niveau\Niveau::where('departementId', $input)->get();
            return $niveau;
        });

        Route::get('niveau/filiere/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('niveau');
            $filieres = \App\Models\Access\Filiere\Filiere::where('niveauId', $input)->get();
            return $filieres;
        });

        Route::get('filiere/place/all', function(){
            $input = \Illuminate\Support\Facades\Input::get('filiere');
            $filiere = \App\Models\Access\Filiere\Filiere::where('id', $input)->first();
            return $filiere;
        });

        Route::get('institution/count/choise', function(){
            $input = \Illuminate\Support\Facades\Input::get('institution');
            $institution = \App\Models\Access\Institution\Institution::where('id', $input)->first();
            return $institution;
        });
                     
        Route::group(['namespace' => 'User'], function () {

                Route::resource('profil', 'ProfilController');
                Route::patch('profile/update', 'ProfilController@update')->name('profile.update');
                Route::patch('password/change', 'ProfilController@changePassword')->name('password.change');

            /*
             * For DataTables
             */
            Route::post('user/get', 'UserTableController')->name('user.get');

            /*
             * User Status'
             */
            Route::get('user/deactivated', 'UserStatusController@getDeactivated')->name('user.deactivated');
            Route::get('user/deleted', 'UserStatusController@getDeleted')->name('user.deleted');

            /*
             * User CRUD
             */
            Route::resource('user', 'UserController');

            /*
             * Specific User
             */
            Route::group(['prefix' => 'user/{user}'], function () {
                // Account
                Route::get('account/confirm/resend', 'UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');

                // Status
                Route::get('mark/{status}', 'UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);

                // Password
                Route::get('password/change', 'UserPasswordController@edit')->name('user.change-password');
                Route::patch('password/change', 'UserPasswordController@update')->name('user.change-password');

                // Access
                Route::get('login-as', 'UserAccessController@loginAs')->name('user.login-as');
            });



            /*
             * Deleted User
             */
            Route::group(['prefix' => 'user/{deletedUser}'], function () {
                Route::get('delete', 'UserStatusController@delete')->name('user.delete-permanently');
                Route::get('restore', 'UserStatusController@restore')->name('user.restore');
            });
        });
    });

    /*
     * Role Management
     */
    Route::group([
        'middleware' => 'access.routeNeedsPermission:manage-roles',
    ], function () {
        Route::group(['namespace' => 'Role'], function () {
            Route::resource('role', 'RoleController', ['except' => ['show']]);

            //For DataTables
            Route::post('role/get', 'RoleTableController')->name('role.get');
        });
    });
});
